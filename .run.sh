#!/bin/bash

shopt -s expand_aliases
let timer=0
alias display="echo -ne"

#clear the file in case of there is old log.
echo '' >output.log

#while (! (lsof -nP -i TCP | grep node | grep 5050))
while ( ! (cat output.log | grep 'Started MongoDB.')); do
	sleep 1
	let timer++
	display "\rwaiting for meteor server to be ready ...\t ${timer}s \t"
done

# pwd
# osascript <<END
#     tell application "Terminal"
#         activate
#         do script "cd ~/bn2/api && meteor shell && exit" in front window
#     end tell
#     tell application "System Events"
#         keystroke "t" using {command down}
#     end tell
#     tell application "Terminal"
#         do script "sleep 1 && cd ~/bn2/api && meteor mongo && exit" in front window
#     end tell
# END

# use ttab to replace osascript, ref: https://www.npmjs.com/package/ttab
#ttab -t "METEOR SHELL" "cd ~/bn2/api && meteor shell && exit 0"
#ttab -t "METEOR MONGO" "cd ~/bn2/api && meteor mongo && exit 0"

ionic serve -c --lib -w 'google chrome' && exit 0

cd ./platforms/android/ && ./gradlew uninstallAll
