// @ts-nocheck
import {
    checkNpmVersions
} from 'meteor/tmeasday:check-npm-versions';

checkNpmVersions({
    'sharp': '^0.20.5'
}, 'my:awesome-package');


export var Sharp = require('sharp');
