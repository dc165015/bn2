declare type Constructor<T> = new (...args: any[]) => T;

declare type PrimitiveConstructor = NumberConstructor | StringConstructor | BooleanConstructor | SymbolConstructor;
declare type ObjectArray<T> = [Constructor<T>];
declare type Primitive = string | number | symbol | boolean | null | undefined;
declare type Enum = Primitive[];
declare type Nil = null | undefined;

declare type Dictionary<T> = { [key: string]: T };
declare type PartialMapTo<T, M> = Partial<Record<keyof T, M>>;
declare type OnlyArrays<T> = T extends any[] ? T : never;
declare type OnlyElementsOfArrays<T> = T extends any[] ? Partial<T[0]> : never;
declare type ElementsOf<T> = {
    [P in keyof T]?: OnlyElementsOfArrays<T[P]>
};

declare interface CanMap<T> {
    map<U>(callback: (doc: T, index: number, ...args) => U, thisArg?: any): Array<U>
}

declare type InstanceOfSubClass<T> = T & { [key: string]: any };

declare module 'meteor/jalik:ufs';
declare module 'meteor/dburles:factory';
declare module 'meteor/practicalmeteor:faker';
declare module 'meteor/xolvio:cleaner';
declare module 'meteor/mdg:validated-method';
declare module 'meteor/matb33:collection-hooks';

declare module 'meteor/sharp-dummy';

declare namespace SMS {
    var phoneTemplates: {
        text: Function;
    };
}

declare module 'meteor/check' {
    namespace Match { function Error(msg: string): void; }
}

declare module 'meteor/mongo' {
    export const MongoInternals: any;
    namespace Mongo {
        interface Collection<T> {
            _name: string;
        }
    }
}

declare module 'meteor/ddp' {
    namespace DDP { function onReconnect(callback: () => any): void; }
}
