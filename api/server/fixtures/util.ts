import { Meteor } from 'meteor/meteor';
import { Picture } from '../models/picture';
import { DocCollection } from '../collections';
import { DocModel } from '../models/doc-model';
import { faker } from './fixture';

export function importAvatarFromUrl(name?, url?) {
    url = url || randomAvatarUrl();
    return Meteor.call('ufsImportURL', url, { name, for: Picture.FOR.USER_AVATAR }, 'pictures');
}

export function randomAvatarUrl() {
    const random = Math.floor(Math.random() * 100);
    const lego = random < 10;
    const gender = lego ? 'lego' : random > 5 ? 'men' : 'women';
    return `https://randomuser.me/api/portraits/${gender}/${random}.jpg`;
}

export function getRandomStatus(length, start = 0) {
    return 1 << (Math.floor(Math.random() * 10) % length + start);
}

export function getRandomSelector<T>(baseSelector?: MongoSelector<T>) {
    return Object.assign({ createdAt: { $gte: faker.date.past() } }, baseSelector);
}

export function getRandomDocs<T extends DocModel>(
    collection: DocCollection<T>,
    count = 1,
    selector: MongoSelector<T> = {},
): T[] {
    const existedCount = collection.find(selector).count();
    if (existedCount < count) throw new Error('insufficient documents to retrieve');

    const samplePositions = [];
    while (count--) {
        const skip = Math.floor(existedCount * Math.random());
        if (samplePositions.includes(skip)) count++; //retry
        samplePositions.push(skip);
    }

    const samples = [];
    for (let i = 0; i < samplePositions.length; i++) {
        samples[i] = collection.findOne(selector, { skip: samplePositions[i] });
    }

    return samples;
}

export function getRandomItem<T>(list: T[]): T {
    return list[Math.floor(Math.random() * list.length)];
}
