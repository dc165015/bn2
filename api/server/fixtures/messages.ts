import { Factory } from "./factory";
import { MessageType, Message } from '../models/message';
import { getUser } from './users';
import { Fixture, faker } from './fixture';
import { Messages } from '../collections/messages';
import { getChat } from './chats';
import { MAX_MESSAGE_LENGTH } from "../lib/constants";
import { Chats } from "../collections/chats";

const fixture = new Fixture(Messages);
export const getMessage = fixture.getOne.bind(fixture);
export const getMessages = fixture.getMany.bind(fixture);
export const createMessages = fixture.makeMany.bind(fixture);

export function getMessagesPerChat(count: number = 1) {
    const chatCount = Chats.find().count();
    getMessages(chatCount * count);
}

Factory.define(Message, function mockup() {
    return {
        chatId: getChat()._id,
        content: faker.lorem.paragraph(3).substring(0, MAX_MESSAGE_LENGTH),
        createdAt: faker.date.past(),
        senderId: getUser()._id,
        type: MessageType.TEXT
    }
});
