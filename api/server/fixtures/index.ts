import { resetDatabase } from 'meteor/xolvio:cleaner';
import { reportWhoHasMostCopies, createCopiesPerUser } from './copies';
import { getOrders, reportWhoHasMostOrders } from './orders';
import { getMessagesPerChat } from './messages';
import { createUsers } from './users';
import { initCollections } from '../collections/init-collections';
import { getChats } from '../fixtures/chats';
import { Log } from '../lib/logger';
import { getBooks } from '../fixtures/books';

export default function (){
    if (process.env.RESET_DB) {
        let start = Date.now();
        // resetDatabase({ excludedCollections: [ 'books' ] });
        // Log.w('All collections are reset, excluded books.');
        initCollections();
        createUsers(5);
        getBooks(10);
        createCopiesPerUser(2);
        getOrders(30);
        getChats(1);
        getMessagesPerChat(1);
        reportWhoHasMostCopies();
        reportWhoHasMostOrders();
        let end = Date.now();
        let spent = end - start;
        Log.l(`mounting fixture took ${spent} ms.`);
    }
}
