import { Copies } from '../collections/copies';
import { getBook } from './books';
import { Users } from '../collections/users';
import { User } from '../models/user';
import { Factory } from './factory';
import { getRandomStatus } from './util';
import { getUsers } from './users';
import { Fixture, faker } from './fixture';
import { Copy } from '../models/copy';
import { Log } from '../lib/logger';

const fixture = new Fixture(Copies);

export const getCopy = fixture.getOne.bind(fixture);
export const getCopies = fixture.getMany.bind(fixture);
export const createCopies = fixture.makeMany.bind(fixture);

export function createCopiesPerUser(copiesPerUser: number = 10) {
    const userCount = Users.find().count();
    this.createCopies(userCount * copiesPerUser);
}

Factory.define(Copy, function mockup() {
    const [ user0, user1 ] = getUsers(2);
    return {
        bookId: getBook()._id,
        ownerId: user0._id,
        borrowerId: user1._id,
        communityIds: user0.communityIds,
        status: getRandomStatus.bind(null, 4),
        createdAt: faker.date.past(),
    };
});

export function reportWhoHasMostCopies() {
    let no1: User,
        no1CopiesCount = 0;
    User.find({}, { fields: { phone: 1, profile: 1 } }).forEach(<User>(user) => {
        const count = Copies.find({ ownerId: user._id }).count();
        if (count > no1CopiesCount) {
            no1 = user;
            no1CopiesCount = count;
        }
    });
    Users.update(no1._id, { $set: { 'phone.number': '+8613333344444' } });
    if (no1)
        Log.i(
            `用户: ${(no1.profile && no1.profile.name) ||
                ''}(ID: ${no1._id}, 手机: 13333344444 ) 藏书最多(${no1CopiesCount}册).`,
        );
    return no1;
}
