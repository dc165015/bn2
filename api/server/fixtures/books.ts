import { HTTP } from 'meteor/http';
import { Book, BookSource } from '../models/book';
import { Books } from '../collections/books';
import { Fixture } from './fixture';
import { Log } from '../lib/logger';
import { Meteor } from 'meteor/meteor';

let timer = 1000,
    i = 0;

const fixture = new Fixture(Books);
fixture.makeOne = makeDoubanBook;

export const getBook = fixture.getOne.bind(fixture);
export const getBooks = fixture.getMany.bind(fixture);

function getBookUrl(id?: ID) {
    id = id || randomBookId();
    return `https://api.douban.com/v2/book/${id}`;
}

function randomBookId() {
    const seed = Math.random();
    return '1' + Math.floor(seed * 1000000);
}

const syncHttpGet = Meteor.wrapAsync(HTTP.get, HTTP);

function syncGetDoubanBook(id?: ID) {
    let res;
    try {
        const url = getBookUrl(id);
        res = syncHttpGet(url, { timeout: 5000 });
    } catch (err) {
        handleError(err);
        return;
    }
    return res.data;
}

function asyncGetDoubanBook(id?: ID) {
    HTTP.call('GET', getBookUrl(id), {}, (err, res) => {
        if (err) {
            handleError(err);
        } else {
            insertBook(res.data);
        }
    });
}

function handleError(err): number {
    const code = err.response.statusCode;
    if (code == 400) {
        // 超出每小时150次限制，设置后续等待时间
        timer = 60 * 60 * 1000 - timer * 150;
        Log.w(err.message);
    } else if (code != 404) Log.e(err);
    else Log.w('not found.');
    return code;
}

// 参考豆瓣api限制： https://developers.douban.com/wiki/?title=api_v2
// 每小时150次
function intervalRequest(count: number = 1) {
    Meteor.setTimeout(() => {
        timer = 1000;
        makeDoubanBook();
        if (--count) intervalRequest(count);
    }, timer);

    return {} as any;
}

function makeDoubanBook() {
    Log.l(`Try #${i++}: `);
    const doc = syncGetDoubanBook();
    if (doc) return insertBook(doc);
}

function insertBook(doc) {
    let book = Books.findOne({ id: doc.id });
    if (book) return book;

    book = Book.wrapDoc({ ...doc, source: BookSource.DOUBAN });
    const bookId = Books.insert(book);
    Log.i(`${book.title} inserted.`);
    Log.p(`total ${Books.find().count()} books.`);
    return Books.findOne(bookId);
}
