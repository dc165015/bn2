import { Chats } from '../collections/chats';
import { randomAvatarUrl } from './util';
import { Factory } from "./factory";
import { getUser } from './users';
import { Fixture, faker } from './fixture';
import { Chat } from '../models/chat';
import { flatten, uniq } from 'lodash';

const fixture = new Fixture(Chats);
export const getChat = fixture.getOne.bind(fixture);
export const getChats = fixture.getMany.bind(fixture);
export const createChats = fixture.makeMany.bind(fixture);

Factory.define(Chat, function mockup() {
    return {
        createdAt: faker.date.past(),
        memberIds: getUniqMemberIds()
    }
});

function getUniqMemberIds() {
    const existedMemberIdsArray = Chats.find({}).map(chat => chat.memberIds);
    let ids = uniq(flatten(existedMemberIdsArray))
    const user0 = ids.length ? getUser({ _id: { $in: ids } }) : getUser();
    const user1 = getUser({ _id: { $nin: ids.concat(user0._id) } });
    if (!user0 || !user1) throw new Error('用户数量不足以创建所要求数量且"不重复"的聊天二人组');
    return [user0._id, user1._id];
}
