import { DocModel } from '../models/doc-model';
import { getRandomDocs } from './util';
import { DocCollection } from '../collections/doc-collection';
import { Factory } from './factory';
import { faker } from 'meteor/practicalmeteor:faker';
import { Log } from '../lib/logger';

class Fixture<T extends DocModel> {
    static registry: Map<DocCollection<any>, Fixture<any>> = new Map();

    static get(Collection: DocCollection<any>) {
        return this.registry.get(Collection);
    }

    Model: Constructor<T>;
    Name: string;

    constructor(public Collection: DocCollection<T>) {
        this.Model = this.Collection.Model;
        this.Name = this.Model.name;

        (<typeof Fixture>this.constructor).registry.set(Collection, this);
    }

    getOne(selector?: MongoSelector<T>): T {
        let doc = this.getMany(1, selector)[0];
        if (!doc) doc = this.makeOne();
        return doc;
    }

    makeOne(): T {
        return Factory.create(this.Name, this.makeOverwritePartial());
    }

    getMany(count: number = 2, selector: MongoSelector<T> = {}): T[] {
        let existedCount = this.Collection.find(selector).count();
        let toCreateCount = count - existedCount;
        if (toCreateCount > 0) {
            this.makeMany(toCreateCount);
            return this.getMany(count, selector);
        }
        return getRandomDocs(this.Collection, count, selector);
    }

    makeMany(count: number = 2): T[] {
        const ret = [];
        Log.i(`create ${count} ${this.Name.toLocaleLowerCase()}`);
        while (count--) ret.push(this.makeOne());
        return ret;
    }

    // this function is to be replaced
    makeOverwritePartial() {
        return {};
    }
}

faker.locale = 'zh_CN';
export { Fixture, faker };
