import { Random } from 'meteor/random';
import { Factory } from './factory';
import { Users } from '../collections/users';
import { importAvatarFromUrl } from './util';
import { MAX_NAME_LENGTH, MAX_DESCRIPTION_LENGTH } from '../lib/constants';
import { User, Profile } from '../models';
import { Fixture, faker } from './fixture';

const fixture = new Fixture(Users);
export const getUser = fixture.getOne.bind(fixture);
export const createUsers = fixture.makeMany.bind(fixture);
export const getUsers = function(count = 2, selector?) {
    return fixture.getMany(count + 1, selector).filter((user) => user._id != User.System._id);
};

Factory.define(User, function mockup() {
    const picture = importAvatarFromUrl();
    return {
        phone: {
            number: faker.phone.phoneNumber('+861##########'),
            verified: Math.random() >= 0.5,
        },

        profile: {
            ...new Profile(),
            name: faker.name.findName().substr(0, MAX_NAME_LENGTH),
            description: faker.lorem.paragraphs(3).substr(0, MAX_DESCRIPTION_LENGTH),
            pictureId: picture._id,
        },

        services: {
            password: { bcrypt: Random.id(17) },
            resume: { loginTokens: [ { when: faker.date.past() } ] },
        },

        createdAt: faker.date.past(),
    };
});
