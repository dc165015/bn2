import { Factory } from './factory';
import { Orders, Users } from '../collections';
import { getRandomItem } from './util';
import { getUsers } from './users';
import { getCopy } from './copies';
import { Fixture } from './fixture';
import { OrderState, Order } from '../models/order';
import { User } from '../models';
import { Log } from '../lib/logger';

const fixture = new Fixture(Orders);

export const getOrder = fixture.getOne.bind(fixture);
export const getOrders = fixture.getMany.bind(fixture);
export const createOrders = fixture.makeMany.bind(fixture);

const makeOne = fixture.makeOne.bind(fixture);
fixture.makeOne = function() {
    let doc: any = makeOne();
    if ([ OrderState.New, OrderState.Cancelled, OrderState.Rejected ].includes(doc.state)) return;
    const order = Order.get(doc._id);
    order.updateStatisticsData();
    order.updateCopyState();
    return doc;
};

Factory.define(Order, function mockup() {
    const [ user0, user1 ] = getUsers(2);
    return {
        ownerId: user0._id,
        borrowerId: user1._id,
        copyId: getCopy({ ownerId: user0._id })._id,
        state: getRandomItem(OrderState.list()),
        updatedBy: user1._id,
    };
});

export function reportWhoHasMostOrders() {
    let no1: User,
        no1OrdersCount = 0;
    User.find({}, { fields: { phone: 1, profile: 1 } }).forEach(<User>(user) => {
        const count = Orders.find({ $or: [ { ownerId: user._id }, { borrowerId: user._id } ] }).count();
        if (count > no1OrdersCount) {
            no1 = user;
            no1OrdersCount = count;
        }
    });
    Users.update(no1._id, { $set: { 'phone.number': '+86133333355555' } });
    if (no1)
        Log.i(
            `用户: ${(no1.profile && no1.profile.name) ||
                ''}(ID: ${no1._id}, 手机: 13333355555 ) 订单最多(${no1OrdersCount}单).`,
        );
    return no1;
}
