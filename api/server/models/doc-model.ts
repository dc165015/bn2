/**
 * This file defines the base model in ORM.
 * all internal members which its name starts with _ should not be saved into DB except _id field.
 */
import { DocCollection } from '../collections/doc-collection';
import { Meteor } from 'meteor/meteor';
import { ObservableCursor } from 'meteor-rxjs';
import { isObject } from 'lodash';
import { Mongo } from 'meteor/mongo';

export interface DocModelClassInterface {
    new (...args): DocModel;
    _collection;
    getCollection;
    wrapDoc;
    get;
    find;
    find$;
    findOne;
    findByIds;
    findByIds$;
}

export class DocModel {
    static _collection: DocCollection<any>;

    static getCollection<T extends DocModelClassInterface> (this: T): DocCollection<InstanceType<T>> {
        return this._collection || (this._collection = Meteor.collections.get(this));
    }

    static wrapDoc<T extends DocModelClassInterface> (this: T, doc: object | InstanceType<T>) {
        if (isObject(doc) && doc instanceof this) return doc as InstanceType<T>;
        const instance = new this();
        Object.assign(instance, doc);
        return instance as InstanceType<T>;
    }

    static get<T extends DocModelClassInterface> (this: T, id: ID): InstanceType<T> {
        const found = this.findOne(id);
        if (!found) return;
        return this.wrapDoc(found); // do wrapDOc in case server doesn't transform the doc
    }

    static isExisted<T extends DocModelClassInterface> (this: T, selector: MongoSelector<T>): boolean {
        return !!this.find(selector).count();
    }

    // reference: https://stackoverflow.com/a/51859905/9087103
    static find<T extends DocModelClassInterface> (
        this: T,
        selector?: MongoSelector<InstanceType<T>>,
        options?: MongoFindOptions,
    ) {
        return this.getCollection().find(selector, options) as Mongo.Cursor<InstanceType<T>>;
    }

    static find$<T extends DocModelClassInterface> (
        this: T,
        selector?: MongoSelector<InstanceType<T>>,
        options?: MongoFindOptions,
    ) {
        return this.getCollection().find$(selector, options) as ObservableCursor<InstanceType<T>>;
    }

    static findOne<T extends DocModelClassInterface> (
        this: T,
        selector?: MongoSelector<InstanceType<T>>,
        options?: MongoFindOptions,
    ) {
        return this.getCollection().findOne(selector, options) as InstanceType<T>;
    }

    static findByIds<T extends DocModelClassInterface> (
        this: T,
        docIds: (string | Mongo.ObjectID)[],
        options?: MongoFindOptions,
    ) {
        return this.find({ _id: { $in: docIds } }, options) as Mongo.Cursor<InstanceType<T>>;
    }

    static findByIds$<T extends DocModelClassInterface> (
        this: T,
        docIds: (string | Mongo.ObjectID)[],
        options?: MongoFindOptions,
    ) {
        return this.find$({ _id: { $in: docIds } }, options) as ObservableCursor<InstanceType<T>>;
    }

    static update<T extends DocModelClassInterface> (
        this: T,
        selector: MongoSelector<InstanceType<T>>,
        modifier: Mongo.Modifier<InstanceType<T>>,
        options?: { multi?: boolean; upsert?: boolean },
        callback?: (error: any, result: any) => any,
    ) {
        return this.getCollection().update(selector, modifier, options, callback) as number;
    }

    _id?: ID;
    createdAt?: number;
    updatedAt?: number;

    getCollection<T extends DocModel> (this: T): DocCollection<T> {
        return Meteor.collections.get(this.constructor);
    }

    getRaw () {
        const doc = {};
        for (let key of Object.keys(this)) {
            // skip all internal members which its name starts with _ except _id
            if (key.startsWith('_') && key != '_id') continue;
            const value = this[key];
            if (value instanceof Function) return;
            doc[key] = value;
        }
        return doc;
    }

    insert (callback?: (error: any, result: any) => any) {
        return this.getCollection().insert(this.getRaw() as any, callback);
    }

    remove (callback?: (error: any, result: any) => any) {
        return this.getCollection().remove(this._id, callback);
    }

    update<T extends DocModel> (
        this: T,
        modifier: Mongo.Modifier<T>,
        options?: { multi?: boolean; upsert?: boolean },
        callback?: (error: any, result: any) => any,
    ) {
        return this.getCollection().update(this._id, modifier, options, callback);
    }

    upsert<T extends DocModel> (
        this: T,
        modifier: Mongo.Modifier<T>,
        options?: { multi?: boolean; upsert?: boolean },
        callback?: (error: any, result: any) => any,
    ) {
        return this.getCollection().upsert(this._id, modifier || { $set: this }, options, callback);
    }
}
