export class Rating {
    max?: number = 10;
    numRaters?: number = 0;
    average?: number = 0;
    min?: number = 0;

    rate ({ points, rater }) {
        let _points = Number(points);

        if (_points > this.max || _points < this.min)
            throw new Meteor.Error(
                'Illigal Rate',
                'exceeded max/min scope',
                `${points} exceeds scope [${this.min} - ${this.max}]`,
            );

        this.numRaters++;
        this.average = this.total + points;
    }

    get total () {
        return this.numRaters * this.average;
    }
}

export class Protocol {
    // 默认租期多少天
    duration?: number = 7;

    // TODO: @v2 加入租押金功能
    // dayRent?: number;
    // deposit?: number;
    // overdueRate?: number;
    // dayDonate?: number;

    terms?: string;
}

export class Tag {
    count?: number;
    relatedTags?: string[];

    createdAt?: number;
    createdBy?: ID;

    constructor (public name: string) {
        this.count = 1;
        this.createdAt = Date.now();
        this.createdBy = Meteor.userId();
    }

    get readonly () {
        return this.createdBy == Meteor.userId();
    }
}
