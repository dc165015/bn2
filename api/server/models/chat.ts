import { DocModel } from './doc-model';
import { User } from './user';
import { Message } from '../models/message';

export class Chat extends DocModel {
    static findOneByMemberIds (...ids: ID[]) {
        return Chat.find({ memberIds: { $all: ids } }).fetch()[0];
    }

    static findOrCreateByMemberIds (...ids: ID[]) {
        let chat = this.findOneByMemberIds.apply(this, ids);
        return chat ? chat._id : new this(ids).insert();
    }

    lastMessageId?: ID;

    constructor (public memberIds: ID[]) {
        super();
    }

    get lastMessage () {
        return Message.get(this.lastMessageId);
    }

    get counterpart () {
        const loginUserId = Meteor.userId();
        const counterpartId = this.memberIds.find((id) => id != loginUserId);
        return User.get(counterpartId);
    }

    get title () {
        return (this.counterpart && this.counterpart.nickname) || '';
    }

    get picture () {
        return (this.counterpart && this.counterpart.avatar) || '';
    }

    static get notificationId () {
        const loginUserId = Meteor.userId();
        if (!loginUserId) return;
        const notification = this.findOne({
            memberIds: {
                $all: [
                    User.System._id,
                    loginUserId,
                ],
            },
        });
        return notification && notification._id;
    }
}
