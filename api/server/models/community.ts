import { DocModel } from './doc-model';
import { switchMap } from 'rxjs/operators';
import { User } from '../models/user';
import { Copy } from './copy';
import { Book } from './book';
import { ObservableCursor } from 'meteor-rxjs';

export enum CommunityType {
    NONE = 0,
    ALL = 1,
    ANY = 2,
    DEFAULT = 10,
    CLASS = 20,
    GRADE,
    SCHOOL = 25,
    UNIVERSITY,
    DEPARTMENT = 30,
    COMPANY,
    RESIDENTIAL_QUARTER = 40,
    DISTRICT,
    CITY,
    FRIENDS = 50,
    TOPIC = 100,
}

export class Community extends DocModel {
    // TODO: @v1: @high 加入年级、学校
    type: CommunityType;
    name: string;
    description?: string;
    createdBy?: ID;
    updatedBy?: ID;
    memberCount?: number = 0;
    copyCount?: number = 0;
    lentCount?: number = 0;
    borrowedCount?: number = 0;

    // TODO: @v1: 加入新申请成员列表
    // applicantIds?: ID[];
    // isPublic: Boolean;

    // parentId?: ID;
    //
    // state: string;

    // TODO: @v2: 加入成员间借阅订单数量
    // orderCount?: number = 0;

    constructor(name?: string, type: CommunityType = CommunityType.DEFAULT) {
        super();
        this.name = name;
        this.type = type;
    }

    get averageOwnCopyCount() {
        return this.memberCount ? this.copyCount / this.memberCount : 0;
    }

    get members$(): ObservableCursor<User> {
        return User.find$({ communityIds: this._id });
    }

    get members() {
        return User.find({ communityIds: this._id });
    }

    get copies$() {
        return this.members$.pipe(switchMap((users) => Copy.findByOwners$(users)));
    }

    get copies() {
        return Copy.findByOwners(this.members);
    }

    get books$() {
        return this.copies$.pipe(switchMap((copies) => Book.findByCopies$(copies)));
    }

    get books() {
        return Book.findByCopies(this.copies);
    }

    get myCommunityName() {
        if (!Meteor.userId()) return this.name;
        return User.Me.getCommunityName(this);
    }

    // 使用含有.的特殊的ID，以便未来可以使用 类似分级域名的方式 来分级社群。 比如：.GOOGLE.CN 表示谷歌中国社群;
    // ALL.* 表示全局公共社群, 所有用户都属于此群；
    static get All() {
        const _id = 'all.*';
        return Community.get(_id) || (Object.assign(new Community('公共群', CommunityType.ALL), { _id }) as Community);
    }

    // NONE. 表示不在某群的用户群, 可以用于表示黑名单；
    // NONE.* 表示系统黑名单
    static get None() {
        const _id = 'none.*';
        return Community.get(_id) || (Object.assign(new Community('黑户', CommunityType.ALL), { _id }) as Community);
    }

    // TOBE. 表示申请加入到某群的用户群，比如：TOBE.5hi348dh4kok8fhqad 表示 _id为5hi348dh4kok8fhqad 的群中在申请加入中的用户群
    // TOBE.* 表示未注册的访客
    static get Tobe() {
        const _id = 'tobe.*';
        return Community.get(_id) || (Object.assign(new Community('申请', CommunityType.ALL), { _id }) as Community);
    }
}
