import { Rating } from './share-models';
import { DocModel } from './doc-model';
import { Copy } from './copy';
import { Community } from './community';
import { map, switchMap } from 'rxjs/operators';
import { Meteor } from 'meteor/meteor';
import { Order } from '../models/order';
import { Book } from './book';
import { ObservableCursor } from 'meteor-rxjs';
import { DEFAULT_TERMS, DEFAULT_PICTURE_URL } from '../lib/constants';
import { Log } from '../lib/logger';
import { Picture } from '../models/picture';

export class Profile {
    name?: string;
    description?: string;
    pictureId?: ID;
    avatar?: string; // auto updated by pictures collection after upload avatar
    copyCount: number = 0;
    borrowedCount: number = 0;
    borrowedRating?: Rating = new Rating();
    lentCount: number = 0;
    lentRating?: Rating = new Rating();
}

export class Phone {
    number: string;
    verified: boolean;
}

export class UserSettings {
    defaultTerms?: string;
}

export class User extends DocModel {
    profile?: Profile = new Profile();
    phone?: Phone = new Phone();
    communityIds?: string[] = [
        Community.All._id,
    ];
    services?: object = {};
    settings?: UserSettings = new UserSettings();
    invitationCodes?: { communityId: ID; createdAt: number }[] = [];
    communitySettings?: { communityId: ID; communityName: string; communityDescription: string; myName: string }[] = [];

    get isMe (): boolean {
        return this._id == Meteor.userId();
    }

    get isProfiled (): boolean {
        return !!this.profile.name;
    }

    get avatar () {
        return this.profile.avatar || Picture.platformPrefix + DEFAULT_PICTURE_URL;
    }

    get rating () {
        return this.profile.borrowedRating || new Rating();
    }

    get nickname () {
        if (this.profile.name) return this.profile.name;
        if (!this.phone || !this.phone.number) return '无名';
        return this.phone.number.replace(/^(?:\+86)?(\d)\d*(\d)$/, '$1**$2');
    }

    get terms () {
        return this.settings.defaultTerms || DEFAULT_TERMS;
    }

    getCopies$ (selector?: MongoSelector<Copy>, options?: MongoFindOptions) {
        selector = Object.assign({}, selector, { ownerId: this._id });
        return Copy.find$(selector, options);
    }

    getCopies (selector?: MongoSelector<Copy>, options?: MongoFindOptions) {
        return this.getCopies$(selector, options).cursor;
    }

    get copies$ () {
        return this.getCopies$();
    }

    get copies () {
        return this.getCopies();
    }

    get books$ () {
        return this.copies$.pipe(switchMap((copies) => Book.findByCopies$(copies)));
    }

    get books () {
        return Book.findByCopies(this.copies);
    }

    getOrders$ (selector?: MongoSelector<Copy>, options?: MongoFindOptions) {
        selector = Object.assign({}, selector, {
            $or: [
                { ownerId: this._id },
                { borrowerId: this._id },
            ],
        });
        return Order.find$(selector, options);
    }

    getOrders (selector?: MongoSelector<Copy>, options?: MongoFindOptions) {
        return this.getOrders$(selector, options).cursor;
    }

    get orders$ () {
        return this.getOrders$();
    }

    get orders () {
        return this.orders$.cursor.map((order) => order);
    }

    get iLentOrders$ () {
        const loginUserId = Meteor.userId();
        let selector = this.isMe ? { ownerId: loginUserId } : { ownerId: loginUserId, borrowerId: this._id };
        Log.l('finding iLentOrders: ', selector);
        return Order.find$(selector);
    }

    get iLentOrders () {
        return this.iLentOrders$.cursor;
    }

    get iBorrowedOrders$ () {
        const loginUserId = Meteor.userId();
        let selector = this.isMe ? { borrowerId: loginUserId } : { borrowerId: loginUserId, ownerId: this._id };
        Log.l('finding iBorrowedOrders: ', selector);
        return Order.find$(selector);
    }

    get iBorrowedOrders () {
        return this.iBorrowedOrders$.cursor;
    }

    get communities$ (): ObservableCursor<Community> {
        return Community.find$({ _id: { $in: this.communityIds } });
    }

    get communities () {
        return this.communities$.cursor;
    }

    // 返回默认的社群
    get community () {
        return Community.findOne(this.communityId);
    }

    // 返回默认的社群ID, communityIds最后一个为默认id
    get communityId () {
        return this.communityIds[this.communityIds.length - 1];
    }

    // 设置默认社群ID, communityIds最后一个为默认id
    set communityId (id: ID) {
        const index = this.communityIds.indexOf(id);
        if (index == -1) return;
        this.communityIds.splice(index, 1);
        this.communityIds.push(id);
    }

    borrow (copy: Copy) {
        return Meteor.call$('requestBorrow', copy._id);
    }

    findCompaniesByBookId$ (bookId: ID) {
        const communityIds = User.Me.communityIds;
        return Copy.find$({ bookId }).pipe(
            map((copies) => copies.map((copy) => copy.ownerId)),
            map((ownerIds) => User.find({ _id: { $in: ownerIds }, communityIds: { $in: communityIds } })),
        );
    }

    findCompaniesByBookId (bookId: ID) {
        const communityIds = User.Me.communityIds;
        let ownerIds = Copy.find({ bookId }).map((copy) => copy.ownerId);
        return User.find({ _id: { $in: ownerIds }, communityIds: { $in: communityIds } });
    }

    getCommunityName (community: Community) {
        let name: string;
        const communityId = community._id;
        const item = this.communitySettings.find((item) => item.communityId == communityId);
        if (item) name = item.communityName;
        if (!name) name = community.name;
        return name;
    }

    // system.* 代表系统用户，用于发送系统通知；
    static get System () {
        const _id = 'system.*';
        return (
            User.get(_id) ||
            (Object.assign(new User(), {
                _id,
                communityIds: [],
                profile: { name: '系统', avatar: '/assets/icon/favicon.ico' },
            }) as User)
        );
    }

    static findByOrders (orders: Mongo.Cursor<Order>, options?: MongoFindOptions) {
        const parties = new Set();
        orders.map((order) => {
            parties.add(order.borrowerId);
            parties.add(order.ownerId);
        });
        const userIds = Array.from(parties);
        return User.find({ _id: { $in: userIds } }, options);
    }

    static findByCommunityIds (communityIds: ID[], options?: MongoFindOptions) {
        return User.find({ communityIds: { $in: communityIds } }, options);
    }

    static isMe (id: ID) {
        return id && Meteor.userId() == id;
    }

    static get Me () {
        return Meteor.user() as User;
    }
}
