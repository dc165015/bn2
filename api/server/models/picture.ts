import { DocModel } from './doc-model';
import { DEFAULT_PICTURE_URL } from '../lib/constants';
import { User } from './user';

export class Picture extends DocModel {
    complete?: boolean;
    extension?: string;
    name?: string;
    progress?: number;
    size?: number;
    store?: string;
    token?: string;
    type?: string;
    uploadedAt?: Date;
    uploading?: boolean;
    url?: string;
    userId?: ID;
    bookId?: ID;
    copyId?: ID;
    for?: string;

    static FOR = {
        USER_AVATAR: 'user-avatar',
        BOOK_SHOT: 'book-shot',
        BOOK_COVER: 'book-cover',
        CHAT_MESSAGE: 'chat-message',
    };

    // this prop may be replaced on client according to its platform specific path.
    static platformPrefix = '';

    // Gets picture's url by a given selector
    static getPictureUrl(selector) {
        const picture = selector ? Picture.findOne(selector) : undefined;
        return picture ? picture.url : DEFAULT_PICTURE_URL;
    }

    static findAvatars(users: CanMap<User>, options?: MongoFindOptions) {
        const avatarIds = users.map((user) => user.profile && user.profile.pictureId);
        return Picture.findByIds(avatarIds, options);
    }
}
