import { DocModel } from './doc-model';
import { Copy } from './copy';
import { Meteor } from 'meteor/meteor';
import { Book } from '../models/book';
import { User } from './user';
import { MessageType } from './message';
import { StateMachine, FiringMode } from 'stateless';
import { notify } from '../lib/notify';
import { Community } from './community';
import { mapTo } from 'rxjs/operators';
import { Log } from '../lib/logger';

export enum OrderState {
    // TODO: add negotiating, delivering states @v2
    New = '正在申请',
    Acting = '正在阅读',
    Completed = '书已归还', // v1功能简单，借书人点击还书即表示订单完成；后续加入还书纠纷和租金押金功能
    Rejected = '被拒申请',
    Cancelled = '撤销申请',
    Lost = '书已遗失',
}

export namespace OrderState {
    export function list() {
        const values = [];
        for (let key in this) {
            const value = this[key];
            if (typeof value == 'function') continue;
            values.push(value);
        }
        return values;
    }

    export function sort(list: OrderState[]) {
        const sorted = [];
        for (let key of Object.keys(OrderState)) {
            const defined = OrderState[key];
            for (let item of list) {
                if (item == defined) sorted.push(item);
            }
        }
        return sorted;
    }
}

export enum OrderOperation {
    RequestBorrow = '再次求借', // create order
    ApproveBorrow = '同意出借',
    RejectBorrow = '拒绝出借',
    CancelBorrow = '取消申请',
    Return = '确认还书',
    Lost = '遗失申报',
}

export namespace OrderOperation {
    export function list() {
        return [
            OrderOperation.RequestBorrow,
            OrderOperation.ApproveBorrow,
            OrderOperation.RejectBorrow,
            OrderOperation.CancelBorrow,
            OrderOperation.Return,
            OrderOperation.Lost,
        ];
    }
}

export class Order extends DocModel {
    static findByUsers(users: Mongo.Cursor<User>, options?: MongoFindOptions) {
        const userIds = users.map((user) => user._id);
        return Order.find({ _id: { $in: userIds } }, options);
    }

    protected state: OrderState = OrderState.New;
    protected readonly _machine: StateMachine<OrderState, OrderOperation>;
    protected _isSuccesful: boolean = false;

    updatedBy: ID;
    bookId: ID;
    readonly ownerId: ID;
    readonly updateHistory: { state: OrderState; updatedAt: number; updatedBy: ID }[] = [];

    // 冗余字段，用于查询
    title: string;
    ownerName: string;
    borrowerName: string;
    terms: string;

    constructor(public borrowerId: ID, public copyId: ID) {
        super();
        const copyDoc = Copy.findOne(copyId) as Copy;
        const copy = Copy.wrapDoc(copyDoc);

        this.bookId = copy.bookId;
        this.ownerId = copy.ownerId;

        this._machine = new StateMachine<OrderState, OrderOperation>(
            {
                mutator: (value) => (this.currentState = value),
                accessor: () => this.currentState,
            },
            FiringMode.Immediate,
        );
        this.setupStateMachine();
    }

    get copy() {
        return Copy.get(this.copyId);
    }

    get book() {
        return Book.get(this.copy.bookId);
    }

    get borrower() {
        return User.get(this.borrowerId);
    }

    get owner() {
        return User.get(this.ownerId);
    }

    get lender() {
        return this.owner;
    }

    get amIBorrower() {
        return this.borrowerId == Meteor.userId();
    }

    get amIOwner() {
        return this.ownerId == Meteor.userId();
    }

    get currentState(): OrderState {
        return this.state;
    }

    set currentState(value: OrderState) {
        Log.l('------------------------');
        Log.l(`Order Id : ${this._id}`);
        Log.l(`Previous state : ${this.state}`);
        const { state, updatedAt, updatedBy } = this;
        const historyEntry = { state, updatedAt, updatedBy };
        this.update<Order>({ $set: { state: value }, $push: { updateHistory: historyEntry } });
        this.state = value;
        Log.i(`New state : ${this.state}`);
    }

    private setupStateMachine() {
        const mustBeIdle = { guard: () => this.copy && this.copy.isIdle, description: '所借书籍必须是闲置状态' };
        const mustBeBorrower = { guard: () => this.amIBorrower, description: '必须是借书者本人才能操作' };
        const mustBeOwner = { guard: () => this.amIOwner, description: '必须是书主本人才能操作' };
        const mustBeAParty = { guard: () => this.amIBorrower || this.amIOwner, description: '必须是交易双方中的一方才能操作' };

        this._machine
            .configure(OrderState.New)
            .onEntry(() =>
                notify(`${this.borrower.nickname}向你借阅《${this.copy.title}》`, this.ownerId, MessageType.ORDER, this._id),
            )
            .permitReentryIf(OrderOperation.RequestBorrow, [ mustBeBorrower, mustBeIdle ], '借书人可再次向书主求借')
            .permitIf(OrderOperation.CancelBorrow, OrderState.Cancelled, [ mustBeBorrower ], '借书人可取消借书请求')
            .permitIf(OrderOperation.ApproveBorrow, OrderState.Acting, [ mustBeOwner, mustBeIdle ], '书主同意出借')
            .permitIf(OrderOperation.RejectBorrow, OrderState.Rejected, [ mustBeOwner ], '书主拒绝出借');

        this._machine
            .configure(OrderState.Acting)
            .onEntryFrom(OrderOperation.ApproveBorrow, () => {
                this.copy.setActing();
                this.updateStatisticsData();
                notify(`${this.owner.nickname}已同意出借《${this.copy.title}》`, this.borrowerId, MessageType.ORDER, this._id);
            })
            .permitIf(OrderOperation.Return, OrderState.Completed, [ mustBeAParty ], '借书人或书主可确认书已归还书主')
            .permitIf(OrderOperation.Lost, OrderState.Lost, [ mustBeAParty ], '借书人或书主可确认书已遗失');

        this._machine
            .configure(OrderState.Completed)
            .onEntry(() => this.copy.setIdle())
            .onEntry(() =>
                notify(`${this.borrower.nickname}已归还《${this.copy.title}》`, this.ownerId, MessageType.ORDER, this._id),
            );

        this._machine
            .configure(OrderState.Lost)
            .onEntry(() => this.copy.setLost())
            .onEntry(() =>
                notify(`${this.borrower.nickname}申报遗失了《${this.copy.title}》`, this.ownerId, MessageType.ORDER, this._id),
            );

        this._machine
            .configure(OrderState.Rejected)
            .onEntry(() =>
                notify(`${this.owner.nickname}已拒绝出借《${this.copy.title}》`, this.borrowerId, MessageType.ORDER, this._id),
            );
    }

    async fire(operation: OrderOperation): Promise<any> {
        this._isSuccesful = false;
        await this._machine.fire(operation);
        this._isSuccesful = true;
    }

    get permittedOperations$() {
        return this._machine.getPermittedTriggers();
    }

    execute$(operation: OrderOperation) {
        return Meteor.call$('operateOrder', this._id, operation).pipe(mapTo(`${operation}操作成功！`));
    }

    requestBorrow$() {
        return this.execute$(OrderOperation.RequestBorrow);
    }

    rejectBorrow$() {
        return this.execute$(OrderOperation.RejectBorrow);
    }

    approveBorrow$() {
        return this.execute$(OrderOperation.ApproveBorrow);
    }

    cancelBorrow$() {
        return this.execute$(OrderOperation.CancelBorrow);
    }

    return$() {
        return this.execute$(OrderOperation.Return);
    }

    lost$() {
        return this.execute$(OrderOperation.Lost);
    }

    get isSuccessful(): boolean {
        return this._isSuccesful;
    }

    updateStatisticsData() {
        const { owner, borrower } = this;
        User.update(this.ownerId, { $inc: { 'profile.lentCount': 1 } });
        User.update(this.borrowerId, { $inc: { 'profile.borrowedCount': 1 } });
        Community.update({ _id: { $in: owner.communityIds } }, { $inc: { lentCount: 1 } });
        Community.update({ _id: { $in: borrower.communityIds } }, { $inc: { borrowedCount: 1 } });
    }

    updateCopyState() {
        const copy: Copy = this.copy;
        switch (this.currentState) {
            case OrderState.Acting:
                return this.copy.setActing();
            case OrderState.Lost:
                return this.copy.setLost();
            default:
                return this.copy.setIdle();
        }
    }

    get isRequest() {
        return this._machine.isInState(OrderState.New);
    }

    get isFinished() {
        return this._machine.isInState(OrderState.Completed);
    }

    get isActing() {
        return this._machine.isInState(OrderState.Acting);
    }

    get isTerminated() {
        const TerminatedStates = [ OrderState.Cancelled, OrderState.Rejected, OrderState.Lost ];
        return !!TerminatedStates.find((state) => this._machine.isInState(state));
    }
}
