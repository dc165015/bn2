import { checkStringLength } from '../lib/check';

export class Location {
    // 参考高德地图POI定义
    name: string; // 地点名称
    province: string;
    city: string;
    citycode: string;
    district: string;
    adcode: string;
    township: string;
    street: string;
    streetNumber: string;
    neighborhood: string;
    neighborhoodType: string;
    building: string;
    buildingType: string;

    constructor(
        public lat: number = Location.DEFAULT_LAT,
        public lng: number = Location.DEFAULT_LNG,
        public zoom: number = Location.DEFAULT_ZOOM,
        public accuracy: number = Location.DEFAULT_ACCURACY,
    ) {
        zoom = Math.min(zoom, 19);
    }

    get lnglat(): number[] {
        return [ this.lng, this.lat ];
    }

    get address(): string {
        return [ this.province, this.city, this.district, this.township, this.street, this.streetNumber ].join('');
    }

    toString(): string {
        return `${this.lat}, ${this.lng}, ${this.zoom}`;
    }

    isValid(): boolean {
        return !!this.lat && !!this.lng;
    }

    static EQUATOR = 40075004;
    static DEFAULT_LAT = 31.214876;
    static DEFAULT_LNG = 121.555182;
    static DEFAULT_ZOOM = 14;
    static DEFAULT_ACCURACY = -1;

    static fromString(str) {
        checkStringLength(str);
        const [ lat, lng, zoom = 0 ] = str.split(',').map(Number);
        return new Location(lat, lng, zoom);
    }

    static fromRawDoc(doc) {
        return Object.assign(new Location(), doc);
    }

    static fromCoords(coords, deviceHeight: number, deviceWidth: number) {
        const location = new Location();
        // Update view-models to represent the current geo-location
        location.accuracy = coords.accuracy;
        location.lat = coords.latitude;
        location.lng = coords.longitude;
        location.zoom = this.calculateZoomByAccureacy(location.accuracy, deviceHeight, deviceWidth);

        return location;
    }

    static calculateZoomByAccureacy(accuracy: number, deviceHeight: number, deviceWidth: number): number {
        // Source: http://stackoverflow.com/a/25143326
        const screenSize = Math.min(deviceWidth, deviceHeight);
        const requiredMpp = accuracy / screenSize;

        return Math.log(Location.EQUATOR / (256 * requiredMpp)) / Math.log(2) + 1;
    }
}
