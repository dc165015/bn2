import { DocModel } from './doc-model';
import { Rating, Tag } from './share-models';
import { Copy, CopyState } from './copy';
import { ObservableCursor } from 'meteor-rxjs';
import { Picture } from '../models/picture';
import { MIN_AGE_SETTING, MAX_AGE_SETTING } from '../lib/constants';

class BookPhotos {
    small?: string;
    large?: string;
    medium?: string;
    photoUrls?: string[] = [];
}

export class Series {
    title?: string;
    id?: ID;
}

let statusNo = 0;
export enum BookStatus {
    // 特殊状态
    IDLE = 1 << ++statusNo,
    ERROR = 1 << ++statusNo,
    PHOBIDDEN_CN = 1 << ++statusNo,
}

export enum BookSource {
    OTHER = 'other',
    DOUBAN = 'douban',
    DANGDANG = 'dangdang',
    AMAZON = 'amazon',
}

const CONFUSED_TAGS = [
    '想读，一定很精彩！',
    '好书，值得一读',
    '未分类',
];

export class Book extends DocModel {
    // 用于记录书种的状态：禁书，有误，
    copyCount?: number = 0;
    ownerCount?: number = 0;
    status?: BookStatus = BookStatus.IDLE;
    source?: BookSource;

    title?: string;
    id?: ID; // the id from source.
    isbn10?: string;
    isbn13?: string;
    origin_title?: string;
    alt?: string;
    alt_title?: string;
    subtitle?: string;
    language?: string[] = [];
    image?: string;
    publisher?: string;
    binding?: string;
    author_intro?: string;
    summary?: string;
    catalog?: string;
    url?: string;
    ebook_url?: string;
    ebook_price?: string;
    price?: number;
    priceUnit?: string = '人民币';
    pages?: number;
    words?: number;
    ageRange? = { lower: MIN_AGE_SETTING, upper: MAX_AGE_SETTING };
    series?: Series = new Series();
    pubdate?: string;
    author?: string[] = [];
    translator?: string[] = [];
    images?: BookPhotos = new BookPhotos();
    rating?: Rating = new Rating();
    tags?: Tag[] = [];

    get photos (): string[] {
        return this.images.photoUrls || [];
    }

    addPhoto (url: string) {
        if (url && (url = url.trim())) {
            this.images = this.images || new BookPhotos();
            if (!this.images.photoUrls.includes(url)) this.images.photoUrls.push(url);
        }
    }

    get cover (): string {
        return (this.images && (this.images.large || this.images.photoUrls[0])) || this.image;
    }

    get seriesTitle () {
        this.series = this.series || {};
        return this.series.title;
    }

    set seriesTitle (title: string) {
        this.series = this.series || {};
        this.series.title = title;
    }

    get authors () {
        return this.author.join(', ');
    }

    set authors (names: string) {
        this.author = names.split(',').map((name) => name.trim());
    }

    get translators () {
        return this.translator.join(', ');
    }

    set translators (names: string) {
        this.translator = names.split(', ').map((name) => name.trim());
    }

    get languages () {
        return this.language.join(', ');
    }

    set languages (langs: string) {
        this.language = langs.split(', ').map((lang) => lang.trim());
    }

    get ageRangeIndicator () {
        const { lower, upper } = this.ageRange;
        if (lower == MAX_AGE_SETTING && upper == MAX_AGE_SETTING) return `${MAX_AGE_SETTING}岁以上`;
        if (lower == MIN_AGE_SETTING && upper == MIN_AGE_SETTING) return `${MIN_AGE_SETTING}岁以下`;
        if (lower == MIN_AGE_SETTING && upper != MAX_AGE_SETTING) return upper + '岁以下';
        if (lower != MIN_AGE_SETTING && upper == MAX_AGE_SETTING) return lower + '岁以上';
        return `从 ${lower} 到 ${upper} 岁`;
    }

    getCopies (selector?: MongoSelector<Copy>) {
        return Copy.find({ bookId: this._id, ...<any>selector });
    }

    getIDleCopies (selector?: MongoSelector<Copy>) {
        return this.getCopies({ state: CopyState.IDLE, ...<any>selector });
    }

    getIDleCopiesOfCommunityId (communityId: ID) {
        return this.getIDleCopies({ communityIds: communityId });
    }

    static get Scanner () {
        const _id = 'scanner.*';
        return (
            Book.get(_id) ||
            (Object.assign(new Book(), {
                _id,
                id: _id,
                source: 'system',
                image: Picture.platformPrefix + '/assets/imgs/add-copy.svg',
            }) as Book)
        );
    }

    static fixAll (data: Dictionary<any>) {
        let doc = { ...data };
        doc.price = this.fixPrice(data.price);
        doc.pages = this.fixPages(data.pages);
        doc.tags = this.fixTags(data.tags);
        return doc;
    }

    static fixPrice (price: string) {
        if (price) return price.replace('元', '').trim();
    }

    static fixPages (pages: string) {
        if (pages) return pages.replace('页', '').trim();
    }

    static fixTags (tags: Tag[]) {
        if (!tags || !tags.length) return;
        let tagsMap = new Map();

        tags.forEach((tag, index) => {
            // 过滤不规范标签
            if (CONFUSED_TAGS.indexOf(tag.name) != -1) {
                tags[index] = undefined;
                return;
            }

            // 去除标点符号, . ; ! / *，以及替换全角空格
            let str = (<string>tag.name)
                .replace(/;+|；+|　+|,+|，+|\!+|！+|。+|\/+|\*+/g, ' ')
                // 去除替换后出现的连续的空格
                .replace(/\s+/g, ' ');

            // 以空格为分隔符拆分
            str.split(' ').forEach((tagName) => {
                // 去除无内容的空标签
                if (!tagName) return;

                // 合并此标签的计数
                let count = tagsMap.get(tagName) || 0;
                tagsMap.set(tagName, tag.count + count);
            });
        });

        return Array.from(tagsMap).sort((a, b) => b[1] - a[1]).map((tag) => {
            return { name: tag[0], count: tag[1] };
        });
    }

    static findByCopies$ (copies: CanMap<Copy>): ObservableCursor<Book> {
        return Book.find$({ _id: { $in: copies.map((copy) => copy.bookId) } });
    }

    static findByCopies (copies: CanMap<Copy>, options?: MongoFindOptions): Mongo.Cursor<Book> {
        const copyIds = copies.map((copy) => copy.bookId);
        return Book.find({ _id: { $in: copyIds } }, options);
    }
}
