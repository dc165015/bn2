import { DocModel } from './doc-model';
import { Location } from './location';
import { Chat } from './chat';
import { User } from './user';

export enum MessageType {
    TEXT = <any>'text',
    LOCATION = <any>'location',
    PICTURE = <any>'picture',

    // a reference to a DB document.
    ORDER = <any>'order',
}

export namespace MessageType {
    export function list (){
        return [
            MessageType.TEXT,
            MessageType.LOCATION,
            MessageType.PICTURE,
            MessageType.ORDER,
        ];
    }
}

export class Message extends DocModel {
    didRead: 'yes' | 'no' = 'no';

    constructor (
        public chatId: ID,
        public senderId: ID,
        public content: string,
        public type: MessageType = MessageType.TEXT,
        public referenceId?: ID,
    ) {
        super();
    }

    get ownership () {
        return this.senderId === Meteor.userId() ? 'mine' : 'other';
    }

    get location () {
        const [
            lat,
            lng,
            zoom = 0,
        ] = this.content.split(',').map(Number);
        return <Location>{ lat, lng, zoom: Math.min(zoom, 19) };
    }

    get sender () {
        return User.get(this.senderId);
    }

    get senderName () {
        const sender = this.sender;
        return (sender && sender.nickname) || '';
    }

    toNotification () {
        const isFromSystem = this.senderId == User.System._id;
        let prefix = isFromSystem ? '【通知】' : '【私信】';
        let senderName = this.senderName;
        let postfix = !isFromSystem && senderName ? `(${senderName})` : '';
        return prefix + this.content + postfix;
    }

    static findByChats (chats: CanMap<Chat>, options: MongoFindOptions) {
        return this.findByChatIds(chats.map((chat) => chat._id), options);
    }

    static findByChatIds (chatIds: ID[], options: MongoFindOptions) {
        return Message.find({ chatId: { $in: chatIds } }, options);
    }

    static getUnreadCount (type: 'all' | 'notification' | 'message' = 'all') {
        return this.getUnread(type).count();
    }

    static getUnread(type: 'all' | 'notification' | 'message' = 'all')
    {
        let options = {};
        let notificationId;
        if (type != 'all')
        {
            notificationId = Chat.notificationId;
            if (notificationId)
            {
                if (type == 'message') options = { chatId: { $ne: notificationId } };
                else if (type == 'notification') options = { chatId: notificationId };
            }
        }
        return Message.find(
            { didRead: 'no', senderId: { $ne: Meteor.userId() }, ...options },
            { sort: { createdAt: -1 } },
        );
    }
}
