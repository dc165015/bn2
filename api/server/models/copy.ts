import { DocModel } from './doc-model';
import { User } from '../models/user';
import { Book } from '../models/book';
import { ObservableCursor, MeteorObservable } from 'meteor-rxjs';
import { Order } from './order';

export enum CopyState {
    IDLE = '闲置',
    ACTING = '借出',
    LOST = '遗失',
}

export class Copy extends DocModel {
    readonly bookId: ID;
    readonly ownerId: ID;

    // TODO: @v2 implements terms on each copy.
    terms: string;

    // denormalized fields which equals to owner.communityIds.
    // It can highly improve query performance.
    // e.g. query owners(who are in the same community with the current logined user) against one certain book or query books in one certain community.
    communityIds: ID[];

    protected state?: CopyState = CopyState.IDLE;

    constructor(bookId: ID, ownerId: ID) {
        super();
        this.bookId = bookId;
        this.ownerId = ownerId;
        const owner = User.get(ownerId);
        if (owner) this.communityIds = owner.communityIds;
    }

    get owner() {
        return User.findOne(this.ownerId);
    }

    get isIdle() {
        return this.state == CopyState.IDLE;
    }

    get isActing() {
        return this.state == CopyState.ACTING;
    }

    get isLost() {
        return this.state == CopyState.LOST;
    }

    get book() {
        return Book.findOne(this.bookId);
    }

    get title() {
        return this.book.title;
    }

    protected setState(state: CopyState) {
        this.state = state;
        this.update<Copy>({ $set: { state } });
    }

    setIdle() {
        this.setState(CopyState.IDLE);
    }

    setLost() {
        this.setState(CopyState.LOST);
    }

    setActing() {
        this.setState(CopyState.ACTING);
    }

    borrow$() {
        return MeteorObservable.call<string>('requestBorrow', this._id);
    }

    static findByOwners(owners: CanMap<User>, options?: MongoFindOptions): Mongo.Cursor<Copy> {
        const userIds = owners.map((user) => user._id);
        return Copy.findByOwnerIds(userIds, options);
    }

    static findByOwnerIds(userIds: ID[], options?: MongoFindOptions) {
        return Copy.find({ ownerId: { $in: userIds } }, options);
    }

    static findByOwners$(owners: CanMap<User>, options?: MongoFindOptions): ObservableCursor<Copy> {
        const userIds = owners.map((user) => user._id);
        return Copy.findByOwnerIds$(userIds, options);
    }

    static findByOwnerIds$(userIds: ID[], options?: MongoFindOptions): ObservableCursor<Copy> {
        return Copy.find$({ ownerId: { $in: userIds } }, options);
    }

    static findByOrders(orders: CanMap<Order>, options?: MongoFindOptions) {
        const copyIds = orders.map((order) => order.copyId);
        return Copy.find({ _id: { $in: copyIds } }, options);
    }

    static findByOrders$(orders: CanMap<Order>, options?: MongoFindOptions) {
        const copyIds = orders.map((order) => order.copyId);
        return Copy.find$({ _id: { $in: copyIds } }, options);
    }

    static findByBook$(book: Book, options?: MongoFindOptions) {
        return Copy.find$({ bookId: book._id }, options);
    }
}
