import { requireLogin, checkInteger } from '../lib/check';
import { Communities } from '../collections/communities';
import { User } from '../models/user';
import { publish } from '../lib/publish';
import { Match, check } from 'meteor/check';

publish('membersOfCommunities', function(batchCounter: number = 1, communityIds: ID[]) {
    requireLogin();
    check(communityIds, Match.Maybe([ String ]));
    checkInteger(batchCounter);

    const myCommunityIds = User.Me.communityIds;
    if (!myCommunityIds) return;
    if (communityIds) communityIds = communityIds.filter((id) => myCommunityIds.includes(id));

    const communities = Communities.find({ _id: { $in: communityIds } }, { limit: 3 * batchCounter });
    const options = { limit: 30 * batchCounter, fields: { profile: 1, communityIds: 1 } };
    const users = User.findByCommunityIds(communityIds, options);
    return [ communities, users ];
});
