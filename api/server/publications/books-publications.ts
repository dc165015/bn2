import { Books } from '../collections/books';
import { requireLogin, checkId, checkInteger, checkStringLength } from '../lib/check';
import { Users } from '../collections/users';
import { Community } from '../models/community';
import { Copy } from '../models/copy';
import { Book } from '../models/book';
import { publish } from '../lib/publish';
import { Match, check } from 'meteor/check';
import { Copies } from '../collections';

publish('book', function (batchCounter: number = 1, pattern: { _id?: string; isbn10?: string; isbn13?: string }){
    requireLogin();
    checkInteger(batchCounter);
    check(pattern, { isbn10: Match.Maybe(String), isbn13: Match.Maybe(String), _id: Match.Maybe(String) });

    const { _id, isbn10, isbn13 } = pattern;

    if (_id) {
        return Books.find({ _id });
    }
    else if (isbn13) {
        return Books.find({ isbn13 });
    }
    else if (isbn10) {
        return Books.find({ isbn10 });
    }
});

publish('booksOfCommunity', function (batchCounter: number = 1, communityId){
    requireLogin();
    checkId(communityId);
    checkInteger(batchCounter);

    const community = Community.get(communityId);
    if (!community) throw new Meteor.Error('DOC_NOT_FOUND', 'Not Found By ID', `没有发现指定的社群{${communityId}}`);
    const averageQty = community.averageOwnCopyCount;
    const booksQtyPerBatch = averageQty > 100 ? 100 : averageQty < 30 ? 30 : averageQty; // 每次订阅最少30本，最多100本
    let limit = batchCounter * booksQtyPerBatch;
    // if (limit > 1000) limit = 1000; // 最多订阅1000本
    // throw new Meteor.Error('QUERY_TOO_MANY', 'Query too many books', `获取指定社群${community.name}{${communityId}书籍超过上限1000本}`);

    const users = Users.find(
        { communityIds: communityId },
        { limit, fields: { profile: 1, 'settings.defaultTerms': 1, communityIds: 1 } },
    );
    const copies = Copy.findByOwners(users, { limit });
    const books = Book.findByCopies(copies);

    return [
        books,
        users,
        copies,
    ];
});

publish('searchBook', function (batchCounter: number = 1, value: string){
    requireLogin();
    checkInteger(batchCounter);
    checkStringLength(value);

    const reg = { $regex: value, $options: 'i' };
    let limit = batchCounter * 30;
    limit = batchCounter * 30 > 1000 ? 1000 : limit;
    const res = Books.find(
        {
            $or: [
                { title: reg },
                { subtitle: reg },
                { alt_title: reg },
                { origin_title: reg },
                { author: reg },
                { translator: reg },
                { 'tags.name': reg },
                { isbn13: reg },
                { isbn10: reg },
            ],
        },
        { limit },
    );
    return res;
});

publish('searchMyCopy', function (batchCounter: number = 1, value: string){
    requireLogin();
    checkInteger(batchCounter);
    checkStringLength(value);

    const reg = { $regex: value, $options: 'i' };
    let limit = batchCounter * 30;
    const bookIds = Copies.find({ ownerId: this.userId }, { limit }).map((copy) => copy.bookId);

    const res = Books.find(
        {
            _id: { $in: bookIds },
            $or: [
                { title: reg },
                { subtitle: reg },
                { alt_title: reg },
                { origin_title: reg },
                { author: reg },
                { translator: reg },
                { 'tags.name': reg },
                { isbn13: reg },
                { isbn10: reg },
            ],
        },
        { limit },
    );
    return res;
});
