import { requireLogin, checkId, checkInteger } from '../lib/check';
import { Copy } from '../models/copy';
import { Book } from '../models/book';
import { publish } from '../lib/publish';
import { Copies, Books } from '../collections';

publish('userCopies', function (batchCounter: number, userId: ID){
    requireLogin();
    checkId(userId);
    checkInteger(batchCounter);

    const copies = Copy.findByOwnerIds(
        [
            userId,
        ],
        { limit: 30 * batchCounter },
    );
    const books = Book.findByCopies(copies);

    return [
        books,
        copies,
    ];
});

publish('copy', function (batchCounter: number, bookId: ID, ownerId?: ID){
    requireLogin();
    checkId(bookId);
    check(ownerId, Match.Maybe(String));

    const copies = Copies.find({ bookId, ownerId: ownerId || this.userId });
    const books = Books.find(bookId);

    return [
        copies,
        books,
    ];
});
