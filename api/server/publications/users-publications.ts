import { requireLogin, checkInteger, checkId, checkStringLength } from '../lib/check';
import { Copies } from '../collections/copies';
import { Users } from '../collections/users';
import { Match } from 'meteor/check';
import { publish } from '../lib/publish';
import { Communities } from '../collections/communities';
import { User } from '../models/user';
import { Messages } from '../collections/messages';
import { Chats } from '../collections';
import { Chat } from '../models';
import { MAX_NAME_LENGTH } from '../lib/constants';

publish('loginUserInfo', function (batchCounter: number = 1){
    requireLogin();

    const options = { limit: batchCounter * 30, sort: { createdAt: -1 } };
    const userFields = {
        fields: { settings: 1, phone: 1, communityIds: 1, communitySettings: 1, profile: 1 },
    };
    const users = Users.find(
        {
            _id: {
                $in: [
                    this.userId,
                    User.System._id,
                ],
            },
        },
        { ...options, ...userFields },
    );

    return users;
});

publish('loginUserMessages', function (batchCounter: number = 1){
    requireLogin();

    const options = { limit: batchCounter * 30, sort: { createdAt: -1 } };

    prepareNotifications(this.userId);
    const chats = Chats.find({ memberIds: this.userId }, { ...options, sort: { updatedAt: -1 } });
    const messages = Messages.find({ chatId: { $in: chats.map((chat) => chat._id) } }, options);

    return [
        chats,
        messages,
    ];
});

publish('loginUserCommunities', function (batchCounter: number = 1){
    requireLogin();
    const options = { limit: batchCounter * 30, sort: { createdAt: -1 } };
    const communities = Communities.find({ _id: { $in: User.Me.communityIds } }, options);
    return communities;
});

publish('users', function (batchCounter: number = 1, pattern?: any){
    requireLogin();
    check(pattern, { _id: Match.Maybe(String), profileName: Match.Maybe(String) });
    checkInteger(batchCounter);
    // if (30 * batchCounter > 500) return new Match.Error('获取用户超过500个上限');

    let selector = {};
    if (pattern) {
        let target;
        if ((target = pattern._id)) selector = { _id: target };
        if ((target = pattern.profileName)) selector = { 'profile.name': { $regex: target, $options: 'i' } };
    }

    return Users.find(selector, { limit: 30 * batchCounter, fields: { profile: 1 } });
});

publish('owners', function (batchCounter: number = 1, bookId: ID){
    requireLogin();
    checkInteger(batchCounter);
    checkId(bookId);

    const communityIds = Users.findOne(this.userId).communityIds;
    const copies = Copies.find({ bookId, communityIds: { $in: communityIds } }, { fields: { ownerId: 1 } });
    return Users.find(
        { _id: { $in: copies.map((copy) => copy.ownerId) } },
        { limit: 30 * batchCounter, fields: { profile: 1 } },
    );
});

publish('searchUser', function (batchCounter: number = 1, value: string){
    requireLogin();
    checkInteger(batchCounter);
    checkStringLength(value, 1, MAX_NAME_LENGTH);

    let limit = batchCounter * 30;
    limit = batchCounter * 30 > 1000 ? 1000 : limit;
    return Users.find({ 'profile.name': { $regex: value, $options: 'i' } }, { limit, fields: { profile: 1 } });
});

function prepareNotifications (userId: ID){
    Chat.findOrCreateByMemberIds(User.System._id, userId);
}
