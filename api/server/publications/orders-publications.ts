import { requireLogin, checkInteger, checkId, checkStringLength } from '../lib/check';
import { Orders } from '../collections/orders';
import { User } from '../models/user';
import { Copy } from '../models/copy';
import { Book } from '../models/book';
import { publish } from '../lib/publish';
import { Copies, Books, Users } from '../collections';

publish('order', function(batchCounter: number = 1, orderId: ID) {
    requireLogin();
    checkInteger(batchCounter);
    checkId(orderId);

    const order = Orders.findOne(orderId);
    if (!order) throw new Meteor.Error('RECORD_NOT_FOUND', '未找到指定订单', `未找到指定的订单${orderId}`);

    const { borrowerId, ownerId } = order;
    let counterpartId;
    if (ownerId == this.userId) counterpartId = borrowerId;
    else if (borrowerId == this.userId) counterpartId = ownerId;
    else throw new Meteor.Error('NOT_AUTHORIZED', '非法订单查询', `用户${this.userId}不是订单的交易方`);

    const orders = Orders.find(orderId);
    const copies = Copies.find(order.copyId);
    const books = Books.find(order.bookId);
    const users = Users.find(counterpartId);

    return [ orders, copies, books, users ];
});

publish('orders', function(batchCounter: number = 1, counterpartId: ID) {
    requireLogin();
    checkInteger(batchCounter);
    checkId(counterpartId);

    const loginUserId = this.userId;
    let selector =
        counterpartId == loginUserId
            ? { $or: [ { ownerId: loginUserId }, { borrowerId: loginUserId } ] }
            : {
                  $or: [
                      { ownerId: loginUserId, borrowerId: counterpartId },
                      { ownerId: counterpartId, borrowerId: loginUserId },
                  ],
              };

    const orders = Orders.find(selector, { limit: 30 * batchCounter });
    const users = User.findByOrders(orders, { fields: { profile: 1 } });
    const copies = Copy.findByOrders(orders);
    const books = Book.findByCopies(copies);

    return [ orders, users, copies, books ];
});

publish('searchOrder', function(batchCounter: number = 1, value: string) {
    requireLogin();
    checkInteger(batchCounter);
    checkStringLength(value);

    const reg = { $regex: value, $options: 'i' };
    let limit = batchCounter * 30;
    limit = batchCounter * 30 > 1000 ? 1000 : limit;

    const loginUserId = this.userId;
    const selector0 = { $or: [ { ownerId: loginUserId }, { borrowerId: loginUserId } ] };
    const selector1 = { $or: [ { ownerName: reg }, { borrowerName: reg }, { title: reg } ] };

    const orders = Orders.find({ $and: [ selector0, selector1 ] }, { limit });
    const users = User.findByOrders(orders, { fields: { profile: 1 } });
    const copies = Copy.findByOrders(orders);
    const books = Book.findByCopies(copies);

    return [ orders, users, copies, books ];
});
