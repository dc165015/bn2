import { Chats } from '../collections/chats';
import { requireLogin, checkInteger } from '../lib/check';
import { flatten, uniq } from 'lodash';
import { publish } from '../lib/publish';
import { Message } from '../models/message';
import { Chat } from '../models/chat';
import { User } from '../models/user';
import { Match, check } from 'meteor/check';

publish('chats', function(batchCounter: number = 1, counterPartId: string) {
    requireLogin();
    checkInteger(batchCounter);
    check(counterPartId, Match.Maybe(String));
    if (counterPartId) counterPartId = counterPartId.trim();

    if (counterPartId == this.userId) counterPartId = void 0;

    let selector: MongoSelector<Chat> = {
        memberIds: !counterPartId ? this.userId : [ counterPartId, this.userId ],
    };
    let limit = batchCounter * 30;
    // limit = limit > 1000 ? 1000 : limit;
    let chats = Chats.find(selector, { limit });

    // insert the new chat if not exist.
    if (!chats.count() && counterPartId) {
        let chatId = Chats.insert({ memberIds: [ counterPartId, this.userId ] } as any);
        if (chatId) chats = Chats.find(selector, { limit });
    }

    const messages = Message.findByChats(chats, { limit });
    const userIds = uniq(flatten(chats.map((chat) => chat.memberIds))).filter((userId) => userId != this.userId);
    const users = User.findByIds(userIds, { fields: { profile: 1 } });

    return [ chats, messages, users ];
});
