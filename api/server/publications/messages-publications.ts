import { Messages } from '../collections/messages';
import { requireLogin, checkId, checkInteger } from '../lib/check';
import { Chats } from '../collections/chats';
import { Message } from '../models/message';
import { Match } from 'meteor/check';
import { publish } from '../lib/publish';
import { Mongo } from 'meteor/mongo';

publish('messages', function(batchCounter: number, chatId: ID): Mongo.Cursor<Message> {
    requireLogin();
    checkId(chatId);
    checkInteger(batchCounter);

    const chat = Chats.findOne(chatId);

    if (!chat) return;

    if (!chat.memberIds.includes(this.userId)) {
        throw new Match.Error('不能查询别人的聊天记录');
    }

    return Messages.find({ chatId }, { limit: 30 * batchCounter });
});

publish('messagesAndMarkRead', function(batchCounter: number, chatId: ID): Mongo.Cursor<Message> {
    requireLogin();
    checkId(chatId);
    checkInteger(batchCounter);

    const chat = Chats.findOne(chatId);

    if (!chat) return;

    if (!chat.memberIds.includes(this.userId)) {
        throw new Match.Error('不能查询别人的聊天记录');
    }

    const msgs = Messages.find({ chatId }, { limit: 30 * batchCounter });

    Messages.update({ _id: { $in: msgs.map((msg) => msg._id) } }, { $set: { didRead: 'yes' } }, { multi: true });

    return msgs;
});
