import { MIN_NAME_LENGTH, MAX_NAME_LENGTH, FORBIDDEN_NAMES } from './constants';
import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';

export function requireLogin(reason: string = '用户未登录', msg: string = '请重新登录') {
    if (!Meteor.userId()) throw new Meteor.Error(401, reason, msg);
}

export const NonEmptyString = Match.Where((x) => {
    check(x, String);
    return x.length > 0;
});

export function checkId(Id: ID) {
    return check(Id, NonEmptyString);
}

export function checkInteger(number: number) {
    return check(number, Match.Integer);
}

export function checkEnum(value, list: any[]) {
    return check(value, Match.Where((x) => list.includes(x)));
}

/**
 * check ths string length.
 *
 * @example
 * checkStringLength(str)  it will throw error if str is empty or not a string.
 * checkStringLength(str, 10) it will throw error if str length is not 10.
 * checkStringLength(str, 10, 30) it will throw error if str length is less than 10 or greater than 30.
 *
 * @param str the string to be checked
 * @param minLength the min length. if undefined, it means it requires at least 1 character
 * @param maxLength tha max length. if undefined but minLength is provided then it means it requires the lenth has to be equaled to minLength;
 */
export function checkStringLength(str: string, minLength?: number, maxLength?: number) {
    if (minLength === 0 && !str) return;
    check(str, String);
    const length = str.trim().length;
    if (minLength == void 0) {
        if (!length) throw new Meteor.Error('CHECK_FAILED', '验证出错', '不能是空字符串');
    } else if (maxLength == void 0) {
        if (length != minLength) throw new Meteor.Error('CHECK_FAILED', '验证出错', `"${str}" is not ${minLength}`);
    } else {
        if (length < minLength) throw new Meteor.Error('CHECK_FAILED', '验证出错', `"${str}" 长度小于最小长度 ${minLength}`);
        else if (length > maxLength) throw new Meteor.Error('CHECK_FAILED', '验证出错', `"${str}" 长度大于最大长度 ${maxLength}`);
    }
}

export function checkName(name) {
    checkStringLength(name, MIN_NAME_LENGTH, MAX_NAME_LENGTH);

    const forbiddenFound = FORBIDDEN_NAMES.find((_name: RegExp | string) => {
        return _name instanceof RegExp ? _name.test(name) : name.toLocaleLowerCase() == _name;
    });

    if (forbiddenFound) throw new Meteor.Error('名字太敏感，换一个试试');
}
