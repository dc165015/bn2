import { MessageType } from '../models/message';

export function notify(content: string, receiverId: ID, type: MessageType = MessageType.TEXT, referenceId?: ID) {
    return Meteor.call('notify', content, receiverId, type, referenceId);
}
