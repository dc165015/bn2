export const DEFAULT_PICTURE_URL = '/assets/imgs/avatar.svg';
export const TWILIO_SMS_NUMBERS = '+8613917189885';
export const MIN_NAME_LENGTH = 2;
export const MAX_NAME_LENGTH = 16;
export const MAX_DESCRIPTION_LENGTH = 32;
export const MAX_TERMS_LENGTH = 1000;
export const MAX_MESSAGE_LENGTH = 512;
export const MIN_AGE_SETTING = 3;
export const MAX_AGE_SETTING = 30;

export const DEFAULT_TERMS = `为使书籍能共享给更多的人使用，请遵守如下要求：
1. 禁止在书上折页、涂画、修改、做笔记和其它损坏行为；
3. 按时归还。如需延期，请提前征询本人意见；
4. 如有意外，需向本人说明情况；否则可能会给予差评；
5. 书籍遗失或严重损伤，需照原价赔偿，或经本人同意免除责任；
`;

export const FORBIDDEN_NAMES = [
    /管理员/,
    'admin',
    'root',
    '上帝',
    /系统用户/,
    'system',
];

export const PERIOD_OF_INVITATION_VALIDITY = 7 * 24 * 60 * 60 * 1000;
export const PERIOD_NEW = 24 * 60 * 60 * 1000;
