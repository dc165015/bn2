import { Log } from './logger';

export function publish(name: string, callback: Function) {
    const logger = function() {
        Log.p(`Publish:`, name);
        Log.n(`Arguments:`, arguments);

        let ret = callback.apply(this, arguments) as Array<Mongo.Cursor<any>> | Mongo.Cursor<any>;

        if (!ret) return Log.i(`results: `, ret);

        if (!(ret instanceof Array)) ret = [ ret ];
        const counters = ret.map((c: any) => c._cursorDescription.collectionName + ': ' + c.count());
        Log.i(`results: ${counters.join(' | ')}`);

        return ret;
    };

    Meteor.publish(name, logger);
}
