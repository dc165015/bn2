import { isArray, isFunction } from 'lodash';

export function hasOwn(obj, key) {
    return Reflect.ownKeys(obj).includes(key);
}

export function convertMapOrSetToString(data: Map<any, any> | Set<any>) {
    return JSON.stringify(data, (key: string, value) => {
        value instanceof Map || value instanceof Set ? Array.from(value) : value;
    });
}

// reference: https://www.typescriptlang.org/docs/handbook/mixins.html
export function mixinClasses(derivedCtor: any, baseCtors: any[]) {
    baseCtors.forEach((baseCtor) => {
        Reflect.ownKeys(baseCtor.prototype).forEach((name) => {
            derivedCtor.prototype[name] = baseCtor.prototype[name];
        });
    });
}

export function equalJSON(source, target) {
    return JSON.stringify(source) == JSON.stringify(target);
}

export function splitArray(array: any[], distinctor: (value: any) => boolean): [any[], any[]] {
    const truth = [];
    const fault = [];
    for (let i = 0; i < array.length; i++) {
        const value = array[i];
        distinctor(value) ? truth.push(value) : fault.push(value);
    }
    return [ truth, fault ];
}

export function isEmptyArray(value) {
    if (isArray(value) && value.length == 0) return true;
}

export function isPrimitiveConstructor(func) {
    return [ String, Number, Boolean, Symbol ].includes(func);
}

export function isObjectConstructor(func): func is (...args) => void {
    return isFunction(func) && !isPrimitiveConstructor(func);
}

export function deleteKeys(object, ...args) {
    args.forEach((key) => delete object[key]);
    return object;
}

export function restrictedProperty(
    writable: boolean = false,
    configurable: boolean = false,
    enumerable: boolean = false,
) {
    return function(target: object, propertyKey: string) {
        Object.defineProperty(target, propertyKey, { writable, configurable, enumerable });
    };
}

export function restrictedMethod(
    writable: boolean = false,
    configurable: boolean = false,
    enumerable: boolean = false,
) {
    return function(target: object, propertyKey: string, descriptor: PropertyDescriptor) {
        (descriptor.configurable = configurable),
            (descriptor.enumerable = enumerable),
            (descriptor.writable = writable);
    };
}

export function restrictedAccessor(configurable: boolean = false, enumerable: boolean = false) {
    return function(target: object, propertyKey: string, descriptor: PropertyDescriptor) {
        (descriptor.configurable = configurable), (descriptor.enumerable = enumerable);
    };
}
