import { Log } from './logger';

if (Meteor.isProduction) {
    const SMSClient = require('@alicloud/sms-sdk');
    const smsClient = new SMSClient(Meteor.settings.ali_SMS);

    SMS.phoneTemplates.text = function(user, code) {
        return code;
    };

    SMS.send = function(options: { from; to: string; body: string }) {
        options.to = options.to.replace('+', '00');
        options.to = options.to.replace('0086', '');
        debugger;
        smsClient
            .sendSMS({
                PhoneNumbers: options.to,
                SignName: '星空',
                TemplateCode: 'SMS_145595021',
                TemplateParam: '{"code":' + options.body + '}',
            })
            .then(
                function(res) {
                    let { Code } = res;
                    if (Code === 'OK') {
                        Log.l(res);
                    }
                },
                function(err) {
                    Log.w(err);
                },
            );
    };
}
