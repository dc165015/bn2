import { NodeStyles } from './style';
import { isObject } from 'lodash';
import { Meteor } from 'meteor/meteor';

const disabled = Meteor.isProduction || Meteor.isCordova;
const isClient = Meteor.isClient;
const info = isClient ? (msg) => [ '%c' + msg, 'color:green' ] : NodeStyles.green;
const warn = isClient ? (msg) => [ '%c' + msg, 'color:cyan' ] : NodeStyles.cyan;
const error = isClient ? (msg) => [ '%c' + msg, 'color:red' ] : NodeStyles.red;
const note = isClient ? (msg) => [ '%c' + msg, 'text-decoration:underline' ] : NodeStyles.underline;
const paragraph = isClient
    ? (msg) => [ '%c' + msg, 'margin:20px 20px 10px; padding:5px 10px; color:white; background-color:blue' ]
    : (msg) => NodeStyles.white.bgBlue('\t  ' + msg + '  ');

let previousMsg = '',
    repeatCount = 0;

export class Log {
    static print(colorFunc, ...args) {
        if (disabled) return;
        let msg: any = args.map((arg) => (isObject(arg) ? JSON.stringify(arg) : arg)).join(' ~ ');
        if (previousMsg == msg) {
            repeatCount++;
        } else {
            if (repeatCount > 0) console.log('...the same repeated:', repeatCount);
            previousMsg = msg;
            repeatCount = 0;
            if (colorFunc) msg = colorFunc(msg);
            isClient && colorFunc ? console.log(...msg) : console.log(msg);
        }
    }

    static log = Log.l;
    static l(...args) {
        this.print(null, ...args);
    }

    static info = Log.i;
    static i(...args) {
        this.print(info, ...args);
    }

    static warn = Log.w;
    static w(...args) {
        this.print(warn, ...args);
    }

    static error = Log.e;
    static e(...args) {
        args = args.map((err) => err && (err.message || err));
        this.print(error, ...args);
    }

    static paragraph = Log.p;
    static p(...args) {
        this.print(paragraph, ...args);
    }

    static note = Log.n;
    static n(...args) {
        this.print(note, ...args);
    }
}
