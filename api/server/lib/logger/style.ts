export interface NodeStyles extends Codes { }

interface NodeStylesFunction extends NodeStyles {
    (str: string): string;
}

type Codes = {
    [k in keyof typeof codes]?: NodeStylesFunction
};

var colors = {
    black: [30, 39],
    red: [31, 39],
    green: [32, 39],
    yellow: [33, 39],
    blue: [34, 39],
    magenta: [35, 39],
    cyan: [36, 39],
    white: [37, 39],
    gray: [90, 39],
    grey: [90, 39],
}

var codes = {
    reset: [0, 0],

    bold: [1, 22],
    dim: [2, 22],
    italic: [3, 23],
    underline: [4, 24],
    inverse: [7, 27],
    hidden: [8, 28],
    strikethrough: [9, 29],

    ...colors,

    bgBlack: [40, 49],
    bgRed: [41, 49],
    bgGreen: [42, 49],
    bgYellow: [43, 49],
    bgBlue: [44, 49],
    bgMagenta: [45, 49],
    bgCyan: [46, 49],
    bgWhite: [47, 49],
};


export var NodeStyles: NodeStyles = {};

Object.setPrototypeOf(NodeStyles, Function.prototype);

Object.keys(codes).forEach(function (key) {
    var val = codes[key];
    var style: any = {};
    style.open = '\x1b[' + val[0] + 'm';
    style.close = '\x1b[' + val[1] + 'm';
    NodeStyles[key] = str => style.open + str + style.close;
    Object.setPrototypeOf(NodeStyles[key], NodeStyles);
});
