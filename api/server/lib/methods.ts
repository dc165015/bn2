import { Log } from './logger';
import { Meteor } from 'meteor/meteor';

const methods = Meteor.methods;

Meteor.methods = function methodsWrapper(methodsObj: object) {
    for (let methodName of Object.keys(methodsObj)) {
        const method = methodsObj[methodName];
        methodsObj[methodName] = function methodWrapper() {
            Log.p('Method:', methodName);
            Log.l('arguments', arguments);
            const ret = method.apply(this, arguments);
            Log.i('result:', ret);
            return ret;
        };
    }

    methods.call(Meteor, methodsObj);
};
