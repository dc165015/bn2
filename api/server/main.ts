import { Meteor } from 'meteor/meteor';
import { Log } from './lib/logger';
import './lib/methods';
import fixture from './fixtures/index';
import { Accounts } from 'meteor/accounts-base';
import { reportDocNumbers, initCollections } from './collections/init-collections';

Meteor.startup(() => {
    setupPhoneLoginOptions();
    initCollections();
    if (!Meteor.isProduction) fixture();
    reportDocNumbers();

    Log.p('STARTED');
});

function setupPhoneLoginOptions() {
    if (Meteor.settings) {
        Object.assign(Accounts._options, Meteor.settings['accounts-phone']);
    }
}
