import { Message } from '../models/message'
import { DocCollection } from './doc-collection';
import { Chats } from '../collections/chats';

export const Messages = new DocCollection('messages', Message);

Messages.after.insert((userId, message) => {
    Chats.update(message.chatId, { $set: { lastMessageId: message._id } });
});
