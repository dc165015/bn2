import { Book } from '../models/book';
import { MongoInternals } from 'meteor/mongo';
import { DocCollection } from './doc-collection';
import { Meteor } from 'meteor/meteor';

let options = {};
// if (Meteor.isServer) {
//     const _driver = new MongoInternals.RemoteCollectionDriver(
//         'mongodb://dc:dc@localhost:8084/douban',
//         // { oplogUrl: "mongodb://localhost:8084/local" }
//     );
//     options = { _driver };
// }

export const Books = new DocCollection('books', Book, options) as BooksCollection;

Books.createIndex(
    {
        title: 'text',
        alt_title: 'text',
        origin_title: 'text',
        subtitle: 'text',
        author: 'text',
        translator: 'text',
        'tags.name': 'text',
        isbn13: 'text',
    },
    {
        weights: {
            title: 10,
            alt_title: 8,
            origin_title: 9,
            subtitle: 9,
            author: 5,
            translator: 3,
            'tags.name': 7,
            isbn13: 1,
        },
        language_override: 'search_language',
    },
);

Books.createIndex({ id: -1, source: -1 });

const insert = Books.insert.bind(Books);
Books.insert = function (doc: any, callback?: Function){
    // 确保无重复的书源记录
    const existed = Books.findOne({ id: doc.id, source: doc.source });
    if (existed) {
        const id = existed._id;
        if (callback) callback(null, id);
        return id;
    }
    return insert(doc, callback);
};

Books.sortGroups = sortGroups;
Books.groupByTags = groupByTags;

interface BooksCollection extends DocCollection<Book> {
    sortGroups: typeof sortGroups;
    groupByTags: typeof groupByTags;
}

function groupByTags (books: CanMap<Book>, sort: boolean = true){
    const groups: Map<string, Set<Book>> = new Map();
    groups.set('全部', new Set());
    groups.set('无标签', new Set());

    books.map((book: Book) => {
        const tags = book.tags;
        groups.get('全部').add(book);
        if (!tags || !tags.length) {
            groups.get('无标签').add(book);
        }
        else {
            tags.forEach((tag) => {
                let group = groups.get(tag.name) || new Set();
                group.add(book);
                groups.set(tag.name, group);
            });
        }
    });

    return sort ? Books.sortGroups(groups) : groups;
}

function sortGroups (groups: Map<string, Set<Book>>){
    const newGroups = new Map();
    const tags = Array.from(groups.keys());

    // 先按标签下书的数量从多到少排序, 若相同再按标签长度从短到长排序
    tags.sort((a, b) => groups.get(b).size - groups.get(a).size || a.length - b.length);
    tags.forEach((tag) => newGroups.set(tag, groups.get(tag)));

    return newGroups;
}
