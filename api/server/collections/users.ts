import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { User } from '../models/user';
import { DocCollection } from './doc-collection';
import { Communities } from './communities';
import { Picture } from '../models/picture';
import { Copies } from './copies';
import { equalJSON } from '../lib/util';
import { Community } from '../models';
import { Log } from '../lib/logger';

export const Users = DocCollection.fromExisting<User>(Meteor.users as any, User, 'users');

if (Meteor.isClient) Users['_transform'] = (doc) => User.wrapDoc(doc);

export function initUsers (){
    if (Meteor.isClient) return;
    let sys = User.System;
    if (!User.get(sys._id)) sys.insert();
}

Users.before.insert(function (userId, doc){
    setupName(doc);
    setupPhone(doc);
    setupAvatar(doc);
    setupCommunities(doc);
});

Users.after.insert(function (userId, doc){
    Communities.update({ _id: { $in: doc.communityIds } }, { $inc: { memberCount: 1 } });
});

// copy the communityIds from user to his/her copies in order to improve query performance
Users.after.update(
    function (userId, user, fieldNames, modifier, options){
        if (!userId || equalJSON(this.previous.communityIds, user.communityIds)) return;
        Copies.update({ ownerId: userId }, { $set: { communityIds: user.communityIds } });
    },
    { fetchPrevious: true },
);

// phobit remove user
Users.before.remove(function (){
    return false;
});

Users.createIndex({ 'profile.name': 'text' });
Users.createIndex({ communityIds: 1 });
Users.createIndex({ 'phone.number': -1 }, { unique: true });

function setupName (doc){
    if ((doc.profile && doc.profile.name) || !doc.phone || !doc.phone.number) return;
    doc.profile = doc.profile || {};
    const matches = /(\+?\d{5})\d*(\d)/.exec(doc.phone.number);
    doc.profile.name = matches[1] + '**' + matches[2];
}

function setupPhone (doc){
    if (!doc.phone || !doc.phone.number) {
        doc.phone = doc.phone || {};
        doc.phone.number = Random.id();
    }
}

function setupAvatar (doc){
    let profile = doc.profile;
    if (!profile || profile.avatar || !profile.pictureId) return;
    profile.avatar = Picture.getPictureUrl(profile.pictureId);
}

function setupCommunities (doc){
    let communityIds = doc.communityIds;
    if (communityIds && communityIds.length) return;
    doc.communityIds = [
        Community.All._id,
    ];
}
