import { DocCollection } from './doc-collection';
import { Communities } from './communities';
import { Copy } from '../models/copy';
import { Users } from './users';
import { Books } from './books';

export const Copies = new DocCollection('copies', Copy);

// collection.attachSchema;

Copies.before.insert((userId, copy) => {
    if (!userId || copy.communityIds) return;
    let owner = Users.findOne(userId);
    copy.communityIds = owner.communityIds;
});

Copies.after.insert((userId, copy) => {
    let owner = Users.findOne(copy.ownerId);
    Users.update(copy.ownerId, { $inc: { 'profile.copyCount': 1 } });
    Books.update(copy.bookId, { $inc: { copyCount: 1 } })

    if (owner.communityIds && owner.communityIds.length)
        Communities.update({ _id: { $in: owner.communityIds } }, { $inc: { copyCount: 1 } }, { multi: true });
});

Copies.createIndex({ bookId: -1 });
Copies.createIndex({ ownerId: -1 });
