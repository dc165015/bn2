import { initCommunities, Communities } from "./communities";
import { initUsers, Users } from "./users";
import { Copies } from "./copies";
import { Orders } from "./orders";
import { Books } from "./books";
import { Chats } from "./chats";
import { Messages } from "./messages";
import { Meteor } from "meteor/meteor";
import { Log } from "../lib/logger";

export function initCollections() {
    initCommunities();
    initUsers();
}

export function reportDocNumbers() {
    Log.l(`There are:
        ${Books.find().count()} books,
        ${Copies.find().count()} copies,
        ${Communities.find().count()} communities,
        ${Users.find().count()} users,
        ${Orders.find().count()} orders,
        ${Chats.find().count()} chats,
        ${Messages.find().count()} messages.`);
}

Meteor['reportDocNumbers'] = reportDocNumbers;
