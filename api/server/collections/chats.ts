import { Chat } from '../models/chat';
import { Messages } from './messages';
import { DocCollection } from './doc-collection';

export const Chats = new DocCollection('chats', Chat);

Chats.createIndex({ memberIds: -1 });

Chats.after.remove(function (userId, doc) {
    Messages.remove({ chatId: doc._id });
});

// 两人之间聊天，memberIds的存放对用户本无意义，但为了便于系统检索以确保唯一性则必须排序
Chats.before.insert(function (userId, doc) {
    doc.memberIds.sort();
});

// 排序原因同before.insert
Chats.before.find(function (userId, selector = {}) {
    let ids = selector.memberIds;
    if (ids && ids instanceof Array) ids.sort();
});

// 排序原因同before.find
Chats.before.findOne(function (userId, selector = {}) {
    let ids = selector.memberIds;
    if (ids && ids instanceof Array) ids.sort();
});

