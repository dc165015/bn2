import { MongoObservable } from 'meteor-rxjs';
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { DocModel } from '../models/doc-model';
import { Observable } from 'rxjs';

export interface ConstructorOptions {
    connection?: Object;
    idGeneration?: string;
    transform?: Function;
}

export class DocCollection<T extends DocModel> extends Mongo.Collection<T> {
    protected static register<T extends DocModel>(Collection, Model, Name) {
        const registry = (Meteor.collections = Meteor.collections || new Map());
        let root = Meteor.isServer ? global : window;
        root[Model.name] = Collection.Model = Model;
        registry.set(Model, Collection);
        Collection._$ = new MongoObservable.Collection<T>(Collection);
        this.installHooks(Collection);
    }

    protected static installHooks<T extends DocModel>(Collection: DocCollection<T>) {
        Collection.before.insert((userId, doc) => {
            Object.setPrototypeOf(doc, Object.prototype); // remove inherited members
            doc.createdAt = doc.updatedAt = Date.now();
        });

        Collection.before.update((userId, doc, fieldNames, modifier, options) => {
            modifier.$set = modifier.$set || {};
            modifier.$set.updatedAt = Date.now();
        });

        Collection.before.find((userId, selector, options = {}) => {
            options['sort'] = options['sort'] || { createdAt: -1 };
        });
    }

    public static fromExisting<T extends DocModel>(Collection: Mongo.Collection<T>, Model, Name) {
        Object.setPrototypeOf(Collection, this.prototype);
        DocCollection.register(Collection, Model, Name);
        return Collection as DocCollection<T>;
    }

    _$: MongoObservable.Collection<T>;
    Model: Constructor<T>;

    constructor(Name: string, Model: Constructor<T>, options: ConstructorOptions = {}) {
        if (!Meteor.isServer) options.transform = options.transform || ((doc) => Model['wrapDoc'](doc));
        super(Name, options);
        this.createIndex({ createdAt: -1 });
        DocCollection.register(this, Model, Name);
    }

    createIndex(keys: { [key: string]: number | string } | string, options?: { [key: string]: any }): void {
        if (Meteor.isServer) {
            this.rawCollection().createIndex(keys, options);
        }
    }

    insert$(doc: T): Observable<string> {
        return this._$.insert(doc);
    }

    remove$(selector: MongoSelector<T>): Observable<number> {
        return this._$.remove(selector);
    }

    update$(
        selector: MongoSelector<T>,
        modifier: Mongo.Modifier<T>,
        options: { multi?: boolean; upsert?: boolean },
    ): Observable<number> {
        return this._$.update(selector, modifier, options);
    }

    upsert$(selector: MongoSelector<T>, modifier: Mongo.Modifier<T>, options: { multi?: boolean }): Observable<number> {
        return this._$.update(selector, modifier, options);
    }

    find$(selector?: MongoSelector<T>, options?: MongoFindOptions) {
        return this._$.find(selector, options);
        // .pipe(
        //     // prevent run multiple times when multiple subscribers subscribe to the same observable data source.
        //     shareReplay(1),
        //     // due to ObservableCursor will resend all data if there is any data change, it has to set debounceTime.
        //     debounceTime(100)
        // );
    }
}
