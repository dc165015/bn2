import { UploadFS } from 'meteor/jalik:ufs';
import { Picture } from '../models/picture';
import { isString } from 'lodash';
import { DocCollection } from './doc-collection';
import { User } from '../models';
import { Log } from '../lib/logger';
import { Books } from '../collections';
import { Sharp } from 'meteor/sharp-dummy';

export const Pictures = new DocCollection('pictures', Picture);

Pictures.before.find(function(userId, selector, options = {}) {
    options['fields'] = options['fields'] || { url: 1 };
});

export const PicturesStore = new UploadFS.store.GridFS({
    collection: Pictures,
    name: 'pictures',
    filter: new UploadFS.Filter({
        contentTypes: [ 'image/*' ],
    }),
    transformWrite(from, to, fileId, file) {
        let width, height, quality;
        if (file.for == Picture.FOR.BOOK_SHOT || file.for == Picture.FOR.BOOK_COVER) {
            (width = 300), (height = 437), (quality = 100);
        } else if (file.for == Picture.FOR.USER_AVATAR) {
            (width = 150), (height = 150), (quality = 70);
        } else {
            (width = 300), (height = 300), (quality = 75);
        }
        const transform = Sharp().resize(width, height).min().crop().toFormat('jpeg', { quality });
        from.pipe(transform).pipe(to);
    },

    permissions: new UploadFS.StorePermissions({
        insert: checkPermission,
        update: checkPermission,
        remove: checkPermission,
    }),

    onFinishUpload(file) {
        Log.i('上传图片' + file.url);
        if (file.for == Picture.FOR.BOOK_SHOT) updateBookPhotos(file);
        else if (file.for == Picture.FOR.USER_AVATAR) updateUserAvatar(file);
    },

    onCopyError(err, fileId, file) {
        Log.e('复制出错：' + file.name);
    },
    onReadError(err, fileId, file) {
        Log.e('读取出错：' + file.name);
    },
    onWriteError(err, fileId, file) {
        Log.e('写入出错：' + file.name);
    },
});

function checkPermission(userId, doc) {
    return (userId == doc.userId && checkPicFields(doc)) || Meteor.isDevelopment;
}

function checkPicFields(doc) {
    if (doc.bookId && !isString(doc.bookId)) return false;
    if (doc.copyId && !isString(doc.copyId)) return false;
    if (doc.for && !isString(doc.for)) return false;
    return true;
}

function updateBookPhotos(file) {
    const book = Books.findOne(file.bookId);
    if (!book.images) return;
    const photoUrls = book.images.photoUrls || [];
    photoUrls.push(file.url);
    Books.update(file.bookId, { $set: { 'images.photoUrls': photoUrls } });
}

function updateUserAvatar(file) {
    if (!file.userId) return;
    const user = User.get(file.userId);
    const previousPicture = user.profile && user.profile.pictureId;
    if (previousPicture) Pictures.remove(previousPicture);
    user.update({ $set: { 'profile.pictureId': file._id, 'profile.avatar': file.url } });
}
