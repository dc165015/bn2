import { Community } from '../models/community';
import { DocCollection } from './doc-collection';
import { Meteor } from 'meteor/meteor';

export const Communities = new DocCollection('communities', Community);

export function initCommunities() {
    if (Meteor.isClient) return;
    for (let item of [ Community.All, Community.None, Community.Tobe ]) {
        if (Community.get(item._id)) continue;
        item.insert();
    }
}

// collection.attachSchema;
Communities.before.insert(function(userId, doc) {
    if (userId) doc.createdBy = userId;
});

Communities.before.update(function(userId, doc) {
    if (userId) doc.updatedBy = userId;
});
