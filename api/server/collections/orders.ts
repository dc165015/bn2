import { Order } from '../models/order';
import { DocCollection } from './doc-collection';
import { Users } from '../collections/users';
import { Books } from '../collections/books';
import { Copies } from '../collections/copies';
import { DEFAULT_TERMS } from '../lib/constants';

export const Orders = new DocCollection('orders', Order);

Orders.before.insert((userId, order) => {
    const owner = Users.findOne(order.ownerId);
    if (owner && owner.profile) order.ownerName = owner.profile.name;

    const borrower = Users.findOne(order.borrowerId);
    if (borrower && borrower.profile) order.borrowerName = borrower.profile.name;

    const copy = Copies.findOne(order.copyId);
    if (copy) {
        order.terms = copy.terms;
        order.bookId = copy.bookId;

        const book = Books.findOne(order.bookId);
        if (book) order.title = [book.title, book.subtitle, book.alt_title, book.origin_title].join(' ').trim();
    }

    if (!order.terms && owner && owner.settings) order.terms = owner.settings.defaultTerms || DEFAULT_TERMS;
});

Orders.createIndex({ ownerId: -1 });
Orders.createIndex({ borrowerId: -1 });
Orders.createIndex({ copyId: -1 });
Orders.createIndex({ title: 'text', ownerName: 'text', borrowerName: 'text' });
