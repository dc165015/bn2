import { requireLogin, checkStringLength, checkName } from '../lib/check';
import { MAX_DESCRIPTION_LENGTH, MAX_TERMS_LENGTH } from '../lib/constants';
import { Meteor } from 'meteor/meteor';
import { Users } from '../collections';

Meteor.methods({
    updateProfile(name: string, description: string) {
        requireLogin();

        checkName(name);
        checkStringLength(description, 0, MAX_DESCRIPTION_LENGTH);

        let fields: any = { 'profile.name': name };
        if (description) fields = { ...fields, 'profile.description': description };

        Users.update(this.userId, { $set: fields });
    },

    updateSettings(terms: string) {
        requireLogin();
        checkStringLength(terms, 0, MAX_TERMS_LENGTH);

        Meteor.users.update(this.userId, { $set: { 'settings.defaultTerms': terms } });
    },
});
