import { requireLogin, checkId } from '../lib/check';
import { Book } from '../models/book';
import { Copies } from '../collections/copies';
import { Copy } from '../models/copy';
import { Meteor } from 'meteor/meteor';
import { Log } from '../lib/logger';
import { Books } from '../collections';

Meteor.methods({
    addCopy (book: Book) {
        requireLogin();

        let bookId = Meteor.call('addBook', book);

        let copy: Copy = Copies.findOne({ bookId, ownerId: this.userId });
        if (copy) throw new Meteor.Error('VIOLATION', '此书已存在', '您已扫描上传过这本书。');

        let copyId = new Copy(bookId, this.userId).insert();
        Log.i('copy inserted: ', copyId);

        return bookId;
    },

    editCopy (book: Book) {
        requireLogin();

        const bookId = book._id;
        checkId(bookId);

        Books.update(bookId, { $set: { ...book } });

        return bookId;
    },
});
