import { Chats } from '../collections/chats';
import { Messages } from '../collections/messages';
import { requireLogin, checkStringLength, checkId, checkEnum } from '../lib/check';
import { MAX_MESSAGE_LENGTH } from '../lib/constants';
import { MessageType, Message } from '../models/message';
import { Meteor } from 'meteor/meteor';
import { User, Chat } from '../models';
import { Log } from '../lib/logger';

Meteor.methods({
    addMessage(chatId: ID, content: string, type: MessageType = MessageType.TEXT) {
        requireLogin();
        checkEnum(type, MessageType.list());
        checkId(chatId);
        checkStringLength(content, 1, MAX_MESSAGE_LENGTH);
        // TODO: filter malicious content

        const chatExists = !!Chats.find(chatId).count();

        if (!chatExists) {
            throw new Meteor.Error('chat-not-exists', "Chat doesn't exist");
        }

        return Messages.insert({
            chatId: chatId,
            senderId: this.userId,
            content: content,
            type: type,
            didRead: 'no',
        } as Message);
    },

    countMessages(chatId: ID): number {
        requireLogin();
        checkId(chatId);
        return Messages.find({ chatId }).count();
    },

    notify(content: string, receiverId: ID, type: MessageType = MessageType.TEXT, referenceId?: ID) {
        checkId(receiverId);
        if (referenceId) checkId(referenceId);
        checkStringLength(content, 1, MAX_MESSAGE_LENGTH);

        const senderId = User.System._id;
        const chatId = Chat.findOrCreateByMemberIds(receiverId, senderId);

        Log.w(`Message(type:${type}):`, `from ${senderId}`, `to ${receiverId}`, content);

        return Messages.insert({ chatId, senderId, content, type, referenceId, didRead: 'no' } as Message);
    },
});
