import { requireLogin, checkId, checkEnum } from '../lib/check';
import { Order, OrderOperation, OrderState } from '../models/order';
import { Copies, Books } from '../collections';
import { Log } from '../lib/logger';
import { notify } from '../lib/notify';
import { MessageType } from '../models';
import { Meteor } from 'meteor/meteor';

Meteor.methods({
    newOrder(copyId: ID) {
        requireLogin();
        checkId(copyId);

        if (Order.findOne({ borrowerId: this.userId, copyId, state: OrderState.New }))
            throw new Meteor.Error('VIOLATION', '重复订单', '您已申请借阅此书，无需再借。如需提醒书主尽快批复，请在您的借入申请订单中再次求借。');

        const copy = Copies.findOne(copyId);
        if (!copy) throw new Meteor.Error('DOC_NOT_FOUND', '没有这本书', `未找到此书${copyId}`);

        if (this.userId == copy.ownerId) throw new Meteor.Error('VIOLATION', '有反常规', '不能找自己借书');

        const order = new Order(this.userId, copyId);
        const book = Books.findOne(copy.bookId);
        const orderId = order.insert();

        notify(`${order.borrower.nickname}向你借阅《${book.title}》`, order.ownerId, MessageType.ORDER, orderId);

        return orderId;
    },

    operateOrder(orderId: ID, operation: OrderOperation) {
        requireLogin();
        checkId(orderId);
        checkEnum(operation, OrderOperation.list());

        const order = Order.get(orderId);
        if (!order) throw new Meteor.Error('DOC_NOT_FOUND', '无此订单', `未找到订单${orderId}`);

        return order.fire(operation).then(
            () => Log.i('onserver', order.currentState),
            (error) => {
                throw new Meteor.Error(`OPERATION_FAIL`, error, ` 订单${orderId}执行${operation}操作失败, 因为${error}`);
            },
        );
    },
});
