import { Book } from '../models/book';
import { Books } from '../collections/books';
import { Log } from '../lib/logger';
import { requireLogin } from '../lib/check';
import { Meteor } from 'meteor/meteor';

Meteor.methods({
    addBook (book: Book) {
        requireLogin();

        let bookId: ID;
        let doc: Book;
        if (book.id) doc = Books.findOne({ id: book.id, source: book.source });
        if (!doc) {
            // TODO: validate !!! @v1
            let _doc = Book.fixAll(book);
            bookId = Book.wrapDoc(_doc).insert();
            Log.i('book inserted: ', bookId, _doc.title);
        }
        else {
            bookId = doc._id;
            Log.w('此书已存在:', bookId, doc.title);
        }

        return bookId;
    },
});
