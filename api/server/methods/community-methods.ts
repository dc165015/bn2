import { requireLogin, checkStringLength, checkId, checkName } from '../lib/check';
import { Users, Communities, Copies } from '../collections';
import {
    PERIOD_OF_INVITATION_VALIDITY,
    MAX_NAME_LENGTH,
    MAX_DESCRIPTION_LENGTH,
    MAX_TERMS_LENGTH,
} from '../lib/constants';
import { User, Community, CommunityType } from '../models';
import { Meteor } from 'meteor/meteor';

Meteor.methods({
    inviteToJoinCommunity (communityId: ID) {
        requireLogin();
        checkId(communityId);

        if (!Communities.findOne(communityId)) {
            throw new Meteor.Error('RECORD_NOT_FOUND', '未找到', `未找到该社群(ID:${communityId})`);
        }

        const me = Users.findOne(this.userId);
        if (!me.communityIds.includes(communityId)) {
            throw new Meteor.Error('NOT_AUTHORIZED', '无权邀请', `您不是该群成员，无权邀请新人`);
        }

        const now = Date.now();

        const modifier: any = {
            $pull: { invitationCodes: { $elemMatch: { createdAt: { $lt: now - PERIOD_OF_INVITATION_VALIDITY } } } },
        };
        //删除过期的邀请
        Users.update(me._id, modifier, { multi: true });

        Users.update(me._id, {
            $push: { invitationCodes: { communityId, createdAt: now } },
        });

        return now;
    },

    joinCommunity (communityId: ID, userId: ID) {
        requireLogin();
        checkId(communityId);
        checkId(userId);

        const invitor = Users.findOne(userId);
        if (!invitor) throw new Meteor.Error('RECORD_NOT_FOUND', '无效邀请码', '未找到原邀请人');

        const community = Communities.findOne(communityId);
        if (!community) throw new Meteor.Error('RECORD_NOT_FOUND', '无效邀请码', '未找到该社群');

        const code = invitor.invitationCodes.find((code) => code.communityId == communityId);
        if (!code) throw new Meteor.Error('RECORD_NOT_FOUND', '无效邀请码', '未找到该邀请码或邀请码已过期');
        if (Date.now() - code.createdAt > PERIOD_OF_INVITATION_VALIDITY)
            throw new Meteor.Error('RECORD_NOT_FOUND', '无效邀请码', '邀请码已过期');

        const me = Users.findOne(this.userId);
        if (me.communityIds.includes(communityId)) return communityId; // 已在本群中

        if (!invitor.communityIds.indexOf(communityId))
            throw new Meteor.Error('RULE_VIOLATION', '无效邀请码', '邀请码无效，因为原邀请人已不在该社群');

        addCommunityIdToUser(this.userId, communityId);

        return communityId;
    },

    updateCommunity (communityId: ID, communityName: string, myName: string, communityDescription: string) {
        requireLogin();
        checkId(communityId);
        checkStringLength(communityName, 1, MAX_NAME_LENGTH);
        checkStringLength(myName, 0, MAX_NAME_LENGTH);
        checkStringLength(communityDescription, 0, MAX_TERMS_LENGTH);

        myName = myName || User.get(this.userId).nickname;

        const community = Communities.findOne(communityId);
        if (this.userId == community.createdBy) {
            Communities.update(communityId, { $set: { description: communityDescription, updatedAt: Date.now() } });
        }

        const communitySettings = Users.findOne(this.userId).communitySettings;
        const existed = communitySettings && communitySettings.find((item) => item.communityId == communityId);

        if (existed)
            Users.update(
                { _id: this.userId, communitySettings: { $elemMatch: { communityId } } },
                { $set: { 'communitySettings.$': { communityId, communityName, myName } } },
            );
        else
            Users.update(this.userId, {
                $push: { communitySettings: { communityId, communityName, myName } },
            });
    },

    quitCommunity (communityId: ID) {
        requireLogin();
        checkId(communityId);

        Users.update(this.userId, { $pull: { communityIds: communityId } }, { multi: false });
        //更新用户所有Copies中的CommunityIds
        Copies.update({ ownerId: this.userId }, { $pull: { communityIds: communityId } }, { multi: true });
        return communityId;
    },

    upsertCommunity (communityId: ID, communityName: string, myName: string, communityDescription: string) {
        if (communityId)
            return Meteor.call('updateCommunity', communityId, communityName, myName, communityDescription);

        requireLogin();
        checkName(communityName);
        checkStringLength(myName, 0, MAX_NAME_LENGTH);
        checkStringLength(communityDescription, 0, MAX_DESCRIPTION_LENGTH);

        const newCommunityId = Communities.insert({
            name: communityName,
            description: communityDescription,
            createdBy: this.userId,
            type: CommunityType.DEFAULT,
        } as Community);
        addCommunityIdToUser(this.userId, newCommunityId);

        return newCommunityId;
    },

    changeDefaultCommunity (communityId: ID) {
        requireLogin();
        checkId(communityId);

        const ids = Users.findOne(this.userId).communityIds || [];
        let index = ids.indexOf(communityId);
        if (index >= 0) {
            ids.splice(index, 1);
            ids.push(communityId);
            Users.update(this.userId, { $set: { communityIds: ids } });
        }
    },
});

function addCommunityIdToUser (userId, communityId){
    Users.update(userId, { $push: { communityIds: communityId } }, { multi: false }, (err, result) => {
        if (err) throw new Meteor.Error(500, '为用户添加群出错', `添加群ID<${communityId}>到用户<${userId}>出错`);
        //更新用户所有Copies中的CommunityIds
        Copies.update({ ownerId: userId }, { $push: { communityIds: communityId } }, { multi: true });
    });
}
