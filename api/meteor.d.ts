import { DocCollection } from './server/collections';
import { Observable } from 'rxjs';

declare type MeteorApplyOptions = {
    wait?: boolean;
    onRetry?: boolean;
    throwStubExceptions?: boolean;
    returnStubValue?: boolean;
    onResultReceived?: Function;
};

declare module 'meteor/meteor' {
    namespace Meteor {
        var users: DocCollection<User>;
        var collections: Map<string | Function, DocCollection<any>>;
        function subscribe$<T>(name: string, ...args: any[]): Observable<T>;
        function call$<T>(name: string, ...args: any[]): Observable<T>;
        function apply$<T>(name: string, args: EJSONable[], options?: MeteorApplyOptions): Observable<T>;
        function autorun$(
            next?: (value: Tracker.Computation) => void,
            error?: (err) => void,
            complete?: () => void,
        ): any;

        interface SubscriptionHandle {
            subscriptionId: string;
        }
    }
}
