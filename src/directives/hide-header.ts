import { Directive, Input, ElementRef, Renderer, OnInit, HostListener } from '@angular/core';

@Directive({
    selector: '[hide-header-in-scroll]', // Attribute selector
})
export class HideHeaderDirective implements OnInit {
    @Input('headerToHide') header: HTMLElement;
    @Input('scrollContainer') container: HTMLElement;
    headerHeight;
    self;
    scrollContent;

    constructor (public element: ElementRef, public renderer: Renderer) {}

    ngOnInit () {
        if (!this.header) return;

        this.self = this.element.nativeElement;

        if (!this.container) {
            this.self = this.scrollContent = this.self.getElementsByClassName('scroll-content')[0];
        }
        else {
            // if container is fixed, then use getFixedElement to get _fixedContent; else use getScrollElement to get _scrollContent
            // this.scrollContent = (this.container as any).getFixedElement();
            this.scrollContent = (this.container as any).getScrollElement();
        }

        this.headerHeight = this.header.clientHeight;
        this.setTransition();
    }

    setTransition () {
        this.renderer.setElementStyle(this.header, 'webkitTransition', 'top 700ms');
        this.renderer.setElementStyle(this.scrollContent, 'webkitTransition', 'margin-top 700ms');
    }

    @HostListener('scroll')
    onScroll () {
        this.move();
    }

    @HostListener('ionScroll')
    onIonScroll () {
        this.move();
    }

    move () {
        if (!this.header) return;
        if (this.self.scrollTop > 56) {
            this.renderer.setElementStyle(this.header, 'top', '-56px');
            this.renderer.setElementStyle(this.scrollContent, 'margin-top', '0px');
        }
        else {
            this.renderer.setElementStyle(this.header, 'top', '0px');
            this.renderer.setElementStyle(this.scrollContent, 'margin-top', '56px');
        }
    }
}
