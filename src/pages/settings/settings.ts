import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { User } from 'api/models/user';
import { TermsOfServicePage } from 'pages/terms-of-service/terms-of-service';
import { ToastProvider } from 'providers/toast';
import { ChatsPage } from 'pages/chats/chats';
import { MyProfileEditPage } from 'pages/my-profile-edit/my-profile-edit';
import { EnhancedBase } from '../../lib/enhanced-base';
import { GetPhonePage } from 'pages/login/get-phone/get-phone';
import { MessagesPage } from 'pages/messages/messages';
import { Log } from 'api/lib/logger';
import { FullTextAreaComponent } from 'components/full-text-area/full-text-area';
import { MAX_TERMS_LENGTH, DEFAULT_TERMS } from 'api/lib/constants';
import { Chat, Message } from 'api/models';

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingsPage extends EnhancedBase implements OnInit {
    settingsForm: FormGroup;
    user: User;
    notificationId;
    hasNewNotifications: boolean = false;
    hasNewMessages: boolean = false;

    constructor (
        public navCtrl: NavController,
        public formBuilder: FormBuilder,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.autorun(() => {
            this.user = User.Me;
            this.notificationId = Chat.notificationId;
            if (!this.notificationId) return;
            this.hasNewNotifications = Message.getUnreadCount('notification') > 0;
            this.hasNewMessages = Message.getUnreadCount('message') > 0;
        });
    }

    showChats () {
        this.navCtrl.push(ChatsPage);
    }

    showNotifications () {
        if (!this.notificationId) return;
        this.navCtrl.push(MessagesPage, {
            chatId: this.notificationId,
            handleRemoveChat: () => this.removeChat(this.notificationId),
        });
    }

    logout () {
        this.navCtrl
            .popToRoot()
            .then(() => this.navCtrl.setRoot(GetPhonePage))
            .then(() => Meteor.logout((err) => err && this.showError(err)))
            .catch((err) => Log.e(err));
    }

    showTermsModal () {
        let modal = this.modalCtrl.create(TermsOfServicePage);
        modal.present();
    }

    openMyPofileEditPage () {
        this.navCtrl.push(MyProfileEditPage);
    }

    editMyTerms () {
        const modal = this.modalCtrl.create(FullTextAreaComponent, {
            content: this.user.terms || DEFAULT_TERMS,
            placeholder: '填写他人向您申请借书时，必须同意并遵守的事项条款。',
            title: '借阅条款',
            maxLength: MAX_TERMS_LENGTH,
        });

        modal.onDidDismiss((terms) => this.updateTerms(terms));

        modal.present();
    }

    updateTerms (terms: string) {
        if (terms == void 0 || terms == this.user.terms) return;
        this.call$('updateSettings', terms).subscribe({
            error: (err) => this.showError(err),
        });
    }

    removeChat (chatId) {
        this.call$('removeChat', chatId).subscribe({ error: (err) => this.showError(err) });
    }
}
