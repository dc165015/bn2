import { Component, ChangeDetectorRef, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { NavParams, LoadingController, ModalController } from 'ionic-angular';
import { Book, Tag } from 'api/models';
import { EnhancedBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { FullTextAreaComponent } from 'components/full-text-area/full-text-area';
import { PictureProvider } from 'providers/picture';
import { Picture } from 'api/models/picture';
import { PreloadImage } from 'components/preload-image/preload-image';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { switchMap } from 'rxjs/operators';
import { CopyProvider } from 'providers/copies';

@Component({
    selector: 'page-edit-book',
    templateUrl: 'edit-book.html',
    changeDetection: ChangeDetectionStrategy.Default,
})
export class EditBookPage extends EnhancedBase {
    // comment: string;
    bookId: ID;
    book: Book;
    @ViewChild(PreloadImage) cover: PreloadImage;

    constructor (
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        public pictureProvider: PictureProvider,
        public copyProvider: CopyProvider,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.bookId = this.navParams.get('bookId');
        if (this.bookId)
            this.autorun(() => {
                this.book = Book.get(this.bookId);
            });
        else this.book = new Book();
        this.getISBN();
    }

    get coverImage () {
        return (this.book && this.book.cover) || Book.Scanner.image;
    }

    getISBN () {
        const isbn = this.navParams.get('isbn');
        if (!isbn) return; // 新建书籍
        if (String(isbn).length == 13) this.book.isbn13 = isbn;
        else if (String(isbn).length == 10) this.book.isbn10 = isbn;
    }

    changeCover (): void {
        this.pictureProvider
            .askPicture('book', true)
            .then((blob) => {
                this.cover._loaded(false);
                this.uploadCover(blob);
            })
            .catch((err) => this.showError(err));
    }

    uploadCover (blob): void {
        this.pictureProvider
            .upload(blob, { for: Picture.FOR.BOOK_COVER })
            .then((picture) => {
                this.cover.src = this.book.image = picture.url;
                this.cover._loaded(true);
                this.detectChanges();
            })
            .catch((err) => this.showError(err));
    }

    editSummary () {
        const modal = this.modalCtrl.create(FullTextAreaComponent, {
            content: this.book.summary,
            placeholder: '填写内容简介',
            title: '内容简介',
            maxLength: 2000,
        });

        modal.onDidDismiss((summary) => {
            if (summary != void 0) this.book.summary = summary;
        });

        modal.present();
    }

    editCatalog () {
        const modal = this.modalCtrl.create(FullTextAreaComponent, {
            content: this.book.catalog,
            placeholder: '填写内容目录',
            title: '内容目录',
            maxLength: 2000,
        });

        modal.onDidDismiss((catalog) => {
            if (catalog != void 0) this.book.summary = catalog;
        });

        modal.present();
    }

    addTag (tag: { name: string }) {
        let existed = this.book.tags.find((_tag) => _tag.name == tag.name);
        if (existed) existed.count++;
        else this.book.tags.push(new Tag(tag.name));
        this.detectChanges();
    }

    removeTag (tag: { name: string }) {
        const tagIndex = this.book.tags.findIndex((_tag) => _tag.name == tag.name);
        if (tagIndex < 0) return;
        const _tag = this.book.tags[tagIndex];
        _tag.count--;
        if (_tag.count <= 0) this.book.tags.splice(tagIndex, 1);
        this.detectChanges();
    }

    onKeyPress (event) {
        this.detectChanges();
    }

    onRangeChange () {
        this.detectChanges();
    }

    onSubmit () {
        const doc = this.book.getRaw();
        let action = !this.bookId ? 'addCopy' : 'editCopy';
        // TODO: add manually check on the updated book data. @v2
        this.call$<ID>(action, doc)
            .pipe(switchMap((bookId) => this.copyProvider.fetchCopy$(bookId)))
            .subscribe(() => this.navCtrl.pop(), (err) => this.showError(err));
    }
}
