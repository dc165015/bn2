import { Component, OnInit, OnDestroy, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NavParams, NavController, AlertController, PopoverController, LoadingController, Events } from 'ionic-angular';
import { Chat } from 'api/models/chat';
import { Observable, Subscription, fromEvent, Subscriber } from 'rxjs';
import { map, takeUntil, filter } from 'rxjs/operators';
import { zoneOperator } from 'meteor-rxjs/dist/zone';
import * as moment from 'moment';
import { groupBy } from 'lodash';
import { PictureProvider } from 'providers/picture';
import { ToastProvider } from 'providers/toast';
import { MessagesAttachmentsComponent } from 'components/messages-attachments/messages-attachments';
import { Message, MessageType } from 'api/models/message';
import { Messages } from 'api/collections/messages';
import { Picture } from 'api/models/picture';
import { EnhancedBase } from '../../lib/enhanced-base';
import { Meteor } from 'meteor/meteor';
import { MeteorObservable } from 'meteor-rxjs';
import { User } from 'api/models/user';
import { Log } from 'api/lib/logger';
import { OrderCardComponent } from 'components/order-card/order-card';
import { Order } from 'api/models';
import { Badge } from '@ionic-native/badge';

@Component({
    selector: 'page-messages',
    templateUrl: 'messages.html',
})
export class MessagesPage extends EnhancedBase implements OnInit, OnDestroy {
    selectedChatId: ID;
    selectedChat: Chat;
    title: string;
    picture: string;
    messagesDayGroups;
    messages: Observable<Message[]>;
    newMessage: string = '';
    autoScroller: MutationObserver;
    scrollOffset = 0;
    senderId: ID;
    isNotifications: boolean;
    loadingMessages: boolean;
    messagesComputation: Subscription;
    batchCounter: number = 0;
    private handleRemoveChat: Function;

    constructor (
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public navParams: NavParams,
        public events: Events,
        public badge: Badge,
        public el: ElementRef,
        public popoverCtrl: PopoverController,
        public pictureProvider: PictureProvider,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
        this.selectedChatId = navParams.get('chatId');
        this.handleRemoveChat = <Function>navParams.get('handleRemoveChat');
    }

    private get messagesPageContent (): Element {
        return this.el.nativeElement.querySelector('.messages-page-content');
    }

    private get messagesList (): Element {
        return this.messagesPageContent.querySelector('.messages');
    }

    private get scroller (): Element {
        return this.messagesList.querySelector('.scroll-content');
    }

    ngOnInit () {
        this.autorun(() => {
            this.senderId = Meteor.userId();
            this.selectedChat = Chat.get(this.selectedChatId);
            if (this.selectedChat) this.setTitleAndPicture();
        });

        this.autoScroller = this.autoScroll();
        this.loadMore();
    }

    protected setTitleAndPicture () {
        const counterpart = this.selectedChat.counterpart;
        if (!counterpart) return;
        if (counterpart._id == User.System._id) {
            this.isNotifications = true;
            this.title = '通知';
        }
        else {
            this.title = this.selectedChat.title || counterpart.nickname;
            this.picture = this.selectedChat.picture || counterpart.avatar;
        }
    }

    loadIfScroll () {
        // Get total messages count in database so we can have an indication of when tostop the auto-subscriber
        this.call$('countMessages', this.selectedChatId).subscribe((count: number) =>
            // Chain every scroll event
            fromEvent(this.scroller, 'scroll').pipe(
                // Remove the scroll listener once all messages have been fetched
                takeUntil(this.autoRemoveScrollListener(count)),
                // Filter event handling unless we're at the top of the page
                filter(() => !this.scroller.scrollTop),
                // Prohibit parallel subscriptions
                filter(() => !this.loadingMessages),
                // Invoke the messages subscription once all the requirements have been met
                map(() => this.loadMore()),
            ),
        );
    }

    ngOnDestroy () {
        if (this.autoScroller) this.autoScroller.disconnect();
        super.ngOnDestroy();
    }

    autoRemoveScrollListener<T> (count: number): Observable<T> {
        return Observable.create((observer: Subscriber<T>) =>
            Messages.find$().subscribe({
                next: (messages) => {
                    if (count != messages.length) return;
                    observer.next();
                    observer.complete();
                },
            }),
        );
    }

    loadMore () {
        this.loadingMessages = true;
        // A custom offset to be used to re-adjust the scrolling position once new dataset is fetched
        this.scrollOffset = this.scroller.scrollHeight;

        this.fetch$('messagesAndMarkRead', ++this.batchCounter, this.selectedChat._id, () => {
            // Keep tracking changes in the dataset and re-render the view
            if (!this.messagesComputation) {
                this.messagesComputation = this.autorunMessages();
                this.addSubscription(this.messagesComputation, 'autorunMessages');
            }

            // Allow incoming subscription requests
            this.loadingMessages = false;
        });
    }

    autorunMessages (): Subscription {
        return MeteorObservable.autorun().subscribe(() => {
            this.messagesDayGroups = this.findMessagesDayGroups();
            this.badge.set(Message.getUnreadCount());
        });
    }

    findMessagesDayGroups () {
        return Messages.find$({ chatId: this.selectedChat._id }, { sort: { createdAt: 1 } }).pipe(
            map((messages: Message[]) => {
                const format = 'YYYY-MM-DD';
                const todayFormat = moment().format(format);

                // Group by creation day
                const groupedMessages = groupBy(messages, (message) => {
                    return moment(message.createdAt).format(format);
                });

                // Transform dictionary into an array since Angular's view engine doesn't know how
                // to iterate through it
                return Object.keys(groupedMessages).map((timestamp: string) => {
                    return {
                        timestamp: timestamp,
                        messages: groupedMessages[timestamp],
                        today: todayFormat === timestamp,
                    };
                });
            }),
        );
    }

    autoScroll (): MutationObserver {
        const autoScroller = new MutationObserver(this.scrollDown.bind(this));

        autoScroller.observe(this.messagesList, {
            childList: true,
            subtree: true,
        });

        this.loadIfScroll();

        return autoScroller;
    }

    scrollDown (): void {
        // Don't scroll down if messages subscription is being loaded
        if (this.loadingMessages) {
            return;
        }

        // Scroll down and apply specified offset
        this.scroller.scrollTop = this.scroller.scrollHeight - this.scrollOffset;
        // Zero offset for next invocation
        this.scrollOffset = 0;
    }

    onInputKeypress ({ keyCode }: KeyboardEvent): void {
        if (keyCode === 13) {
            this.sendTextMessage();
        }
    }

    sendTextMessage (): void {
        // If message was yet to be typed, abort
        if (!this.newMessage) {
            return;
        }

        this.call$('addMessage', this.selectedChat._id, this.newMessage).pipe(zoneOperator()).subscribe(() => {
            // Zero the input field
            this.newMessage = '';
        });
    }

    removeChat (): void {
        const alert = this.alertCtrl.create({
            message: '确定要删除消息吗？',
            buttons: [
                {
                    text: '取消',
                    role: 'cancel',
                },
                {
                    text: '确定',
                    handler: () => {
                        this.handleRemoveChat();
                        alert.dismiss();
                        this.navCtrl.pop();

                        // It's important to note that the handler returns false. A feature of button handlers is that they automatically dismiss the alert when their button was clicked, however, we'll need more control regarding the transition. Because the handler returns false, then the alert does not automatically dismiss itself. Instead, you now have complete control of when the alert has finished transitioning, and the ability to wait for the alert to finish transitioning out before starting a new transition.
                        //return false;
                    },
                },
            ],
        });
        alert.present();
    }

    showAttachments (): void {
        const popover = this.popoverCtrl.create(
            MessagesAttachmentsComponent,
            {
                chat: this.selectedChat,
            },
            {
                cssClass: 'attachments-popover',
            },
        );

        popover.onDidDismiss((params) => {
            if (params) {
                if (params.messageType === MessageType.LOCATION) {
                    this.sendLocationMessage(params.selectedLocation);
                }
                else if (params.messageType === MessageType.PICTURE) {
                    this.sendPictureMessage(params.selectedPicture);
                }
            }
        });

        popover.present();
    }

    sendLocationMessage (location: Location): void {
        Log.l('uploading location...');
        this.call$('addMessage', this.selectedChat._id, location.toString(), MessageType.LOCATION)
            .pipe(zoneOperator())
            .subscribe(() => (this.newMessage = ''));
    }

    sendPictureMessage (blob): void {
        Log.l('uploading picture...');
        this.pictureProvider
            .upload(blob, { for: Picture.FOR.CHAT_MESSAGE })
            .then((picture) =>
                this.call$('addMessage', this.selectedChat._id, picture.url, MessageType.PICTURE)
                    .pipe(zoneOperator())
                    .subscribe(),
            )
            .catch((err) => this.showError(err));
    }

    showOrder (ev, orderId) {
        const order = Order.get(orderId);
        if (!order)
            this.fetch$('order', 1, orderId, () => {
                this.presentOrder(ev, orderId);
            });
        else this.presentOrder(ev, orderId);
    }

    presentOrder (ev, orderId) {
        const popover = this.popoverCtrl.create(OrderCardComponent, { orderId }, { cssClass: 'message-order-card' });
        popover.present({ ev });
    }
}
