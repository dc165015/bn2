import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import {
    NavController,
    NavParams,
    AlertController,
    LoadingController,
    Platform,
    ActionSheetController,
} from 'ionic-angular';
import { Book } from 'api/models/book';
import { EnhancedBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { User } from 'api/models/user';
import { Copies } from 'api/collections/copies';
import { UserProfilePage } from 'pages/user-profile/user-profile';
import { PictureProvider } from 'providers/picture';
import { Picture } from 'api/models/picture';
import { EditBookPage } from 'pages/edit-book/edit-book';
import { BrowserTab } from '@ionic-native/browser-tab';
import { Log } from 'api/lib/logger';

@Component({
    selector: 'page-book-profile',
    templateUrl: 'book-profile.html',
})
export class BookProfilePage extends EnhancedBase implements OnInit {
    bookId: ID;
    book: Book;
    copyId: ID;
    photos: string[];
    owners;
    batchCounter: number = 1;
    doShowAllTags: boolean = false;
    doShowAllSummary: boolean = false;

    constructor (
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        public actionSheetCtrl: ActionSheetController,
        public platform: Platform,
        public browserTab: BrowserTab,
        public pictureProvider: PictureProvider,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
        public zone: NgZone,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.bookId = this.navParams.get('bookId');
        this.autorun(() => {
            this.book = Book.get(this.bookId);
            this.owners = User.Me.findCompaniesByBookId(this.book._id);
            this.photos = this.book.photos || [];
            this.sortTags();
        });
        if (!this.owners.count()) this.initLoad();
    }

    ngAfterViewInit () {
        if (this.navParams.get('action') == 'shot') {
            this.addPicture();
        }
    }

    subscribeOwners () {
        this.fetch$('owners', this.batchCounter++, this.book._id);
    }

    sortTags () {
        this.book.tags = this.book.tags || [];
        this.book.tags.sort((a, b) => b.count - a.count);
    }

    get authorAndTranslator () {
        let translators: string = this.book.translators;
        return this.book.authors + (translators ? ` / 译者：${translators}` : '');
    }

    get otherConditions () {
        let ret = [];
        const { pages, binding, price, publisher, pubdate, isbn13, isbn10 } = this.book;
        if (pages) ret.push(pages + ' 页');
        if (binding) ret.push(binding);
        if (price) ret.push(price + ' 元');
        if (publisher) ret.push(publisher);
        if (pubdate) ret.push(pubdate);
        if (isbn13) ret.push('ISBN: ' + isbn13);
        // isbn13不存在时，才提供isbn10信息
        if (!isbn13 && isbn10) ret.push('ISBN: ' + isbn10);
        return ret.join(' / ');
    }

    loadMore (infiniteSchroll?) {
        super.loadMore(infiniteSchroll);
        this.subscribeOwners();
    }

    borrowFrom (owner: User) {
        if (owner.isMe) return this.toast.present('不能找自己借书。');
        const copy = Copies.findOne({ ownerId: owner._id, bookId: this.book._id });
        if (copy.isActing) return this.showError('书主的这本书已被人借走，请过两天再试。');
        if (copy.isLost) return this.showError('书主的这本书已遗失。');
        const terms = owner.terms;

        const handler = () => {
            this.call$('newOrder', copy._id).subscribe(
                (orderId) => {
                    this.toast.present('订单已创建，等待书主回复。');
                },
                (error) => this.showError(error),
            );
        };

        this.presentTerms(terms, handler);
    }

    moreMenu (event) {
        let actions = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: '添加照片',
                    handler: () => this.addPicture(),
                },
                {
                    text: '补充或修正资料',
                    handler: () => this.navCtrl.push(EditBookPage, { bookId: this.bookId }),
                },
                {
                    text: '取消',
                    role: 'cancel',
                },
            ],
        });

        actions.present();
    }

    openDouban () {
        Log.w('open douban url:', this.book.alt);
        if (this.book.alt) this.browserTab.openUrl(this.book.alt);
    }

    presentTerms (terms: string, handler) {
        if (!/^<pre>.*<\/pre>$/.test(terms)) terms = '<pre>' + terms + '</pre>';
        this.alertCtrl
            .create({
                title: '是否向书主借书，并同意下列借阅条款？',
                message: terms,
                cssClass: 'full-alert',
                buttons: [
                    { text: '取消', role: 'cancel' },
                    { text: '同意', handler },
                ],
            })
            .present();
    }

    onClickUser (event, owner) {
        this.navCtrl.push(UserProfilePage, { userId: owner._id });
    }

    addPicture () {
        this.pictureProvider
            .askPicture('book', true)
            .then((file) =>
                this.pictureProvider.upload(file, {
                    bookId: this.bookId,
                    copyId: this.copyId,
                    for: Picture.FOR.BOOK_SHOT,
                }),
            )
            .then((file) => this.fetch$('book', 1, { _id: file.bookId }))
            .catch((err) => this.showError(err));
    }

    switchSummaryStyle () {
        this.doShowAllSummary = !this.doShowAllSummary;
        this.detectChanges();
    }

    switchTagsStyle () {
        this.doShowAllTags = !this.doShowAllTags;
        this.detectChanges();
    }

    getCopyOfOwner (owner: User) {
        return Copies.findOne({ bookId: this.bookId, ownerId: owner._id });
    }

    isCopyActing (owner: User) {
        return this.getCopyOfOwner(owner).isActing;
    }

    isCopyLost (owner: User) {
        return this.getCopyOfOwner(owner).isLost;
    }
}
