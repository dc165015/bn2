import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { NavController, Loading, LoadingController, NavParams } from 'ionic-angular';
import { EnhancedBase } from '../../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { PhoneProvider } from 'providers/phone';
import { MainPage } from '../..';
import { User } from 'api/models';
import { MyProfileEditPage } from 'pages/my-profile-edit/my-profile-edit';
import { Platform } from 'ionic-angular';
import { Log } from 'api/lib/logger';

@Component({
    selector: 'page-get-code',
    templateUrl: 'get-code.html',
    changeDetection: ChangeDetectionStrategy.Default,
})
export class GetCodePage extends EnhancedBase {
    code: string;
    phone: string;
    origin;
    loading: Loading;

    constructor (
        public navCtrl: NavController,
        public navParams: NavParams,
        public phoneProvider: PhoneProvider,
        public toast: ToastProvider,
        public platform: Platform,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.phone = this.navParams.get('phone');
        this.origin = this.navParams.get('origin');
        this.code = ' ';
    }

    ionViewDidEnter () {
        this.code = '4444';
        // this.next({});
    }

    next (event) {
        this.newLoading();
        Log.n('Login with: ', this.phone, this.code);
        this.phoneProvider.login('+86' + this.phone, this.code).then(
            () => {
                this.dismissLoading();
                this.fetch$('loginUserInfo', 1, () => {
                    const me = User.Me;
                    if (!me.profile.name) {
                        this.navCtrl.setRoot(MyProfileEditPage, { origin: this.origin });
                    }
                    !this.origin ? this.navCtrl.setRoot(MainPage) : this.navCtrl.setPages(this.origin);
                });
            },
            (err) => {
                this.dismissLoading();
                this.showError(err);
            },
        );
    }

    onKeyPress () {
        this.detectChanges();
    }
}
