import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { NavController, LoadingController, NavParams } from 'ionic-angular';
import { GetCodePage } from 'pages/login/get-code/get-code';
import { EnhancedBase } from '../../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { PhoneProvider } from 'providers/phone';

@Component({
    selector: 'page-get-phone',
    templateUrl: 'get-phone.html',
    changeDetection: ChangeDetectionStrategy.Default,
})
export class GetPhonePage extends EnhancedBase {
    sentAt: number = 0;
    phone: string;
    origin;

    constructor(
        public navCtrl: NavController,
        public toast: ToastProvider,
        public navParams: NavParams,
        public phoneProvider: PhoneProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit() {
        this.origin = this.navParams.get('origin');
    }

    ionViewDidEnter() {
        this.phone = '13333344444';
        // this.next({});
    }

    next(event) {
        if (this.doHaveToWait) return;
        this.phoneProvider.verify('+86' + this.phone).then(
            () => {
                this.sentAt = Date.now();
                this.navCtrl.push(GetCodePage, { phone: this.phone, origin: this.origin });
            },
            (error) => this.showError(error),
        );
    }

    protected get doHaveToWait() {
        if (this.sentAt) {
            let time = 60 * 1000 - (Date.now() - this.sentAt);
            if (time > 0) {
                time = Math.floor(time / 1000);
                this.toast.present(`操作太频繁，请${time}秒后再试.`);
                return time;
            }
        }
    }

    onKeyPress(){
        this.detectChanges();
    }
}
