import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommunityProfilePage } from 'pages/community-profile/community-profile';

@NgModule({
    declarations: [ CommunityProfilePage ],
    imports: [ IonicPageModule.forChild(CommunityProfilePage) ],
    exports: [ CommunityProfilePage ],
})
export class CommunityProfilePageModule {}
