import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, ViewController, ModalController } from 'ionic-angular';
import { Log } from 'api/lib/logger';
import { User, Community } from 'api/models';
import { EnhancedBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { QrCodeImageComponent } from 'pages/community-profile/qrcodeimage.component';
import QRCoder from '../../lib/qrcoder';
import { PERIOD_OF_INVITATION_VALIDITY } from 'api/lib/constants';
import { GetPhonePage } from 'pages/login/get-phone/get-phone';
import { TabsPage } from 'pages/tabs/tabs';
import { CommunityListSlidesPage } from 'pages/community-list-slides/community-list-slides';

export interface CommunityVerifyCode {
    communityId?: ID;
    userId?: ID;
    createdAt?: number;
    untilDate?: number;
}

//Ref: https://ionicframework.com/docs/nightly/api/navigation/IonicPage/
@IonicPage({
    segment: '/communities/join/:communityId/:invitorId/:createdAt',
    priority: 'high',
})
@Component({
    selector: 'page-community-profile',
    templateUrl: 'community-profile.html',
})
export class CommunityProfilePage extends EnhancedBase implements OnInit {
    communityId: string;
    community: Community;
    communityName: string;
    myName: string;
    communityDescription: string;
    isCreator: boolean = false;
    isFirstPage: boolean = false;
    members: User[];
    code: CommunityVerifyCode = {};

    constructor (
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public modalCtrl: ModalController,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.checkLogin();

        this.communityId = this.navParams.get('communityId');

        // 没有指定CommunityId, 是用户将要新建社群
        if (!this.communityId) return (this.isCreator = true);

        this.isFirstPage = this.viewCtrl.isFirst();

        this.checkCode();
        this.autorun(() => {
            this.community = Community.get(this.communityId);
            if (!this.community) return;
            const settings = User.Me.communitySettings.find((item) => item.communityId == this.communityId);
            this.communityName = (settings && settings.communityName) || this.community.name;
            this.myName = (settings && settings.myName) || User.Me.nickname;
            this.communityDescription = this.community.description;
            this.isCreator = this.community.createdBy == Meteor.userId();
        });
    }

    checkLogin () {
        Log.w('check login');
        if (!Meteor.userId())
            this.navCtrl.setRoot(GetPhonePage, {
                origin: [
                    { page: TabsPage },
                    { page: CommunityListSlidesPage },
                    this.viewCtrl,
                ],
            });
    }

    // 检查邀请码
    checkCode () {
        this.getCodeFromNavParams();
        const { communityId, userId, createdAt } = this.code;
        if (!communityId || !userId || !createdAt) return;

        // 不能自己邀请自己
        // if (userId == Meteor.userId()) return;

        // 过了有效期
        if (Date.now() > this.code.untilDate) return;

        this.call$('joinCommunity', communityId, userId).subscribe(
            () =>
                this.fetch$('membersOfCommunities', 1, [
                    communityId,
                ]),
            (err) => this.showError(err),
        );
    }

    getCodeFromNavParams () {
        this.code.communityId = this.navParams.get('communityId');
        this.code.userId = this.navParams.get('userId');
        this.code.createdAt = this.navParams.get('createdAt');
        if (this.code.createdAt) this.code.untilDate = this.code.createdAt + PERIOD_OF_INVITATION_VALIDITY;
    }

    getQrCode () {
        this.call$<number>('inviteToJoinCommunity', this.communityId).subscribe(
            (createdAt) => this.presentQrCode(createdAt),
            (err) => this.showError(err),
        );
    }

    presentQrCode (createdAt: number) {
        const url = [
            'https://www.bookheart.cn:8100',
            '#',
            'communities',
            'join',
            this.communityId,
            Meteor.userId(),
            createdAt,
        ].join('/');
        Log.i('generated QrCode url', url);
        const src = QRCoder.generatePNG(url);
        this.showQrCodeImage(src);
    }

    showQrCodeImage (src: string) {
        const popover = this.modalCtrl.create(
            QrCodeImageComponent,
            {
                src,
                userName: this.myName,
                communityName: this.communityName,
                untilDate: this.code.untilDate,
            },
            // { cssClass: 'large-popover' },
        );
        popover.present();
    }

    save () {
        this.newLoading();
        this.call$(
            'upsertCommunity',
            this.communityId,
            this.communityName,
            this.myName,
            this.communityDescription,
        ).subscribe(
            () => {
                this.fetch$('loginUserCommunities', 1, () => this.back());
            },
            (err) => this.showError(err),
        );
    }

    cancel () {
        this.navCtrl.setRoot(TabsPage);
    }

    quit () {
        this.newLoading();
        this.call$('quitCommunity', this.communityId).subscribe(
            () => {
                this.fetch$('loginUserCommunities', 1, () => this.back());
            },
            (err) => this.showError(err),
        );
    }

    back () {
        this.dismissLoading();
        if (this.viewCtrl.isFirst()) this.navCtrl.setRoot(TabsPage, {});
        else {
            const previous = this.navCtrl.getPrevious().instance;
            const slider = previous.slider;
            this.navCtrl.pop().then(() => slider && slider.slideTo(0));
        }
    }
}
