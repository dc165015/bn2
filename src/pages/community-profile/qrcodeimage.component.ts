import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { ViewController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
    selector: 'qrcode-image',
    templateUrl: 'qrcodeimage.component.html',
})
export class QrCodeImageComponent {
    src: string;
    untilDate: number;
    communityName: string;
    userName: string;

    constructor(public navParams: NavParams, public viewCtrl: ViewController, private socialSharing: SocialSharing) {}

    ngOnInit() {
        this.src = this.navParams.get('src');
        this.communityName = this.navParams.get('communityName');
        this.userName = this.navParams.get('userName');
        this.untilDate = this.navParams.get('untilDate');
    }

    get expireDate() {
        return moment(this.untilDate).format('YYYY-MM-DD HH:mm');
    }

    cancel() {
        this.viewCtrl.dismiss();
    }

    share() {
        this.socialSharing.share(`${this.userName}邀请您加入星空书友群${this.communityName}`, '星空入群邀请码', this.src, this.src);
    }
}
