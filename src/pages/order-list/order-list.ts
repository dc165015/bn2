import { Component, ViewChildren, QueryList, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { NavController, LoadingController, InfiniteScroll } from 'ionic-angular';
import { SearchPage } from 'pages/search/search';
import { OrderListSectionComponent } from 'components/order-list-section/order-list-section';
import { User } from 'api/models';
import { EnhancedBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';

@Component({
    selector: 'page-order-list',
    templateUrl: 'order-list.html',
})
export class OrderListPage extends EnhancedBase implements OnInit {
    userId;
    user: User;
    display: 'borrowed' | 'lent' = 'borrowed';

    borrowedCount: number = 0;
    lentCount: number = 0;

    isBorrowedListSet: boolean = true;
    isLentListSet: boolean = false;

    @ViewChild(InfiniteScroll) infiniteScroll;

    lentList: OrderListSectionComponent;
    borrowedList: OrderListSectionComponent;
    @ViewChildren(OrderListSectionComponent) orderLists: QueryList<OrderListSectionComponent>;

    constructor(
        public navCtrl: NavController,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
        this.userId = Meteor.userId();
    }

    ngOnInit() {
        this.autorun(() => {
            this.user = User.get(this.userId);
            const profile = this.user.profile;
            if (!profile) return;
            this.borrowedCount = this.user.profile.borrowedCount;
            this.lentCount = this.user.profile.lentCount;
        });
    }

    ngAfterViewInit() {
        this.assignLists(this.orderLists);

        this.orderLists.changes.subscribe((lists) => this.assignLists(lists));
    }

    protected assignLists(lists) {
        lists.forEach((list) => {
            this.assignWidgets(list);
            this.assignListByType(list);
        });
    }

    protected assignListByType(list) {
        if (list.type == 'borrowed') this.borrowedList = list;
        else if (list.type == 'lent') this.lentList = list;
    }

    protected assignWidgets(list) {
        list.loadingCtrl = this.loadingCtrl;
        list.infiniteScroll = this.infiniteScroll;
    }

    pushSearchPage(ev) {
        this.navCtrl.push(SearchPage, { type: 'Order' });
    }

    segmentChanged() {
        if (this.display == 'borrowed') this.isBorrowedListSet = true;
        else if (this.display == 'lent') this.isLentListSet = true;
        this.detectChanges();
    }

    loadMore(infiniteScroll?) {
        if (this.display == 'borrowed') this.borrowedList.loadMore(infiniteScroll);
        else if (this.display == 'lent') this.lentList.loadMore(infiniteScroll);
    }
}
