import {
    Component,
    OnInit,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
    ViewChildren,
    QueryList,
    ViewChild,
} from '@angular/core';
import { NavController, NavParams, LoadingController, InfiniteScroll } from 'ionic-angular';
import { User } from 'api/models/user';
import { ToastProvider } from 'providers/toast';
import { EnhancedBase } from '../../lib/enhanced-base';
import { Chats } from 'api/collections/chats';
import { MessagesPage } from 'pages/messages/messages';
import { OrderListSectionComponent } from 'components/order-list-section/order-list-section';
import { CopyListSectionComponent } from 'components/copy-list-section/copy-list-section';

@Component({
    selector: 'page-user-profile',
    templateUrl: 'user-profile.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProfilePage extends EnhancedBase implements OnInit {
    userId: ID;
    user: User;
    display: string = 'copies';
    batchCounter: number = 1;

    borrowedCount: number = 0;
    lentCount: number = 0;
    copyCount: number = 0;

    isBorrowedListSet: boolean = false;
    isLentListSet: boolean = false;
    isCopyListSet: boolean = true;

    @ViewChild(InfiniteScroll) infiniteScroll;

    @ViewChild(CopyListSectionComponent) copyList;

    lentList: OrderListSectionComponent;
    borrowedList: OrderListSectionComponent;
    @ViewChildren(OrderListSectionComponent) orderLists: QueryList<OrderListSectionComponent>;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
        this.userId = navParams.get('userId');
    }

    ngOnInit() {
        this.subscribeUser();
        this.autorun(() => {
            this.user = User.get(this.userId) || User.Me;
            const profile = this.user.profile;
            if (!profile) return;
            this.borrowedCount = this.user.profile.borrowedCount;
            this.lentCount = this.user.profile.lentCount;
            this.copyCount = this.user.profile.copyCount;
        });
    }

    ngAfterViewInit() {
        this.orderLists.changes.subscribe((lists) =>
            lists.forEach((list) => {
                this.assignWidgets(list);
                if (list.type == 'borrowed') this.borrowedList = list;
                else if (list.type == 'lent') this.lentList = list;
            }),
        );
        this.assignWidgets(this.copyList);
    }

    assignWidgets(list) {
        list.loadingCtrl = this.loadingCtrl;
        list.infiniteScroll = this.infiniteScroll;
    }

    subscribeUser() {
        this.fetch$('users', this.batchCounter++, { _id: this.userId });
    }

    showMessages(ev) {
        const loginUserId = Meteor.userId();
        const memberIds =
            this.user._id == loginUserId ? [ User.System._id, loginUserId ] : [ this.user._id, loginUserId ];
        this.pushMessagesPage(memberIds) || this.getChat$(memberIds);
    }

    protected pushMessagesPage(memberIds: ID[]) {
        let chat = this.getLocalChat(memberIds);
        if (chat) this.navCtrl.push(MessagesPage, { chatId: chat._id });
        return chat;
    }

    protected getChat$(memberIds: ID[]) {
        this.fetch$('chats', 1, this.user._id, () => this.pushMessagesPage(memberIds));
    }

    getLocalChat(memberIds: ID[]) {
        return Chats.findOne({ memberIds: { $all: memberIds } });
    }

    segmentChanged() {
        if (this.display == 'borrowed') this.isBorrowedListSet = true;
        else if (this.display == 'lent') this.isLentListSet = true;
        else if (this.display == 'copies') this.isCopyListSet = true;
        this.detectChanges();
    }

    loadMore(infiniteScroll?) {
        if (this.display == 'borrowed') this.borrowedList.loadMore(infiniteScroll);
        else if (this.display == 'lent') this.lentList.loadMore(infiniteScroll);
        else if (this.display == 'copies') this.copyList.loadMore(infiniteScroll);
    }
}
