import { Component, OnInit } from '@angular/core';
import { Community, User } from 'api/models';
import { NavController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Meteor } from 'meteor/meteor';
import { CommunityProfilePage } from 'pages/community-profile/community-profile';

@Component({
    selector: 'select-community',
    templateUrl: 'select-community.component.html',
})
export class SelectCommunityComponent implements OnInit {
    defaultCommunityId: ID;
    communities: Community[] | Mongo.Cursor<Community>;
    user: User;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController) {}

    ngOnInit() {
        this.user = User.Me;
        this.defaultCommunityId = this.user.communityId;
        this.communities = this.user.communities;
    }

    changeDefaultCommunity() {
        if (this.defaultCommunityId != this.user.communityId)
            Meteor.call('changeDefaultCommunity', this.defaultCommunityId);
    }

    addNewCommunity() {
        this.viewCtrl.dismiss();
        this.navCtrl.push(CommunityProfilePage);
    }

    close() {
        this.viewCtrl.dismiss();
    }
}
