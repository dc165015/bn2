import { Component, ViewChild, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { User } from 'api/models/user';
import { PictureProvider } from 'providers/picture';
import { ToastProvider } from 'providers/toast';
import { UsernameValidator } from './username.validator';
import { Picture } from 'api/models/picture';
import { MIN_NAME_LENGTH, MAX_NAME_LENGTH, MAX_DESCRIPTION_LENGTH } from 'api/lib/constants';
import { PreloadImage } from 'components/preload-image/preload-image';
import { EnhancedBase } from '../../lib/enhanced-base';
import { Log } from 'api/lib/logger';
import { SelectCommunityComponent } from 'pages/my-profile-edit/select-community.component';

@Component({
    selector: 'page-my-profile-edit',
    templateUrl: 'my-profile-edit.html',
    changeDetection: ChangeDetectionStrategy.Default,
})
export class MyProfileEditPage extends EnhancedBase implements OnInit, OnDestroy {
    settingsForm: FormGroup;
    user: User;
    communityName: string;
    computation: Tracker.Computation;
    @ViewChild(PreloadImage) avatar: PreloadImage;

    constructor (
        public navCtrl: NavController,
        public formBuilder: FormBuilder,
        public navParams: NavParams,
        public pictureProvider: PictureProvider,
        public modalCtrl: ModalController,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.settingsForm = new FormGroup({
            name: new FormControl(
                '',
                Validators.compose([
                    UsernameValidator.forbiddenName,
                    Validators.minLength(MIN_NAME_LENGTH),
                    Validators.maxLength(MAX_NAME_LENGTH),
                    Validators.required,
                    Validators.pattern(/^[^\s][a-zA-Z0-9_\s\u4e00-\u9fa5]*[^\s]$/),
                ]),
            ),
            description: new FormControl('', Validators.maxLength(MAX_DESCRIPTION_LENGTH)),
        });
        this.computation = Tracker.autorun(() => {
            Log.w('my profile edit is running...');
            this.user = User.Me;
            const community = this.user.community;
            this.communityName = community && community.name;
            this.pathValue();
            this.cdf.detectChanges();
        });
    }

    pathValue () {
        this.settingsForm.patchValue({
            name: this.user.profile.name,
            description: this.user.profile.description,
        });
    }

    selectProfilePicture (): void {
        this.pictureProvider
            .askPicture('avatar', true)
            .then((blob) => {
                this.avatar._loaded(false);
                this.uploadProfilePicture(blob);
            })
            .catch((err) => this.showError(err));
    }

    uploadProfilePicture (blob): void {
        this.pictureProvider
            .upload(blob, { for: Picture.FOR.USER_AVATAR })
            .then((picture) => {
                this.avatar.src = picture.url;
                this.avatar._loaded(true);
            })
            .catch((err) => this.showError(err));
    }

    updateProfile () {
        const name = this.settingsForm.get('name').value;
        const description = this.settingsForm.get('description').value;
        this.call$('updateProfile', name, description).subscribe({
            next: () => {
                this.navCtrl.pop();
            },
            error: (err) => this.showError(err),
        });
    }

    onSubmit () {
        if (this.settingsForm.valid && this.settingsForm.dirty) this.updateProfile();
    }

    validation_messages = {
        name: [
            { type: 'required', message: '名字不能空着' },
            { type: 'minlength', message: `最少${MIN_NAME_LENGTH}个字` },
            { type: 'maxlength', message: `最多${MAX_NAME_LENGTH}个字` },
            { type: 'pattern', message: '只能包含英文字母、数字、汉字、下划线和空格，\n首尾不能有空格' },
            { type: 'forbiddenName', message: '不能含有敏感词' },
        ],
        description: [
            { type: 'maxlength', message: `最多${MAX_DESCRIPTION_LENGTH}个字` },
        ],
    };

    setupCommunities (event) {
        const popover = this.modalCtrl.create(SelectCommunityComponent);
        popover.present();
    }

    ngOnDestroy () {
        this.computation.stop();
    }
}
