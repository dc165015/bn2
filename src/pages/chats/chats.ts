import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavController, ModalController, LoadingController } from 'ionic-angular';
import { Chats } from 'api/collections/chats';
import { MessagesPage } from 'pages/messages/messages';
import { ToastProvider } from 'providers/toast';
import { EnhancedBase } from '../../lib/enhanced-base';
import { User } from 'api/models';
import { NewChatPage } from 'pages/new-chat/new-chat';

@Component({
    selector: 'page-chats',
    templateUrl: 'chats.html',
})
export class ChatsPage extends EnhancedBase implements OnInit {
    chats;
    senderId: ID;
    batchCounter: number = 1;

    constructor(
        public navCtrl: NavController,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit() {
        this.autorun(() => {
            this.chats = Chats.find({}).fetch().filter(chat => !chat.memberIds.includes(User.System._id));
        });
        this.initLoad();
    }

    subscribeChats() {
        this.fetch$('chats', this.batchCounter++);
    }

    showMessages(chat): void {
        this.navCtrl.push(MessagesPage, { chatId: chat._id, handleRemoveChat: () => this.removeChat(chat) });
        this.senderId = Meteor.userId();
    }

    addChat() {
        this.navCtrl.push(NewChatPage);
    }

    removeChat(chat) {
        this.call$('removeChat', chat._id).subscribe({ error: (err) => this.showError(err) });
    }

    loadMore(infiniteScroll?) {
        super.loadMore(infiniteScroll);
        // TODO: implement subscribe on books of the specified tag
        this.subscribeChats();
    }
}
