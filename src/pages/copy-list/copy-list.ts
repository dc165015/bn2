import { Component, ChangeDetectorRef, ViewChild, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, InfiniteScroll } from 'ionic-angular';
import { EnhancedBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { User } from 'api/models';
import { CopyProvider } from 'providers/copies';
import { SearchPage } from 'pages/search/search';
import { CopyListSectionComponent } from 'components/copy-list-section/copy-list-section';

@Component({
    selector: 'page-copy-list',
    templateUrl: 'copy-list.html',
})
export class CopyListPage extends EnhancedBase implements OnInit {
    userId: ID;
    user: User;
    @ViewChild(InfiniteScroll) infiniteScroll;
    @ViewChild(CopyListSectionComponent) copyList;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit() {
        this.userId = this.navParams.get('userId') || Meteor.userId();
    }

    ngAfterViewInit() {
        this.assignWidgets(this.copyList);
    }

    protected assignWidgets(list) {
        list.loadingCtrl = this.loadingCtrl;
        list.infiniteScroll = this.infiniteScroll;
    }

    pushSearchPage(ev) {
        this.navCtrl.push(SearchPage, { type: 'MyCopy' });
    }

    loadMore(infiniteScroll?) {
        this.copyList.loadMore(infiniteScroll);
    }
}
