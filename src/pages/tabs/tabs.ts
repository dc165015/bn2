import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Tab0Root, Tab1Root, Tab2Root, Tab3Root, Tab4Root } from '..';
import { NavController, Tabs, Events, LoadingController } from 'ionic-angular';
import { Badge } from '@ionic-native/badge';
import { GetPhonePage } from 'pages/login/get-phone/get-phone';
import { Log } from 'api/lib/logger';
import { EnhancedBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { Message } from 'api/models';

@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html',
})
export class TabsPage extends EnhancedBase {
    @ViewChild('mainTabs') tabs: Tabs;
    @ViewChild('mainTab0') mainTab0;
    @ViewChild('mainTab1') mainTab1;
    @ViewChild('mainTab2') mainTab2;
    @ViewChild('mainTab3') mainTab3;
    @ViewChild('mainTab4') mainTab4;

    tab0Root: any = Tab0Root;
    tab0Title = '书库';
    tab0Params: { communityId: ID };

    tab1Root: any = Tab1Root;
    tab1Title = '书友';
    tab1Params: { communityId: ID };

    tab2Root: any = Tab2Root;
    tab2Title = '扫书';
    tab2Params: { copyId: ID };

    tab3Root: any = Tab3Root;
    tab3Title = '借还';
    tab3Params: { selectedOrderId: ID };

    tab4Root: any = Tab4Root;
    tab4Title = '设置';
    tab4Params;

    selectedIndex = 0;

    unreadMessages: Message[] = [];
    lastUnreadMessage: Message;
    previousLastUnreadMessage: Message;
    hasNewMessages: boolean = false;

    constructor (
        public navCtrl: NavController,
        public events: Events,
        public badge: Badge,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.autorun(() => {
            if (!Meteor.userId()) this.navCtrl.setRoot(GetPhonePage);
            this.unreadMessages = Message.getUnread().fetch();
            const count = this.unreadMessages.length;
            if (!count) return;
            this.notify();
            this.hasNewMessages = true;
            this.badge.set(count);
        });
    }

    ionViewDidEnter () {
        this.events.subscribe('change-maintab', (tab, tabParams) => {
            Log.l('tabs received change maintab events', tab, tabParams);
            if (tab == 0) this.tab0Params = tabParams;
            else if (tab == 1) this.tab1Params = tabParams;
            else if (tab == 2) this.tab2Params = tabParams;
            else if (tab == 3) this.tab3Params = tabParams;
            else if (tab == 4) this.tab4Params = tabParams;
            this.tabs.select(tab);
        });
    }

    notify () {
        if (!this.unreadMessages) return;
        this.lastUnreadMessage = this.unreadMessages[0];
        if (
            !this.previousLastUnreadMessage ||
            (this.previousLastUnreadMessage && this.previousLastUnreadMessage._id != this.lastUnreadMessage._id)
        ) {
            if (this.lastUnreadMessage) this.toast.present(this.lastUnreadMessage.toNotification());
            this.previousLastUnreadMessage = this.lastUnreadMessage;
        }
        this.badge.set(this.unreadMessages.length);
    }

    onTabChanged () {
        Log.i('tabs changed');
        this.detectChanges();
    }
}
