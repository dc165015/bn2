import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { EnhancedBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { BookProfilePage } from 'pages/book-profile/book-profile';
import { UserProfilePage } from 'pages/user-profile/user-profile';
import { Books, Users, Orders } from 'api/collections';
import { Log } from 'api/lib/logger';
import { Copy, User } from 'api/models';

@Component({
    selector: 'page-search',
    templateUrl: 'search.html',
})
export class SearchPage extends EnhancedBase {
    protected _value: string;
    items: any[] | Mongo.Cursor<any>;
    type: 'Book' | 'User' | 'Order' | 'MyCopy';
    searching: boolean = true;
    placeholder: string = '输入关键词查找';
    batchNumber: number = 1;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
        this.type = this.navParams.get('type');
        this.placeholder = this.getPlaceHolder();
    }

    set value(val: string) {
        this._value = val.trim();
    }

    get value() {
        return this._value;
    }

    ngOnInit() {
        this.autorun(() => {
            this.items = this.searchLocal();
        });
        this.loadMore();
    }

    getPlaceHolder() {
        switch (this.type) {
            case 'Book':
            case 'MyCopy':
                return '输入书名、作者、标签、ISBN';
            case 'User':
                return '输入用户名';
            case 'Order':
                return '输入单号、书名、用户名';
        }
    }

    search(val) {
        this.value = val || '';
        this.autorun();
        this.loadMore();
    }

    searchRemote() {
        if (!this.value) return this.completeFetch$();
        this.fetchNow$('search' + this.type, this.batchNumber++, this.value, () => {
            Log.l('remote search done.');
        });
    }

    searchLocal() {
        if (!this.value) return [];
        const reg = { $regex: this.value, $options: 'i' };
        switch (this.type) {
            case 'Book':
                return this._searchLocalBook(reg);
            case 'MyCopy':
                return this._searchLocalBook(reg, 'mine');
            case 'User':
                return this._searchLocalUser(reg);
            case 'Order':
                return this._searchLocalOrder(reg);
        }
    }

    protected _searchLocalBook(reg: object, type: 'mine' | 'all' = 'all') {
        let bookIds;
        let selector = {};
        if (type == 'mine') {
            bookIds = User.Me.copies.map((copy) => copy.bookId);
            selector = { _id: { $in: bookIds } };
        }

        return Books.find({
            ...selector,
            $or: [
                { title: reg },
                { subtitle: reg },
                { alt_title: reg },
                { origin_title: reg },
                { author: reg },
                { translator: reg },
                { 'tags.name': reg },
                { isbn13: reg },
                { isbn10: reg },
            ],
        });
    }

    protected _searchLocalUser(reg: object) {
        return Users.find({ 'profile.name': reg });
    }

    protected _searchLocalOrder(reg: object) {
        const loginUserId = Meteor.userId();
        const selector0 = { $or: [ { ownerId: loginUserId }, { borrowerId: loginUserId } ] };
        const selector1 = { $or: [ { ownerName: reg }, { borrowerName: reg }, { title: reg } ] };
        return Orders.find({ $and: [ selector0, selector1 ] });
    }

    openItem(item) {
        let target = this._getTarget();
        let params = this._getParams(item);
        this.navCtrl.push(target, params);
    }

    protected _getTarget() {
        switch (this.type) {
            case 'Book':
            case 'MyCopy':
                return BookProfilePage;
            case 'User':
                return UserProfilePage;
        }
    }

    protected _getParams(item) {
        switch (this.type) {
            case 'Book':
            case 'MyCopy':
                return { bookId: item._id };
            case 'User':
                return { userId: item._id };
        }
    }

    loadMore(infiniteScroll?) {
        super.loadMore(infiniteScroll);
        this.searchRemote();
    }
}
