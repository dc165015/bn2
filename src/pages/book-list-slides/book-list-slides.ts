import { Component, ViewChild, OnInit, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { NavController, NavParams, PopoverController, Slides, LoadingController, Events } from 'ionic-angular';
import { ToastProvider } from 'providers/toast';
import { User } from 'api/models/user';
import { Community } from 'api/models/community';
import { Books } from 'api/collections/books';
import { EnhancedBase } from '../../lib/enhanced-base';
import { BookProfilePage } from 'pages/book-profile/book-profile';
import { Copies } from 'api/collections';
import { SearchPage } from 'pages/search/search';

// TODO: 按用户数据推导用户感兴趣并容易获取的书的标签
const SubscribedTags = [
    '全部',
];

@Component({
    selector: 'page-book-list-slides',
    templateUrl: 'book-list-slides.html',
    changeDetection: ChangeDetectionStrategy.Default,
})
export class BookListSlidesPage extends EnhancedBase implements OnInit, OnDestroy {
    title: string = '书  库';
    batchCounter: number = 1;
    communityId: ID;
    community: Community;
    selectedTags: string[] = SubscribedTags;
    // booksOfTag_BatchCounterMap: Map<string, number> = new Map();
    tags;
    groups;
    books;

    @ViewChild('slider') slider: Slides;

    constructor (
        public navCtrl: NavController,
        public navParams: NavParams,
        public events: Events,
        public popoverCtrl: PopoverController,
        public toast: ToastProvider,
        public loadingCtrl: LoadingController,
        public cdf: ChangeDetectorRef,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.communityId = this.navParams.get('communityId');
        this.subscribeTabsChangeEvent();
        this.autorun(() => {
            this.community = Community.get(this.communityId);
            this.community = this.community || (User.Me && User.Me.community) || Community.All;
            this.books = this.community.books;
            this.groups = Books.groupByTags(this.books, true);
            this.tags = Array.from(this.groups.keys());
            if (this.groups.size != 0) this.slider.update();
            this.onSlideDidChange();
        });
        if (this.groups.size <= 2) this.initLoad();
    }

    ngAfterContentInit () {
        this.onSlideDidChange();
    }

    subscribeTabsChangeEvent () {
        this.events.subscribe('change-maintab', (tabIndex, tabParams) => {
            if (this.communityId != tabParams.communityId) {
                this.updateWhole(tabParams.communityId);
            }
        });
    }

    subscribeBooksOfCommunity () {
        this.fetch$('booksOfCommunity', this.batchCounter++, this.community._id, () => {
            if (Copies.find({ communityIds: this.community._id }).count() >= this.community.copyCount)
                this.infiniteScroll && this.infiniteScroll.enable(false);
        });
    }

    getBooksOfTag (tag) {
        // Log.l(`...get tag <${tag}>`);
        return this.groups.get(tag);
    }

    getGroupSize (tag) {
        return this.groups.get(tag).size;
    }

    get currentSlideIndex () {
        return this.slider.getActiveIndex();
    }

    get currentSlideTag () {
        return this.selectedTags[this.currentSlideIndex - 1];
    }

    isSelected (tag: string) {
        return this.selectedTags.indexOf(tag) != -1;
    }

    updateSelection (ev, tag) {
        const index = this.selectedTags.indexOf(tag);
        if (index != -1) {
            this.selectedTags.splice(index, 1);
        }
        else {
            this.selectedTags.splice(this.selectedTags.length, 0, tag);
        }
        this.detectChanges();
    }

    onSlideDidChange () {
        if (this.currentSlideIndex === 0) {
            this.title = '选择兴趣';
        }
        else {
            if (!this.currentSlideTag || !this.community) return;
            // 跳过第一页标签设置页标题
            this.title =
                this.community.name + ' - ' + this.currentSlideTag + `(${this.getGroupSize(this.currentSlideTag)}册)`;
        }
    }

    viewMembers (event) {
        this.events.publish('change-maintab', 1, { communityId: this.community._id });
    }

    updateWhole (communityId: ID) {
        if (!User.Me.communities.fetch().find((community) => community._id == communityId)) return;
        this.communityId = communityId;
        this.autorun();
    }

    onClick (event, book) {
        this.navCtrl.push(BookProfilePage, { bookId: book._id });
    }

    loadMore (infiniteScroll?) {
        super.loadMore(infiniteScroll);
        // TODO: implement subscribe on books of the specified tag
        this.subscribeBooksOfCommunity();
    }

    pushSearchPage (ev) {
        this.navCtrl.push(SearchPage, { type: 'Book' });
    }
}
