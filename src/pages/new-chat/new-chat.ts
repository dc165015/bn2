import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { NavController, NavParams, ViewController, LoadingController } from "ionic-angular";
import { Observable, Subscription } from "rxjs";
import { User } from "api/models/user";
import { Chats } from "api/collections/chats";
import { ToastProvider } from "providers/toast";
import { EnhancedBase } from "../../lib/enhanced-base";
import { flatten, uniq } from "lodash";

@Component({
    selector: 'page-new-chat',
    templateUrl: 'new-chat.html',
})
export class NewChatPage extends EnhancedBase implements OnInit {
    _value: string;
    senderId: ID;
    users;
    users$: Observable<User[]>;
    usersSubscription: Subscription;
    batchCounter: number = 1;

    constructor(
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public navParams: NavParams,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
        this.senderId = Meteor.userId();
    }

    set value(val: string) {
        this._value = val.trim();
    }

    get value() {
        return this._value;
    }

    ngOnInit() {
        this.autorun(() => {
            const existedChatUserIds = uniq(flatten(Chats.find({}).map(chat => chat.memberIds)));
            const selector = this.value ? { 'profile.name': { $regex: this.value, $options: 'i' } } : {};
            this.users = User.find({ _id: { $nin: existedChatUserIds }, ...selector }).fetch();
        });
    }

    search(val) {
        this.value = val || '';
        this.autorun();
        this.loadMore();
    }

    addChat(user): void {
        this.call$('addChat', user._id).subscribe({
            next: () => this.navCtrl.pop(),
            error: (err) => this.showError(err),
        });
    }

    subscribeUsers() {
        if (!this.value) return this.completeFetch$();
        // Fetch all users matching search pattern
        this.fetch$('users', this.batchCounter++, { profileName: this.value });
    }

    loadMore(infiniteScroll?) {
        super.loadMore(infiniteScroll);
        this.subscribeUsers();
    }
}
