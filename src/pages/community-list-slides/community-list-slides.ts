import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, Slides, LoadingController, Events, ActionSheetController } from 'ionic-angular';
import { ToastProvider } from 'providers/toast';
import { User } from 'api/models/user';
import { Community } from 'api/models/community';
import { EnhancedBase } from '../../lib/enhanced-base';
import { Users } from 'api/collections';
import { UserProfilePage } from 'pages/user-profile/user-profile';
import { SearchPage } from 'pages/search/search';
import { Log } from 'api/lib/logger';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { from } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { PERIOD_OF_INVITATION_VALIDITY } from 'api/lib/constants';
import { Meteor } from 'meteor/meteor';

@Component({
    selector: 'page-community-list-slides',
    templateUrl: 'community-list-slides.html',
})
export class CommunityListSlidesPage extends EnhancedBase implements OnInit {
    communityId: ID;

    user: User;
    communities: Community[];
    groups: Map<ID, Mongo.Cursor<User>> = new Map();
    title: string;
    batchCounter: number = 1;

    @ViewChild('slider') slider: Slides;
    infiniteScroll: any;

    constructor (
        public navCtrl: NavController,
        public navParams: NavParams,
        public events: Events,
        public actionSheetCtrl: ActionSheetController,
        public scanner: BarcodeScanner,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.communityId = this.navParams.get('communityId');
        this.subscribeTabsChangeEvent();
        this.autorun(() => {
            this.user = User.Me;
            this.communities = Community.find(
                { _id: { $in: this.user.communityIds } },
                { sort: { createdAt: -1 } },
            ).fetch();
            for (let community of this.communities) {
                const users = Users.find({ _id: { $ne: User.System._id }, communityIds: community._id });
                this.groups.set(community._id, users);
            }
            this.onSlideDidChange();
        });
        this.initLoad();
    }

    initLoad () {
        const community = this.user.community;
        if (!community || community.members.count() <= 2) super.initLoad();
    }

    ionViewWillEnter () {
        this._autorunner();
    }

    subscribeTabsChangeEvent () {
        this.events.subscribe('change-maintab', (tabIndex, tabParams) => {
            this.communityId = tabParams.communityId;
            const index = this.communities.findIndex((community) => community._id == this.communityId);
            if (this.currentSlideIndex != index) this.slideTo(index);
        });
    }

    subscribeMembersOfCommunities (communityId?: ID, onReady?: Function) {
        const _communityId = communityId || this.currentSlideCommunityId || User.Me.communityId;
        this.fetch$(
            'membersOfCommunities',
            this.batchCounter++,
            [
                _communityId,
            ],
            onReady,
        );
    }

    getMembersOf (communityId) {
        // Log.l('get members...');
        return this.groups.get(communityId);
    }

    get currentSlideIndex () {
        return this.slider.getActiveIndex() || 0;
    }

    get currentSlideCommunityId () {
        const community = this.communities[this.currentSlideIndex];
        return community && community._id;
    }

    onSlideDidChange () {
        let currentIndex = this.slider.getActiveIndex();
        this.setTitle(currentIndex);
    }

    protected setTitle (index: number = 0) {
        const community = this.communities[index];
        if (community) {
            const size = this.getCommunitySize(community._id);
            this.title = this.getMyCommuntiyName(community) + `(${size}人)`;
        }
    }

    protected getMyCommuntiyName (community: Community) {
        const settings = this.user.communitySettings.find((item) => item.communityId == community._id);
        return (settings && settings.communityName) || community.name;
    }

    protected getCommunitySize (communityId: ID) {
        const group = this.groups.get(communityId);
        return group ? group.count() : 0;
    }

    slideTo (index: number) {
        this.detectChanges(() => {
            this.slider.slideTo(index);
            this.onSlideDidChange();
        });
    }

    loadMore (infiniteScroll?) {
        super.loadMore(infiniteScroll);
        this.subscribeMembersOfCommunities();
    }

    onClick (event, member) {
        this.navCtrl.push(UserProfilePage, { userId: member._id });
    }

    pushSearchPage (ev) {
        this.navCtrl.push(SearchPage, { type: 'User' });
    }

    moreMenu (event) {
        let actions = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: '群藏书',
                    handler: () =>
                        this.events.publish('change-maintab', 0, { communityId: this.currentSlideCommunityId }),
                },
                {
                    text: '群设置',
                    handler: () =>
                        this.navCtrl.push('CommunityProfilePage', { communityId: this.currentSlideCommunityId }),
                },
                {
                    text: '新建群',
                    handler: () => this.navCtrl.push('CommunityProfilePage'),
                },
                {
                    text: '扫描群二维码',
                    handler: () => this.scanQrCode(),
                },
                {
                    text: '取消',
                    role: 'cancel',
                },
            ],
        });

        actions.present();
    }

    scanQrCode () {
        from(this.scanner.scan())
            .pipe(
                map((code) => code.text),
                map((text) => {
                    Log.l('Got QrCode:', text);
                    return extractInvitationCode(text);
                }),
                switchMap(({ communityId, userId, createdAt }) =>
                    Meteor.call$('joinCommunity', communityId, userId, createdAt),
                ),
                tap((communityId) => {
                    this.subscribeMembersOfCommunities(communityId, () => this.toast.present('您已成功加入该群!'));
                }),
            )
            .subscribe(() => {}, (err) => this.showError(err));
    }
}

function extractInvitationCode (text){
    const matches = /https?:\/\/([^\/]+)\/#\/communities\/join\/([^\/]+)\/([^\/]+)\/([\d]+)/.exec(text);
    if (!matches) throw new Error('二维码错误');
    const [
        ,
        ,
        communityId,
        userId,
        createdAt,
    ] = matches;
    if (Date.now() - Number(createdAt) > PERIOD_OF_INVITATION_VALIDITY) throw new Error('邀请码已过期');
    return { communityId, userId, createdAt };
}
