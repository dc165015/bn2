declare type ID = string;

declare module 'mongo/collection' {
    function aggregate(...args): any;
}

declare type MongoFindOptions = {
    sort?: Mongo.SortSpecifier;
    skip?: number;
    limit?: number;
    fields?: Mongo.FieldSpecifier;
    reactive?: boolean;
    transform?: Function;
};

declare type MongoSelector<T> = ID | Mongo.Selector<T>;
