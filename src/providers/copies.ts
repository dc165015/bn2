import { Injectable } from '@angular/core';
import { switchMap, mapTo, take } from 'rxjs/operators';
import { DoubanProvider } from 'providers/douban';
import { Meteor } from 'meteor/meteor';
import { Observable, of } from 'rxjs';
import { Book, Copy } from 'api/models';

@Injectable()
export class CopyProvider {
    constructor (public douban: DoubanProvider) {}

    addCopy$ (isbn: string) {
        return this.getFromDouban$(isbn).pipe(switchMap((book: Book) => this.submitCopy$(book)));
    }

    getFromDouban$ (isbn: string): Observable<Book> {
        return this.douban.getBookFromDouban(isbn);
    }

    submitCopy$ (book: Book): Observable<ID> {
        return Meteor.call$('addCopy', book).pipe(switchMap((bookId: ID) => this.retrieveCopy$(bookId)));
    }

    // 先看本地是否有记录, 没有时向服务器取
    retrieveCopy$ (bookId: ID, ownerId?: ID): Observable<ID> {
        ownerId = ownerId || Meteor.userId();
        const copy = Copy.findOne({ bookId, ownerId });
        const book = Book.findOne(bookId);
        return copy && book ? of(bookId) : this.fetchCopy$(bookId, ownerId);
    }

    // 从服务器取
    fetchCopy$ (bookId: ID, ownerId?: ID) {
        ownerId = ownerId || Meteor.userId();
        return Meteor.subscribe$('copy', 1, bookId, ownerId).pipe(mapTo(bookId));
    }
}
