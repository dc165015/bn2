import { Injectable } from '@angular/core';
import { Platform, ActionSheetController, AlertController } from 'ionic-angular';
import { UploadFS } from 'meteor/jalik:ufs';
import { PicturesStore } from 'api/collections/pictures';
import { Camera, MediaType, EncodingType } from '@ionic-native/camera';
import { Crop, CropOptions } from '@ionic-native/crop';
import { Log } from 'api/lib/logger';

interface FileObj {
    name: string;
    type: string;
    size: number;
    data: Blob;
}

@Injectable()
export class PictureProvider {
    constructor (
        public alertCtrl: AlertController,
        private platform: Platform,
        private camera: Camera,
        private crop: Crop,
        public actionSheetCtrl: ActionSheetController,
    ) {}

    askPicture (type: 'avatar' | 'book' | 'other' = 'book', crop: boolean = false) {
        return new Promise((resolve: (isFromCamera: boolean) => void, reject) => {
            const actionSheet = this.actionSheetCtrl.create({
                title: '怎样获取照片？',
                buttons: [
                    {
                        text: '用相机拍照',
                        role: 'destructive',
                        handler: () => {
                            resolve(true);
                        },
                    },
                    {
                        text: '从相册选取',
                        handler: () => {
                            resolve(false);
                            return true;
                        },
                    },
                    {
                        text: '取消',
                        role: 'cancel',
                    },
                ],
            });
            actionSheet.present();
        }).then((isFromCamera) => {
            return this.getPicture(type, isFromCamera, crop);
        });
    }

    getPicture (
        type: 'avatar' | 'book' | 'other' = 'book',
        isFromCamera: boolean,
        crop: boolean = false,
    ): Promise<FileObj> {
        if (!this.platform.is('cordova')) {
            return new Promise((resolve, reject) => {
                if (isFromCamera === true) {
                    reject(new Error("Can't access the camera on Browser"));
                }
                else {
                    try {
                        UploadFS.selectFile((file: File) => {
                            resolve({ data: file, name: file.name, type: file.type, size: file.size });
                        });
                    } catch (e) {
                        reject(e);
                    }
                }
            });
        }

        let options: CropOptions = { quality: 100 };
        if (type == 'avatar') options.targetHeight = options.targetWidth = 300;
        else if (type == 'book') (options.targetHeight = 1020), (options.targetWidth = 700);

        return this.camera
            .getPicture({
                encodingType: EncodingType.JPEG,
                mediaType: MediaType.PICTURE,
                correctOrientation: true,
                sourceType: isFromCamera ? 1 : 0,
            })
            .then((fileURI) => {
                Log.n('crop options:', options);
                return crop ? this.crop.crop(fileURI, options) : fileURI;
            })
            .then((croppedFileURI) => {
                return this.convertURItoBlob(croppedFileURI);
            });
    }

    upload (fileObj: FileObj, options?: { bookId?: ID; copyId?: ID; for?: string }): Promise<any> {
        return new Promise((resolve, reject) => {
            const pic: any = {
                userId: Meteor.userId(),
                name: fileObj.name,
                type: fileObj.type,
                size: fileObj.size,
                ...options, //TODO: validate pic on server
            };

            const upload = new UploadFS.Uploader({
                data: fileObj.data,
                file: pic,
                store: PicturesStore,
                onComplete: resolve,
                onError: reject,
            });

            upload.start();
        });
    }

    convertURItoBlob (url: string, options = {}): Promise<FileObj> {
        return new Promise((resolve, reject) => {
            const image = document.createElement('img');
            image.src = url;

            image.onload = async () => {
                try {
                    const dataURI = this.convertImageToDataURI(image, options);
                    const blob = this.convertDataURIToBlob(dataURI);
                    const pathname = new URL(url).pathname;
                    const filename = pathname.substring(pathname.lastIndexOf('/') + 1);
                    resolve({ name: filename, data: blob, size: blob.size, type: blob.type });
                } catch (e) {
                    Log.e(e);
                    reject(e);
                }
            };
        });
    }

    convertImageToDataURI (image: HTMLImageElement, { MAX_WIDTH = 400, MAX_HEIGHT = 400 } = {}): string {
        // Create an empty canvas element

        const canvas = document.createElement('canvas');

        var width = image.width,
            height = image.height;

        if (width > height) {
            if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
            }
        }
        else {
            if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
            }
        }

        canvas.width = width;
        canvas.height = height;

        // Copy the image contents to the canvas
        const context = canvas.getContext('2d');
        context.drawImage(image, 0, 0, width, height);

        // Get the data-URI formatted image
        // Firefox supports PNG and JPEG. You could check image.src to
        // guess the original format, but be aware the using 'image/jpg'
        // will re-encode the image.
        const dataURI = canvas.toDataURL('image/png');

        return dataURI.replace(/^data:image\/(png|jpg);base64,/, '');
    }

    convertDataURIToBlob (dataURI): Blob {
        const binary = atob(dataURI);

        // Write the bytes of the string to a typed array
        const charCodes = Object.keys(binary).map<number>(Number).map<number>(binary.charCodeAt.bind(binary));

        // Build blob with typed array
        return new Blob(
            [
                new Uint8Array(charCodes),
            ],
            { type: 'image/jpeg' },
        );
    }

    log (...msgs) {
        const msg = JSON.stringify(msgs);
        this.alertCtrl.create({ title: msg }).present();
        console.error('\n\n\t' + msg);
    }
}
