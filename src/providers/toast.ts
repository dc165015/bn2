import { Injectable } from '@angular/core';
import { ToastController, Toast } from 'ionic-angular';
import { Log } from 'api/lib/logger';

// 2s 内相同消息算作重复
const Duplicated_Duration = 2000;

@Injectable()
export class ToastProvider {
    handler: Toast;
    previousMsg;
    previousMsgWhen;
    constructor (public toastCtrl: ToastController) {}

    present (msg: string) {
        if (this.checkDuplicatedAndRecord(msg)) return;
        this.dismiss();
        msg = this.translate(msg);
        this.handler = this.toastCtrl.create({
            message: msg,
            position: 'top',
            duration: 5000,
            closeButtonText: '关闭',
            showCloseButton: true,
        });
        this.handler.present();
    }

    dismiss () {
        if (this.handler) {
            this.handler.dismiss();
            this.handler = null;
        }
    }

    presentError (e: Error | Meteor.Error | string) {
        this.dismiss();
        let msg;
        if (e instanceof Meteor.Error) msg = e.details || e.reason || e.error;
        else msg = e['message'] || JSON.stringify(e);
        this.present(msg);
    }

    translate (msg: string) {
        switch (msg.toLowerCase()) {
            case 'not a valid code':
                return '无效验证码';
            case 'not a valid phone':
                return '无效手机号';
            case 'match failed':
                return '上传数据格式错误';
            default:
                return msg;
        }
    }

    checkDuplicatedAndRecord (msg) {
        if (this.checkDuplicated(msg)) return true;
        else this.recordMsg(msg);
    }

    checkDuplicated (msg) {
        if (!msg || (msg == this.previousMsg && Date.now() - this.previousMsgWhen < Duplicated_Duration)) return true;
    }

    recordMsg (msg) {
        this.previousMsg = msg;
        this.previousMsgWhen = Date.now();
        Log.n(msg);
    }
}
