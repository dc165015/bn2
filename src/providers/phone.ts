import { Injectable } from '@angular/core';
import { Accounts } from 'meteor/accounts-base';
import { Platform } from 'ionic-angular';

@Injectable()
export class PhoneProvider {
    constructor(private platform: Platform) {}

    verify(phoneNumber: string) {
        return new Promise(function(resolve, reject) {
            Accounts.requestPhoneVerification(phoneNumber, function(err, result) {
                if (err) reject(err);
                else resolve(result);
            });
        });
    }

    // FIXME: 如果用户当前手机号以前是归属于其他星空用户的，则登录后会取得原用户所有权限。 @v2
    login(phoneNumber: string, code: string) {
        // TODO: 加入密码验证功能 @v2
        return new Promise(function(resolve, reject) {
            Accounts.verifyPhone(phoneNumber, code, function(err, result) {
                if (err) reject(err);
                else resolve(result);
            });
        });
    }
}
