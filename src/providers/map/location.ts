import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Geolocation, Geoposition, GeolocationOptions } from '@ionic-native/geolocation';
import { Location } from 'api/models/location';
import { map, catchError } from 'rxjs/operators';
import { Observable, from, EMPTY } from 'rxjs';
import { Log } from 'api/lib/logger';

const options: GeolocationOptions = { enableHighAccuracy: false, maximumAge: 10 * 60 * 1000, timeout: 10000 };

@Injectable()
export class LocationProvider {
    location: Location;

    constructor (private platform: Platform, private geolocation: Geolocation) {}

    watchPosition$ (): Observable<Location> {
        return this.geolocation.watchPosition(options).pipe(map((position: Geoposition) => this.transform(position)));
    }

    get located$ (): Observable<Location> {
        Log.l('getting Geo location...');
        return from(this.geolocation.getCurrentPosition(options)).pipe(
            map((position) => (this.location = this.transform(position))),
            catchError((err) => {
                Log.e('Get Geolocation ERROR', err);
                return EMPTY;
            }),
        );
    }

    protected transform (position: Geoposition): Location {
        Log.l('got Geolocation: ', position);
        this.location = Location.fromCoords(position.coords, this.platform.height(), this.platform.width());
        return this.location;
    }
}
