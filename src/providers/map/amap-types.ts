import { Location } from 'api/models/location';

export interface MakeMapOptions {
    location: Location;
    mapId: string | HTMLElement;
    inputId: string | HTMLElement;
    markerLabel: string;
}

export interface PoiInfo {
    id: string;
    name: string;
    location: number[];
    address: string;
}

export interface ReGeocode {
    addressComponent: AddressComponent;
    formattedAddress: string;
    pois: ReGeocodePoi[];
}

export interface AddressComponent {
    province: string;
    city: string;
    citycode: string;
    district: string;
    adcode: string;
    township: string;
    street: string;
    streetNumber: string;
    neighborhood: string;
    neighborhoodType: string;
    building: string;
    buildingType: string;
    businessAreas: BusinessArea[];
}

export interface BusinessArea {
    id: string;
    name: string;
    location: string;
}

export interface ReGeocodePoi {
    id: string;
    name: string;
    type: string;
    tel: string;
    distance: number;
    direction: string;
    address: string;
    location: number[];
    businessArea: string;
}
