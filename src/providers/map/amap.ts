import { Injectable } from '@angular/core';
import { Location } from 'api/models/location';
import { LocationProvider } from './location';
import { AMapLoader } from './amap-loader';
import { AMapClass } from './amap-class';

interface MakeMapOptions {
    location: Location;
    mapId: string | HTMLElement;
    inputId: string | HTMLElement;
    markerLabel: string;
}

@Injectable()
export class AMapProvider {

    constructor(public locationProvider: LocationProvider, public amapLoader: AMapLoader) { }

    create() {
        return new AMapClass(this.amapLoader.loaded$, this.locationProvider.located$);
    }

}
