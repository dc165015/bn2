import { Injectable } from "@angular/core";
import { from } from "rxjs";
import { Log } from "api/lib/logger";

declare var initAMapUI;

@Injectable()
export class AMapLoader {

    get loaded$() {
        return from(this.load());
    }

    protected load() {
        return new Promise<void>((resolve: Function, reject: Function) => {
            const start = Date.now();
            const callback = '__onAMapLoad__';

            if (window[callback]) {
                //发现callback时，说明先前已加载，则跳过加载直接监测。
                watch();
            } else {
                const key = '0ba107c0a7a18927f4e1167530ef7e5c';
                const plugins = ['AMap.Geocoder'];
                const apiScript = this.insertScript(`https://webapi.amap.com/maps?v=1.4.9&key=${key}&callback=${callback}`);
                const uiScript = this.insertScript('https://webapi.amap.com/ui/1.0/main-async.js');
                window[callback] = watch;
                Log.l('loading AMap api...');
                apiScript.onerror = (error: Event) => { reject(error); };
                uiScript.onerror = (error: Event) => { reject(error); };
            }

            function watch() {
                const duration = Date.now() - start;
                if (window['initAMapUI'] && window['AMap']) {
                    initAMapUI();
                    Log.l(`loading AMap api and ui COMPLETE in ${duration}ms`);
                    return resolve();
                }
                return duration < 10000 ? setTimeout(watch, 20) : reject('网络异常,地图服务加载超时');
            }
        });
    }

    protected insertScript(src) {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.defer = true;
        script.src = src;
        document.body.appendChild(script);
        return script;
    }
}
