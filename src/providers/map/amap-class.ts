import { Location } from 'api/models/location';
import { switchMapTo, tap, take } from 'rxjs/operators';
import { Observable, Observer, from, zip, merge } from 'rxjs';
import { MakeMapOptions, ReGeocode } from './amap-types';
import { Log } from 'api/lib/logger';

declare const AMap, AMapUI;

export class AMapClass {
    map;
    marker;
    location: Location;
    mapId: string | HTMLElement = 'AMapContainer';
    inputId: string | HTMLElement = 'poiInput';
    markerLabel: string = '群址';

    constructor (public loaded$: Observable<void>, public located$: Observable<Location>) {}

    present ({ location, mapId, inputId, markerLabel }: MakeMapOptions) {
        const getOperator:any = () => {
            return location
                ? take(1)
                : // 未提供location时，默认绘制PoiPicker 和 PositionPicker
                  switchMapTo(merge(this.drawPoiPicker(inputId), this.drawPositionPicker()));
        };

        return this.mark$(location, mapId, markerLabel).pipe(getOperator());
    }

    getLocation () {
        if (this.map) this.location.zoom = this.map.getZoom();
        return this.location;
    }

    protected getLocated$ (location?: Location) {
        const task$ = location ? from(Promise.resolve(location)) : this.located$;
        return task$.pipe(tap((location) => (this.location = location)));
    }

    //初始化地图
    protected drawMap (location: Location, mapId: string | HTMLElement) {
        Log.n('draw map on:', location);
        const center = location ? location.lnglat : {};
        this.map = new AMap.Map(mapId || this.mapId, {
            resizeEnable: true,
            zoom: (location && location.zoom) || 14,
            ...center,
        });
        this.map.plugin('AMap.ToolBar', () => this.map.addControl(new AMap.ToolBar()));
    }

    protected map$ (location: Location, mapId: string | HTMLElement) {
        const task$ = new Observable((observer: Observer<void>) => {
            this.drawMap(location, mapId);
            this.map.on('complete', () => observer.next(void 0));
        });

        return this.loaded$.pipe(switchMapTo(task$));
    }

    protected drawMarker (location: Location, markerLabel: string = '') {
        if (!this.marker) {
            const position = location ? { position: location.lnglat } : {};
            this.marker = new AMap.Marker({
                map: this.map,
                iconLabel: markerLabel || this.markerLabel,
                ...position, // AMap BUG: 使用this.marker.setPosition() 将报错
                // draggable: true,
            });
        }

        if (location) this.map.setCenter(location.lnglat);
    }

    protected mark$ (location: Location, mapId: string | HTMLElement, markerLabel: string) {
        return zip(this.getLocated$(location), this.map$(location, mapId)).pipe(
            tap(([ location, _void
            ]) => {
                this.drawMarker(location, markerLabel);
            }),
        );
    }

    protected drawPoiPicker (inputId: string | HTMLElement) {
        return new Observable<void>((observer: Observer<void>) => {
            AMapUI.loadUI(
                [
                    'misc/PoiPicker',
                ],
                (PoiPicker) => {
                    var poiPicker = this.makePicker(PoiPicker, inputId);
                    // window.poiPicker = poiPicker;
                    // poiPicker.onCityReady(() => poiPicker.suggest('小学'));
                    this.listenOnPick(poiPicker, observer);
                },
            );
        });
    }

    protected makePicker (PoiPicker, inputId) {
        return new PoiPicker({
            input: inputId || this.inputId,
            placeSearchOptions: {
                map: this.map,
                pageSize: 5,
                autoFitView: true,
                extensions: 'all',
            },
        });
    }

    protected listenOnPick (poiPicker, observer: Observer<void>) {
        //选取了某个POI
        poiPicker.on('poiPicked', (poiResult) => {
            const poi = poiResult.item;
            Log.l('gained poi: ', poiResult);
            this.location = new Location(poi.location.getLat(), poi.location.getLng(), this.map.getZoom());
            this.drawMarker(this.location, poi.name);
            this.map.setCenter(this.location.lnglat);
            this.reGeoCode(this.location).then(() => observer.next(void 0), (err) => observer.error(err));
        });
    }

    protected drawPositionPicker (location?: Location) {
        return new Observable((observer: Observer<void>) => {
            AMapUI.loadUI(
                [
                    'misc/PositionPicker',
                ],
                (PositionPicker) => {
                    const positionPicker = new PositionPicker({ map: this.map });
                    positionPicker.on('success', (positionResult) => {
                        Log.l('get position after drag:', positionResult);
                        this.location.lng = positionResult.position.getLng();
                        this.location.lat = positionResult.position.getLat();
                        this.location.zoom = this.map.getZoom();
                        this.addRegeocodeToLocation(positionResult.regeocode);
                        observer.next(void 0);
                    });
                    positionPicker.on('fail', () => observer.error('无法取到有效地址'));
                    location = location || this.location;
                    positionPicker.start(location && location.lnglat);
                },
            );
        });
    }

    protected reGeoCode (location: Location) {
        return new Promise<void>((resolve, reject) => {
            if (!location) return resolve();
            AMap.plugin('AMap.Geocoder', () => {
                const geoCoder = new AMap.Geocoder({ radius: 500 });
                geoCoder.getAddress(location.lnglat, (status: string, result) => {
                    if (status == 'no_data') reject('未查到该地址信息');
                    else if (status == 'error') reject(result);
                    else {
                        this.addRegeocodeToLocation(result.regeocode);
                        Log.l('resolved reGeocode location:', this.location);
                        resolve();
                    }
                });
            });
        });
    }

    protected addRegeocodeToLocation (code: ReGeocode) {
        const address: any = code.addressComponent;
        delete address.businessAreas;
        Object.assign(this.location, address);
        this.location.name = code.formattedAddress;
    }

    // 高德提供的JS API的定位功能
    protected autoLocate () {
        return new Observable((observer: Observer<void>) => {
            this.map.plugin('AMap.Geolocation', () => {
                const geolocation = new AMap.Geolocation({
                    enableHighAccuracy: false, //是否使用高精度定位，默认:true
                    timeout: 10000, //超过10秒后停止定位，默认：无穷大
                    zoomToAccuracy: true, //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
                });
                this.map.addControl(geolocation);
                geolocation.getCurrentPosition((result) => {
                    Log.l('got AMap location:', result);
                    this.location.lat = result.getLat();
                    this.location.lng = result.getLng();
                    this.location.zoom = this.map.getZoom();
                    this.location.accuracy = result.accuracy;
                });
                AMap.event.addListener(geolocation, 'complete', () => observer.next(void 0));
                AMap.event.addListener(geolocation, 'error', (err) => observer.error(err));
            });
        });
    }
}
