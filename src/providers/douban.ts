import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { range, timer, Observable } from 'rxjs';
import { filter, map, mergeMap, retryWhen, zip, timeout } from 'rxjs/operators';
import { Book, BookSource } from 'api/models/book';
import { Log } from 'api/lib/logger';

@Injectable()
export class DoubanProvider {
    constructor (public http: HttpClient) {}

    getBookFromDouban (isbn: string): Observable<Book> {
        return this.http.jsonp(`https://api.douban.com/v2/book/isbn/${isbn}`, 'callback').pipe(
            timeout(3500),
            backOff(isbn),
            map((data: Book) => {
                data.source = BookSource.DOUBAN;
                let doc = Book.fixAll(data);
                return Book.wrapDoc(doc);
            }),
        );
    }
}

// if error occurred, retry after delay.
function backOff (isbn, maxTries: number = 1, delay: number = 250){
    return retryWhen((errors) => {
        return range(1, maxTries).pipe(
            zip(errors),
            filter((i) => {
                const err = i[1];
                Log.e(err);
                const status = err.status;
                if (status == 404) throw new Meteor.Error(404, '未找到此书的数据', isbn);
                if (status == 400) throw new Meteor.Error(400, '请求过于频繁，请稍候再试', isbn);
                if (status == 0) throw new Meteor.Error(0, '未知的远程错误', isbn);
                return status != 404 && status != 400;
            }),
            map((i) => i[0] ** 2),
            mergeMap((i) => timer(i * delay)),
        );
    });
}
