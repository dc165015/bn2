import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { ErrorHandler, Injectable, Injector, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicStorageModule } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BookHeart } from './app.component';
import { MomentModule } from 'angular2-moment';
import { PhoneProvider } from 'providers/phone';
import { PictureProvider } from 'providers/picture';
import { ToastProvider } from 'providers/toast';
import { PreloadImage } from 'components/preload-image/preload-image';
import { UserProfilePage } from 'pages/user-profile/user-profile';
import { RatingComponent } from 'components/rating/rating';
import { HideHeaderDirective } from 'directives/hide-header';
import { SettingsPage } from 'pages/settings/settings';
import { TabsPage } from 'pages/tabs/tabs';
import { ChatsPage } from 'pages/chats/chats';
import { MessagesPage } from 'pages/messages/messages';
import { BookListSlidesPage } from 'pages/book-list-slides/book-list-slides';
import { LocationMessageComponent } from 'components/location-message/location-message';
import { MessagesAttachmentsComponent } from 'components/messages-attachments/messages-attachments';
import { BookProfilePage } from 'pages/book-profile/book-profile';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DoubanProvider } from 'providers/douban';
import { CopyProvider } from 'providers/copies';
import { MyProfileEditPage } from 'pages/my-profile-edit/my-profile-edit';
import { TermsOfServicePage } from 'pages/terms-of-service/terms-of-service';
import { CommunityListSlidesPage } from 'pages/community-list-slides/community-list-slides';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { GetPhonePage } from 'pages/login/get-phone/get-phone';
import { GetCodePage } from 'pages/login/get-code/get-code';
import { SearchPage } from 'pages/search/search';
import { OrderListPage } from 'pages/order-list/order-list';
import { BlockComponent } from 'components/block/block';
import { BookBlockComponent } from 'components/block/book-block';
import { CopyBlockComponent } from 'components/block/copy-block';
import { UserBlockComponent } from 'components/block/user-block';
import { CopyListPage } from 'pages/copy-list/copy-list';
import { OrderCardComponent } from 'components/order-card/order-card';
import { CopyListSectionComponent } from 'components/copy-list-section/copy-list-section';
import { OrderListSectionComponent } from 'components/order-list-section/order-list-section';
import { NewChatPage } from 'pages/new-chat/new-chat';
import { AMapComponent } from 'components/a-map/a-map';
import { AMapProvider } from 'providers/map/amap';
import { LocationProvider } from 'providers/map/location';
import { AMapLoader } from 'providers/map/amap-loader';
import { QrCodeImageComponent } from 'pages/community-profile/qrcodeimage.component';
import { SelectCommunityComponent } from 'pages/my-profile-edit/select-community.component';
import { CodePush } from '@ionic-native/code-push';
import { WalkthroughPage } from 'pages/walkthrough/walkthrough';
import { Badge } from '@ionic-native/badge';
import { CommunityProfilePageModule } from 'pages/community-profile/community-profile.module';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditBookPage } from 'pages/edit-book/edit-book';
import { BrowserTab } from '@ionic-native/browser-tab';
import { FullTextAreaComponent } from 'components/full-text-area/full-text-area';

import { Pro } from '@ionic/pro';

Pro.init('efe9e0df', {
    appVersion: '0.1.7',
});

@Injectable()
export class MyErrorHandler implements ErrorHandler {
    ionicErrorHandler: IonicErrorHandler;

    constructor (injector: Injector) {
        try {
            this.ionicErrorHandler = injector.get(IonicErrorHandler);
        } catch (e) {
            // Unable to get the IonicErrorHandler provider, ensure
            // IonicErrorHandler has been added to the providers list below
        }
    }

    handleError (err: any): void {
        Pro.monitoring.handleNewError(err);
        // Remove this if you want to disable Ionic's auto exception handling
        // in development mode.
        this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
    }
}

@NgModule({
    declarations: [
        BookHeart,

        // pages
        BookListSlidesPage,
        CommunityListSlidesPage,
        UserProfilePage,
        SettingsPage,
        MyProfileEditPage,
        TabsPage,
        ChatsPage,
        MessagesPage,
        BookProfilePage,
        OrderListPage,
        TermsOfServicePage,
        SearchPage,
        GetPhonePage,
        GetCodePage,
        CopyListPage,
        NewChatPage,
        WalkthroughPage,
        EditBookPage,

        // Components
        BlockComponent,
        BookBlockComponent,
        CopyBlockComponent,
        UserBlockComponent,
        PreloadImage,
        RatingComponent,
        LocationMessageComponent,
        MessagesAttachmentsComponent,
        OrderCardComponent,
        CopyListSectionComponent,
        OrderListSectionComponent,
        AMapComponent,
        QrCodeImageComponent,
        SelectCommunityComponent,
        FullTextAreaComponent,

        // Directives
        HideHeaderDirective,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        HttpClientJsonpModule,
        MomentModule,
        TagInputModule,
        IonicImageViewerModule,
        CommunityProfilePageModule,
        BrowserAnimationsModule,
        IonicStorageModule.forRoot(),
        IonicModule.forRoot(BookHeart, {
            backButtonText: '',
            iconMode: 'ios',
            tabsPlacement: 'bottom',
            pageTransition: 'ios-transition',
            tabsHideOnSubPages: true,
            mode: 'ios',
            preload: true,
        }),
    ],
    bootstrap: [
        IonicApp,
    ],
    entryComponents: [
        BookHeart,

        // pages
        BookListSlidesPage,
        CommunityListSlidesPage,
        UserProfilePage,
        SettingsPage,
        TabsPage,
        ChatsPage,
        MessagesPage,
        BookProfilePage,
        OrderListPage,
        MyProfileEditPage,
        TermsOfServicePage,
        SearchPage,
        GetPhonePage,
        GetCodePage,
        CopyListPage,
        NewChatPage,
        WalkthroughPage,
        EditBookPage,

        // Components
        BlockComponent,
        BookBlockComponent,
        CopyBlockComponent,
        UserBlockComponent,
        PreloadImage,
        RatingComponent,
        LocationMessageComponent,
        MessagesAttachmentsComponent,
        OrderCardComponent,
        CopyListSectionComponent,
        OrderListSectionComponent,
        AMapComponent,
        QrCodeImageComponent,
        SelectCommunityComponent,
        FullTextAreaComponent,
    ],
    providers: [
        Camera,
        SplashScreen,
        StatusBar,
        CodePush,
        PhoneProvider,
        Geolocation,
        LocationProvider,
        AMapLoader,
        AMapProvider,
        PictureProvider,
        Camera,
        SocialSharing,
        Crop,
        Badge,
        ToastProvider,
        BarcodeScanner,
        DoubanProvider,
        CopyProvider,
        BrowserTab,
        // Keep this to enable Ionic's runtime error handling during development
        IonicErrorHandler,
        [
            { provide: ErrorHandler, useClass: MyErrorHandler },
        ],
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
    ],
})
export class AppModule {}
