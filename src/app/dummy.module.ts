// this file is only used to escape "ionic cordova build --prod" errors: "Cannot determine the module for class X in X.ts! Add X to the NgModule to fix it error", ref: https://github.com/angular/angular/issues/13590#issuecomment-401873950

import { NgModule } from '@angular/core';
import { OverlayPortal } from 'ionic-angular/components/app/overlay-portal';
import { IonicApp } from 'ionic-angular/components/app/app-root';
import { ClickBlock } from 'ionic-angular/components/app/click-block';
import { EnhancedBase, SubscriptionBase } from '../lib/enhanced-base';

const basicComponents = [ EnhancedBase, SubscriptionBase ];
const environmentComponents = Meteor.isProduction ? [] : [ OverlayPortal, IonicApp, ClickBlock ];

@NgModule({
    declarations: [ ...environmentComponents, ...basicComponents ],
})
export class DummyModule {}
