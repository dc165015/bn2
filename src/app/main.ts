import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import './meteor-client.js';
import { Meteor } from 'meteor/meteor';
import { AppModule } from './app.module';
import { reportDocNumbers } from 'api/collections/init-collections';
import '../lib/meteor-rxjs-shortcuts';
import { User } from 'api/models';
import { Log } from 'api/lib/logger';
import { merge } from 'rxjs';

enableProdMode();

Meteor.startup(() => {
    subscribeLoginUserData();
    bootApp();
});

function subscribeLoginUserData (){
    let subscription;

    Accounts.onLogin(() => {
        subscription = merge(
            Meteor.subscribe$('loginUserInfo'),
            Meteor.subscribe$('loginUserMessages'),
            Meteor.subscribe$('loginUserCommunities'),
        ).subscribe(() => Log.w('Auto subscribed loginUserData since startup'), (err) => Log.e(err));
        hookDebugger();
    });

    Accounts.onLogout(() => {
        if (subscription) subscription.unsubscribe();
    });
}

function hookDebugger (){
    window['report'] = reportDocNumbers;
    window['me'] = User.Me;
}

function bootApp (){
    const subscription = Meteor.autorun$(() => {
        if (Meteor.loggingIn()) return;
        setTimeout(() => subscription.unsubscribe());
        platformBrowserDynamic().bootstrapModule(AppModule);
    });
}
