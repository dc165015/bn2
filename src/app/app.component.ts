import { Component, ViewChild, OnInit } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { Meteor } from 'meteor/meteor';
import { MainPage } from '../pages';
import { ToastProvider } from 'providers/toast';
import { Observable } from 'rxjs';
import { GetPhonePage } from 'pages/login/get-phone/get-phone';
import { User } from 'api/models';
import { MyProfileEditPage } from 'pages/my-profile-edit/my-profile-edit';
import { CodePush, InstallMode, SyncStatus } from '@ionic-native/code-push';
import { Log } from 'api/lib/logger';
import { CommunityProfilePage } from 'pages/community-profile/community-profile';
import { Deeplinks } from '@ionic-native/deeplinks';
import { Picture } from 'api/models/picture';

@Component({
    templateUrl: './app.html',
})
export class BookHeart implements OnInit {
    rootPage;
    online$: Observable<any>;
    @ViewChild(Nav) nav: Nav;

    pages: any[] = [
        { title: 'Login', component: GetPhonePage },
    ];

    constructor (
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen,
        public codePush: CodePush,
        public toast: ToastProvider,
        public alertCtrl: AlertController,
    ) {
        platform.ready().then(() => {
            this.setupPicturePrefix();

            if (platform.is('cordova')) {
                this.splashScreen.show();
                this.statusBar.styleLightContent();
                this.setupDeepLinks();
                this.checkCodePush();
                this.splashScreen.hide();
            }
        });

        this.monitorMeteorConnection();
    }

    ngOnInit () {
        if (Meteor.userId()) {
            this.rootPage = MainPage;
            if (!User.Me.isProfiled)
                setTimeout(() => {
                    this.nav.setPages([
                        { page: MainPage },
                        { page: MyProfileEditPage },
                    ]);
                }, 5000);
        }
        else this.rootPage = MainPage;
    }

    openPage (page) {
        this.nav.setRoot(page.component);
    }

    setupPicturePrefix () {
        if (this.platform.is('android')) Picture.platformPrefix = '/android_asset/www';
    }

    // ref: https://ionicframework.com/docs/native/deeplinks/
    setupDeepLinks () {
        if (!this.platform.is('cordova')) return;
        new Deeplinks()
            .routeWithNavController(
                this.nav,
                {
                    '/communities/join/:communityId/:invitorId/:createdAt': CommunityProfilePage,
                },
                { root: false },
            )
            .subscribe(
                (match) => {
                    Log.w('成功找到匹配路由', match);
                },
                (nomatch) => {
                    Log.e('路由匹配失败', nomatch);
                },
            );
    }

    checkCodePush () {
        this.reportUpdateStatus();

        const options = this.platform.is('ios')
            ? {}
            : {
                  updateDialog: {
                      appendReleaseDescription: true,
                      descriptionPrefix: '\n\n更新概要:\n',
                      mandatoryUpdateMessage: 'App有新版本，将马上进行更新。',
                      mandatoryContinueButtonLabel: '更新',
                      optionalUpdateMessage: 'App有新版本，需要更新吗?',
                      optionalIgnoreButtonLabel: '取消',
                      optionalInstallButtonLabel: '好的',
                      updateTitle: '新版本',
                  },
                  installMode: InstallMode.IMMEDIATE,
              };

        this.codePush.sync({ ...options }).subscribe(
            (syncStatus) => {
                switch (syncStatus) {
                    case SyncStatus.UP_TO_DATE:
                        this.toast.present('已经是最新版本了');
                        break;
                    case SyncStatus.UPDATE_INSTALLED:
                        this.toast.present('更新完成');
                        break;
                    case SyncStatus.UPDATE_IGNORED:
                        this.toast.present('取消更新');
                        break;
                    case SyncStatus.ERROR:
                        this.toast.present('An unknown error occurred.');
                        break;
                    case SyncStatus.IN_PROGRESS:
                        this.toast.present(' Another sync is already running, so this attempt aborted.');
                        break;
                    case SyncStatus.CHECKING_FOR_UPDATE:
                        this.toast.present('检查更新');
                        break;
                    case SyncStatus.AWAITING_USER_ACTION:
                        this.toast.present('等待用户协作');
                        break;
                    case SyncStatus.DOWNLOADING_PACKAGE:
                        this.toast.present('下载安装包');
                        break;
                    case SyncStatus.INSTALLING_UPDATE:
                        this.toast.present('正在安装更新');
                        break;
                }

                Log.n('CODE PUSH SUCCESSFUL: ' + syncStatus);
            },
            (err) => Log.e('CODE PUSH ERROR: ' + err),
        );
    }

    reportUpdateStatus () {
        this.codePush
            .getCurrentPackage()
            .then((localPackage) => localPackage && Log.n('current local package', localPackage.appVersion));
        this.codePush
            .checkForUpdate()
            .then(
                (remotePackage) =>
                    remotePackage && Log.n('remote package', remotePackage.appVersion, remotePackage.downloadUrl),
            );
        this.codePush
            .getPendingPackage()
            .then((pendingPackage) => pendingPackage && Log.n('pending package', pendingPackage.appVersion));
    }

    monitorMeteorConnection () {
        if (!Meteor.status().connected) {
            if (!navigator.onLine) {
                this.toast.present(`网络断了，请检查...`);
            }
            else {
                const status = Meteor.status();
                const count = status.retryCount;
                if (status.status == 'connecting' && count >= 1) {
                    this.toast.present(`正在尝试第${count}次连接服务器......`);
                }
            }
        }
        else {
            this.toast.dismiss();
        }
    }
}
