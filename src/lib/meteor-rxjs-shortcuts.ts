import { Meteor } from 'meteor/meteor';
import { MeteorObservable } from 'meteor-rxjs';
import { Subscription, Observable } from 'rxjs';
import { Log } from 'api/lib/logger';

Meteor.call$ = function<T> (name: string, ...args: any[]): Observable<T>{
    Log.p('CALLING', arguments);
    return MeteorObservable.call.apply(MeteorObservable, arguments) as Observable<T>;
};

Meteor.subscribe$ = function (){
    Log.p('SUBSCRIBING', arguments);
    return MeteorObservable.subscribe.apply(MeteorObservable, arguments);
};

Meteor.autorun$ = function (next?: (value: Tracker.Computation) => void, error?: (err) => void, complete?: () => void){
    Log.p('AUTORUNING');
    error = error || ((err) => Log.e(err));
    complete = complete || (() => Log.i('autorun completed'));
    return MeteorObservable.autorun().subscribe(next, error, complete) as Subscription;
};
