import { InfiniteScroll } from 'ionic-angular';
import { debounce } from 'lodash';

const complete = InfiniteScroll.prototype.complete;
InfiniteScroll.prototype.complete = debounce(function () {
    complete.call(this);

    // because ion-infinite-scroll comp won't disappear after loading while working with slides, we mannually hide it.
    const ele: HTMLElement = this._elementRef.nativeElement;
    if (ele.className == 'scroll-slides') {
        this._dom.write(() => {
            this._content.scrollTop = 0;
        });
    };
}, 500); // avoid frequent loading caused by scroll event
