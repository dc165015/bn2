import { OnDestroy, OnInit, ChangeDetectorRef, Component, ChangeDetectionStrategy } from '@angular/core';
import { Subscription } from 'rxjs';
import { InfiniteScroll, Refresher, Loading, LoadingController } from 'ionic-angular';
import './infinite-scroll-patch';
import { ToastProvider } from 'providers/toast';
import { Meteor } from 'meteor/meteor';
import { getLastCallbackArg } from './util';
import { reportDocNumbers } from 'api/collections/init-collections';
import { debounce } from 'lodash';
import { Log } from 'api/lib/logger';

@Component({
    template: '',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscriptionBase implements OnDestroy {
    protected readonly subscriptions: Map<string, Subscription> = new Map();
    protected readonly computations: Map<number, Tracker.Computation> = new Map();
    protected readonly meteorSubscribers: Map<string, Meteor.SubscriptionHandle> = new Map();

    protected _autorunner;

    constructor (public cdf: ChangeDetectorRef, public toast: ToastProvider) {
        window[this.constructor.name] = this;
        Log.p('>>> constructing ', this.constructor.name);
    }

    protected detectChanges (afterChangesAction?: Function, delay: number = 0) {
        setTimeout(() => {
            Log.l('check changes of', this.constructor.name);
            if (!this.cdf['destroyed']) {
                this.cdf.detectChanges();
                if (afterChangesAction) afterChangesAction.call(this);
            }
        }, 0);
    }

    protected autorun (runner?: (x: Tracker.Computation) => void, name?) {
        if (!runner) return this._autorunner && this._autorunner.call(this);

        name = name || this.constructor.name;
        const tailorRunner = (x: Tracker.Computation) => {
            runner.call(this, x);
            this.detectChanges();
        };
        this._autorunner = debounce(tailorRunner, 50, { leading: true, maxWait: 1000 });
        const computation = Tracker.autorun((computation) => {
            this._autorunner(computation);
        });
        computation.onStop((x) => {
            Log.w('!!! stop computation', x.name, x._id);
        });
        computation['name'] = name;
        this.addComputation(computation);
    }

    protected addComputation (computation: Tracker.Computation) {
        Log.l('record computation', computation['name']);
        this.computations.set(computation['_id'], computation);
    }

    protected addSubscription (subscription: Subscription, ...args) {
        const id = JSON.stringify(args || subscription);
        const existed = this.subscriptions.get(id);
        if (existed) {
            Log.l('stop subscription firstly before record: ', id);
            existed.unsubscribe();
            this.subscriptions.delete(id);
        }
        Log.l('record subscription:', id);
        this.subscriptions.set(id, subscription);
    }

    protected addMeteorSubscriber (key, handler) {
        let previous: Meteor.SubscriptionHandle = this.meteorSubscribers.get(key);
        if (previous) {
            previous.stop();
            this.meteorSubscribers.delete(key);
            Log.l('manually stopped previous meteor subscription:', previous.subscriptionId);
        }
        Log.l('add meteor.subscription:', handler.subscriptionId);
        this.meteorSubscribers.set(key, handler);
        return handler;
    }

    // release memory
    ngOnDestroy () {
        this.cdf.detach();
        this.subscriptions.forEach((subscription) => subscription.unsubscribe());
        this.subscriptions.clear();

        this.computations.forEach((computation) => computation.stop());
        this.computations.clear();

        // 为了保持数据，不予注销
        // this.meteorSubscribers.forEach(subscriber => subscriber.stop());
        // this.meteorSubscribers.clear();
    }
}

export class EnhancedBase extends SubscriptionBase implements OnInit {
    constructor (public toast: ToastProvider, public cdf: ChangeDetectorRef, public loadingCtrl: LoadingController) {
        super(cdf, toast);
    }

    loading: Loading;
    refresher: Refresher;
    infiniteScroll: InfiniteScroll;
    protected _isInitialized = false;
    protected readonly subscriptionTimer: Map<string, number> = new Map();

    ngOnInit () {}

    protected newLoading (options?) {
        if (this.loading) return;

        this.loading = this.loadingCtrl.create({
            dismissOnPageChange: true,
            enableBackdropDismiss: true,
            ...options,
        });
        this.loading.present();
    }

    protected dismissLoading () {
        if (!this.loading) return;
        this.loading.dismiss().catch((e) => {});
        this.loading = undefined;
    }

    protected completeInfiniteScroll () {
        if (this.infiniteScroll) this.infiniteScroll.complete();
    }

    protected completeRefresher () {
        if (this.refresher) this.refresher.complete();
    }

    protected completeFetch$ () {
        this.dismissLoading();
        this.completeInfiniteScroll();
        this.completeRefresher();
    }

    protected initLoad () {
        if (this._isInitialized) return;
        Log.l('initial loading...');
        this.newLoading();
        this.loadMore();
        this._isInitialized = true;
    }

    protected call$<T> (name: string, ...args: any[]) {
        return Meteor.call$<T>(name, ...args);
    }

    protected fetch$ (name: string, batchCounter: number = 1, ...args: any[]) {
        let key = JSON.stringify([
            name,
            ...args,
        ]);
        let timer = this.subscriptionTimer.get(key) || 0;
        if (Date.now() - timer < 1000) {
            this.completeFetch$();
            return; // 忽略间隔1s内的请求
        }

        this.subscriptionTimer.set(key, Date.now());

        this.fetchNow$(name, batchCounter, ...args);
    }

    protected fetchNow$ (name: string, batchCounter: number = 1, ...args: any[]) {
        let key = JSON.stringify([
            name,
            ...args,
        ]);
        const callback = getLastCallbackArg(args, true);

        Log.p('remotely fetching', name, batchCounter, ...args);
        const handler = Meteor.subscribe(name, batchCounter, ...args, {
            onStop: (err) => this._onFetch$Stop(err, handler),
            onReady: () => this._onFetch$Ready(callback, handler),
        });
        this.addMeteorSubscriber(key, handler);
    }

    protected _onFetch$Stop (err, handler) {
        this.completeFetch$();
        if (err) this.showError(err);
        Log.n(name, 'subscribe stopped:', handler.subscriptionId);
    }

    protected _onFetch$Ready (callback, handler) {
        const { refresher, infiniteScroll } = this;
        Log.l('meteor.subscribe is ready:', handler.subscriptionId);
        reportDocNumbers();
        this.completeFetch$();
        if (callback) callback.call(this);
        this.detectChanges();
    }

    doRefresh (refresher?) {
        if (refresher) this.refresher = refresher;
        this.ngOnInit();
    }

    loadMore (infiniteScroll?) {
        if (infiniteScroll) this.infiniteScroll = infiniteScroll;
        else this.newLoading();
    }

    protected showError (error) {
        this.dismissLoading();
        this.toast.presentError(error);
    }

    trackById (index, item) {
        return item._id;
    }
}
