import { Log } from "api/lib/logger";

// a class decorator, prevents malicious data injection, intends to use only in constructor
export function sealed<T extends { new(...args: any[]): {} }>(originConstructor: T) {
    return class extends originConstructor {
        constructor(...args) {
            super(...args);
            Object.seal(this);
            if (args.length === 1) {
                try {
                    Object.assign(this, ...args);
                } catch (e) {
                    Log.w(`Malice: extend ${originConstructor.name} is forbidden`, e, args);
                }
            }
        }
    };
}

// Depreciated: use Object.keys(str).length insteadly
export function getMixedStringLength(str) {
    let arr = str.match(/[^ -~]/g);
    // 常用英文字符集外的字符长度统统算作2个字符
    return arr ? str.length + arr.length : str.length;
}

export function getLastCallbackArg(args: any[], isSpliceArgs: boolean = false) {
    let callback: Function;
    const lastIndex = args.length - 1;
    if (args[lastIndex] instanceof Function) {
        callback = isSpliceArgs ? args.splice(lastIndex, 1)[0] : args[lastIndex];
    }
    return callback;
}
