import { Component, Input, ChangeDetectorRef, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, ActionSheetController } from 'ionic-angular';
import { EMPTY, of, Subscription } from 'rxjs';
import { Book } from 'api/models/book';
import { EnhancedBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { User, Copy } from 'api/models';
import { switchMap, mapTo } from 'rxjs/operators';
import { Books, Copies } from 'api/collections';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { CopyProvider } from 'providers/copies';
import { BookProfilePage } from 'pages/book-profile/book-profile';
import { SearchPage } from 'pages/search/search';
import { EditBookPage } from 'pages/edit-book/edit-book';

@Component({
    selector: 'copy-list-section',
    templateUrl: 'copy-list-section.html',
})
export class CopyListSectionComponent extends EnhancedBase implements OnInit {
    @Input() userId: ID;
    user: User;
    copies: Copy[];
    batchCounter: number = 1;

    loading: Loading;

    constructor (
        public navCtrl: NavController,
        public navParams: NavParams,
        public actionSheetCtrl: ActionSheetController,
        public barcodeScanner: BarcodeScanner,
        public copyStore: CopyProvider,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit () {
        this.userId = this.userId || Meteor.userId();
        this.autorun(() => {
            this.user = User.get(this.userId);
            this.copies = this.user.copies.fetch();
            this.filterCopies();
            this.sortCopies();
            this.extendCopies();
        });
        this.initLoad();
    }

    initLoad () {
        if (this.copies.length <= 1) super.initLoad();
    }

    filterCopies () {
        return (this.copies = this.copies.filter((copy) => !copy.isLost || User.isMe(this.userId)));
    }

    sortCopies () {
        return this.copies.sort((a, b) => b.updatedAt - a.updatedAt);
    }

    // add Book.scanner into this.books list
    extendCopies () {
        if (!this.user.isMe || this.copies.find((copy) => copy.bookId == Book.Scanner._id)) return;

        const length = this.copies.length;
        let position = length >= 4 ? 4 : length >= 1 ? 1 : 0;

        this.copies.splice(position, 0, Book.Scanner as any);
    }

    scan () {
        //9787020103331, 9787559417817, 9787570201174, 9787548437918, 9787020042494
        //9787559806093, 9787544292870,9787115482679,9787532767373,9787020116133
        //9787559418111,9787544138000,9787801656087,9787301292686,9780062367853,
        const subscription: Subscription = of(this.newLoading())
            .pipe(
                mapTo('9787115482679'),
                // switchMap(() => this.barcodeScanner.scan()),
                // map((code) => code.text),
                switchMap((isbn) => {
                    if (this.checkExistedAfterScan(isbn)) return EMPTY;
                    // this.loading.present();
                    return this.copyStore.addCopy$(isbn);
                }),
            )
            .subscribe(
                (bookId) => {
                    this.cdf.detectChanges();
                    this.dismissLoading();
                    this.showRepeatScanDialog(bookId);
                },
                (err) => {
                    this.dismissLoading();
                    this.manualInput(err);
                },
            );

        this.addSubscription(subscription, 'scan');
    }

    checkExistedAfterScan (isbn: string) {
        const book = Books.findOne({
            $or: [
                { isbn10: isbn },
                { isbn13: isbn },
            ],
        });
        if (!book) return;

        const found = Copies.findOne({ ownerId: Meteor.userId(), bookId: book._id });
        if (found) this.showScanExistedDialog(book);

        return found;
    }

    showScanExistedDialog (book) {
        this.actionSheetCtrl
            .create({
                title: '您已扫描过此书',
                buttons: [
                    {
                        text: '继续扫描',
                        role: 'destructive',
                        handler: () => this.scan(),
                    },
                    {
                        text: '查看',
                        handler: () =>
                            this.navCtrl.push(BookProfilePage, { bookId: book._id }).catch((e) => this.showError(e)),
                    },
                    {
                        text: '编辑',
                        handler: () => {
                            this.navCtrl.push(EditBookPage, { bookId: book._id });
                        },
                    },
                    {
                        text: '取消',
                        role: 'cancel',
                    },
                ],
            })
            .present();
    }

    showRepeatScanDialog (bookId: ID) {
        this.actionSheetCtrl
            .create({
                title: '上传完成，要继续扫描吗？',
                buttons: [
                    {
                        text: '好的',
                        role: 'destructive',
                        handler: () => this.scan(),
                    },
                    {
                        text: '编辑',
                        handler: () => {
                            this.navCtrl.push(EditBookPage, { bookId });
                        },
                    },
                    {
                        text: '拍照',
                        handler: () =>
                            this.navCtrl
                                .push(BookProfilePage, { bookId, action: 'shot' })
                                .catch((e) => this.showError(e)),
                    },
                    {
                        text: '取消',
                        role: 'cancel',
                    },
                ],
            })
            .present();
    }

    manualInput (err) {
        if (err instanceof Meteor.Error && (err.error == 404 || err.error == 0)) {
            this.showManualInputDialog(err.details);
        }
        else {
            this.showError(err);
        }
    }

    showManualInputDialog (isbn: string) {
        this.actionSheetCtrl
            .create({
                title: '没有此书的数据，需要手工录入吗？(耗时大约2分钟)',
                buttons: [
                    {
                        text: '放弃，扫描另一本',
                        role: 'destructive',
                        handler: () => {
                            this.scan();
                        },
                    },
                    {
                        text: '好的，手工录入',
                        handler: () => {
                            this.navCtrl.push(EditBookPage, { isbn });
                        },
                    },
                    {
                        text: '关闭',
                        role: 'cancel',
                    },
                ],
            })
            .present();
    }

    onClick (event, copy) {
        if (copy._id == Book.Scanner._id) {
            return this.scan();
        }
        this.navCtrl.push(BookProfilePage, { bookId: copy.bookId });
    }

    subscribeCopiesList () {
        this.fetch$('userCopies', this.batchCounter++, this.user._id, () => {
            if (Copies.find({ ownerId: this.userId }).count() >= this.user.profile.copyCount) {
                this.infiniteScroll && this.infiniteScroll.enable(false);
            }
        });
    }

    loadMore (infiniteScroll?) {
        super.loadMore(infiniteScroll);
        this.subscribeCopiesList();
    }

    pushSearchPage (ev) {
        this.navCtrl.push(SearchPage, { type: 'Book' });
    }
}
