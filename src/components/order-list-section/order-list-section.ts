import { Component, OnInit, OnDestroy, ChangeDetectorRef, Input } from '@angular/core';
import { Order, OrderState, OrderOperation } from 'api/models/order';
import { NavController } from 'ionic-angular';
import { SegmentButton, LoadingController, NavParams } from 'ionic-angular';
import { EnhancedBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { User } from 'api/models';

@Component({
    selector: 'order-list-section',
    templateUrl: 'order-list-section.html',
})
export class OrderListSectionComponent extends EnhancedBase implements OnInit, OnDestroy {
    @Input() userId: ID;
    @Input() type: 'borrowed' | 'lent';

    user: User;
    orders: Order[];

    groups: Map<OrderState, Order[]> = new Map();
    display: string = '正在阅读';
    batchCounter: number = 1;

    noHeader: boolean = false;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit() {
        this.autorun(() => {
            this.user = User.get(this.userId);
            this.getOrders();
            this.resort();
        });
        this.initLoad();
    }

    initLoad() {
        if (!this.orders.length) super.initLoad();
    }

    get isBorrowedType() {
        return this.type == 'borrowed';
    }

    get isLentType() {
        return this.type == 'lent';
    }

    getOrders() {
        let selector;
        const loginUserId = Meteor.userId(),
            userId = this.user._id;
        if (this.user.isMe) {
            selector = this.isBorrowedType ? { borrowerId: userId } : { ownerId: userId };
        } else {
            selector = this.isBorrowedType
                ? { borrowerId: userId, ownerId: loginUserId }
                : { borrowerId: loginUserId, ownerId: userId };
        }
        this.orders = this.user.getOrders(selector).fetch();
    }

    getGroup(state) {
        return this.groups.get(state);
    }

    resort() {
        this.assignSegment();
        this.displayLatest();
    }

    assignSegment() {
        this.groups = new Map();
        this.orders.forEach((order) => {
            const groups = this.groups;
            const state = order.currentState;
            const list = [ ...(groups.get(state) || []), order ];
            list.sort((a, b) => b.updatedAt - a.updatedAt);
            groups.set(state, list);
        });
    }

    displayLatest() {
        // 显示最新订单所在Segment
        const latest = this.orders[this.orders.length - 1];
        if (!latest) return;
        this.display = latest.currentState;
    }

    get states() {
        const list = Array.from(this.groups.keys());
        return OrderState.sort(list);
    }

    onSegmentSelected(segmentButton: SegmentButton) {
        this.display = segmentButton.value;
        this.detectChanges();
    }

    onOrderStateChange(operation: OrderOperation) {
        this.resort();
        this.detectChanges();
    }

    subscribeOrdersList() {
        this.fetch$('orders', this.batchCounter++, this.user._id, this.type);
    }

    loadMore(infiniteScroll?) {
        super.loadMore(infiniteScroll);
        this.subscribeOrdersList();
    }
}
