import { Component, Input, OnDestroy, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Order } from 'api/models/order';
import { ToastProvider } from 'providers/toast';
import { User, Book, Copy } from 'api/models';
import { EnhancedBase } from '../../lib/enhanced-base';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { UserProfilePage } from 'pages/user-profile/user-profile';
import { BookProfilePage } from 'pages/book-profile/book-profile';

@Component({
    selector: 'order-card',
    templateUrl: 'order-card.html',
})
export class OrderCardComponent extends EnhancedBase implements OnInit, OnDestroy {
    @Input() orderId: ID;
    order: Order;
    borrower: User;
    owner: User;
    copy: Copy;
    book: Book;
    title: string;
    operations;
    computation: Tracker.Computation;

    @Output() orderStateChange: EventEmitter<Order> = new EventEmitter<Order>();

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit() {
        this.getOrderId();
        this.autorun(() => {
            this.order = Order.get(this.orderId);
            if (!this.order) return;
            this.order.permittedOperations$.then(
                (operations) => (this.operations = operations),
                (err) => this.showError(err),
            );

            this.borrower = this.order.borrower;

            this.owner = this.order.owner;

            this.copy = this.order.copy;
            if (!this.copy) return;

            this.book = this.copy.book;
            if (this.book) this.title = this.book.title;
        });
    }

    getOrderId() {
        let orderId = this.navParams.get('orderId');
        if (orderId) {
            this.orderId = orderId;
        }
    }

    perform(operation) {
        this.order.execute$(operation).subscribe(
            (msg) => {
                this.toast.present(msg);
                this.orderStateChange.emit(this.order);
            },
            (e) => this.toast.presentError(e),
        );
    }

    openUserProfile(ev, user) {
        this.navCtrl.push(UserProfilePage, { userId: user._id });
    }

    openBookProfile(ev, book) {
        this.navCtrl.push(BookProfilePage, { bookId: book._id });
    }
}
