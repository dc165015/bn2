import { Input, OnInit, ChangeDetectorRef, Component } from '@angular/core';
import { Copy, Book } from 'api/models';
import { ToastProvider } from 'providers/toast';
import { BlockComponent } from './block';

@Component({
    selector: 'copy-block',
    templateUrl: 'block.html',
})
export class CopyBlockComponent extends BlockComponent implements OnInit {
    @Input() copyId: ID;
    copy: Copy;
    ratio = { w: 1, h: 1.45 };

    constructor(public cdf: ChangeDetectorRef, public toast: ToastProvider) {
        super(cdf, toast);
    }

    ngOnInit() {
        if (this.copyId) {
            if (this.copyId == Book.Scanner._id) {
                this.item = Book.Scanner;
                super.ngOnInit();
            } else
                this.autorun(() => {
                    this.copy = Copy.get(this.copyId);
                    if (this.copy) this.item = Book.get(this.copy.bookId);
                    super.ngOnInit();
                });
        }
    }
}
