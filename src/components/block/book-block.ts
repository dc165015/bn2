import { Input, OnInit, ChangeDetectorRef, Component } from '@angular/core';
import { Book } from 'api/models';
import { ToastProvider } from 'providers/toast';
import { BlockComponent } from './block';

@Component({
    selector: 'book-block',
    templateUrl: 'block.html',
})
export class BookBlockComponent extends BlockComponent implements OnInit {
    @Input() bookId: ID;
    ratio = { w: 1, h: 1.45 };

    constructor(public cdf: ChangeDetectorRef, public toast: ToastProvider) {
        super(cdf, toast);
    }

    ngOnInit() {
        if (this.bookId)
            this.autorun(() => {
                this.item = Book.get(this.bookId);
                super.ngOnInit();
            });
    }
}
