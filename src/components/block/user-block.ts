import { Input, OnInit, ChangeDetectorRef, Component } from '@angular/core';
import { User } from 'api/models';
import { ToastProvider } from 'providers/toast';
import { BlockComponent } from './block';

@Component({
    selector: 'user-block',
    templateUrl: 'block.html',
    styles: [ 'user', 'no-rating' ],
})
export class UserBlockComponent extends BlockComponent implements OnInit {
    @Input() userId: ID;
    @Input() communityId: ID;

    constructor(public cdf: ChangeDetectorRef, public toast: ToastProvider) {
        super(cdf, toast);
    }

    ngOnInit() {
        if (!this.userId) return;

        this.autorun(() => {
            const user = (this.item = User.get(this.userId));
            super.ngOnInit();
            if (this.communityId) {
                const settings = user.communitySettings.find((item) => item.communityId == this.communityId);
                this.title = (settings && settings.myName) || user.nickname;
            }
        });
    }
}
