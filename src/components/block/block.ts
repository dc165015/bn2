import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { Rating } from 'api/models';
import { SubscriptionBase } from '../../lib/enhanced-base';
import { ToastProvider } from 'providers/toast';
import { PERIOD_NEW } from 'api/lib/constants';

@Component({
    selector: 'block',
    templateUrl: 'block.html',
})
export class BlockComponent extends SubscriptionBase implements OnInit {
    @Input() item: Dictionary<any>;

    @Input() image: string;
    @Input() rating: Rating;
    @Input() title: string;
    @Input() isNew: boolean;

    doFlashNew: boolean = true;

    ratio = { w: 1, h: 1 };

    computation: Tracker.Computation;

    constructor(public cdf: ChangeDetectorRef, public toast: ToastProvider) {
        super(cdf, toast);
    }

    ngOnInit() {
        if (!this.item) return;

        this.rating = this.rating || this.item.rating;

        this.title = this.title || this.item.title || this.item.name || this.item.nickname;

        this.image = this.image || this.item.image || this.item.avatar || this.item.picture || this.item.photo;

        this.isNew = this.isNew != void 0 ? this.isNew : Date.now() - this.item.createdAt < PERIOD_NEW;
    }
}
