import { Component, ViewChild } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { AMapComponent } from 'components/a-map/a-map';

@Component({
    selector: 'location-message',
    templateUrl: 'location-message.html',
})
export class LocationMessageComponent {

    @ViewChild(AMapComponent) map: AMapComponent;

    constructor(public viewCtrl: ViewController) { }

    sendLocation() {
        this.viewCtrl.dismiss(this.map.getLocation());
    }
}
