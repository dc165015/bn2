import { Component, Input, ViewChild, OnDestroy, ChangeDetectorRef, OnInit } from '@angular/core';
import { AMapProvider } from 'providers/map/amap';
import { Location } from 'api/models';
import { TextInput, LoadingController } from 'ionic-angular';
import { Random } from 'meteor/random';
import { ToastProvider } from 'providers/toast';
import { Subscription } from 'rxjs';
import { AMapClass } from 'providers/map/amap-class';
import { isPlainObject } from 'lodash';
import { EnhancedBase } from '../../lib/enhanced-base';

@Component({
    selector: 'a-map',
    templateUrl: 'a-map.html'
})
export class AMapComponent extends EnhancedBase implements OnInit {
    @Input() location: Location;
    @Input() readonly: boolean = false;
    @Input() mapId: string = 'AMapContainer';
    @Input() inputId: string = 'poiInput';
    @Input() markerLabel: string = '群址';
    map: AMapClass;
    name: string;
    address: string;

    constructor(
        public amapProvider: AMapProvider,
        public toast: ToastProvider,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    ngOnInit() {
        if (isPlainObject(this.location))
            this.location = Location.fromRawDoc(this.location);

        const randomId = 'ID' + Random.id(10);
        this.mapId = this.mapId + randomId;
        this.inputId = this.inputId + randomId;
    }

    ngAfterViewInit() {
        this.drawMap();
    }

    getLocation() {
        return this.map.getLocation();
    }

    drawMap() {
        this.map = this.amapProvider.create();

        const options = {
            location: this.location,
            mapId: this.mapId,
            inputId: this.inputId,
            markerLabel: this.markerLabel,
        };

        this.addSubscription(this.map.present(options).subscribe({
            next: () => {
                this.name = this.map.location.name;
                this.address = this.map.location.address;
                this.cdf.detectChanges();
            },
            error: err => this.toast.presentError(err)
        }));
    }
}
