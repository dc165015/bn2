import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

@Component({
    selector: 'full-text-area',
    templateUrl: 'full-text-area.html',
})
export class FullTextAreaComponent implements OnInit {
    settingsForm: FormGroup;
    title: string;
    content: string;
    placeholder: string;
    maxLength: number;

    constructor (public formBuilder: FormBuilder, public viewCtrl: ViewController, public navParams: NavParams) {}

    ngOnInit () {
        this.content = this.navParams.get('content');
        this.placeholder = this.navParams.get('placeholder');
        this.title = this.navParams.get('title') || this.placeholder;
        this.maxLength = this.navParams.get('maxLength') || 1000;

        this.settingsForm = new FormGroup({
            textareaCtrl: new FormControl('', Validators.maxLength(this.maxLength)),
        });
        if (this.content)
            this.settingsForm.patchValue({
                textareaCtrl: this.content,
            });
    }

    get textareaCtrl () {
        return this.settingsForm.get('textareaCtrl');
    }

    close () {
        this.viewCtrl.dismiss();
    }

    save () {
        if (this.settingsForm.valid) this.viewCtrl.dismiss(this.textareaCtrl.value);
    }

    validation_messages = {
        textareaCtrl: [
            { type: 'maxlength', message: `最多${this.maxLength}字` },
        ],
    };
}
