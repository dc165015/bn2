import { Component, ChangeDetectorRef } from '@angular/core';
import { ModalController, ViewController, Platform, NavController, LoadingController } from 'ionic-angular';
import { MessageType } from 'api/models/message';
import { LocationMessageComponent } from 'components/location-message/location-message';
import { PictureProvider } from 'providers/picture';
import { ToastProvider } from 'providers/toast';
import { EnhancedBase } from '../../lib/enhanced-base';
import { Log } from 'api/lib/logger';

@Component({
    selector: 'messages-attachments',
    templateUrl: 'messages-attachments.html',
})
export class MessagesAttachmentsComponent extends EnhancedBase {
    constructor (
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public modelCtrl: ModalController,
        public pictureProvider: PictureProvider,
        public toast: ToastProvider,
        public platform: Platform,
        public cdf: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
    ) {
        super(toast, cdf, loadingCtrl);
    }

    sendLocation (): void {
        const locationModal = this.modelCtrl.create(LocationMessageComponent);
        locationModal.onDidDismiss((location) => {
            if (!location) {
                this.viewCtrl.dismiss();
                return;
            }

            this.viewCtrl.dismiss({
                messageType: MessageType.LOCATION,
                selectedLocation: location,
            });
        });

        locationModal.present();
    }

    sendPicture (): void {
        this.pictureProvider.askPicture('other', true).then(
            (file) => {
                this.viewCtrl.dismiss({
                    messageType: MessageType.PICTURE,
                    selectedPicture: file,
                });
            },
            (err) => this.showError(err),
        );
    }
}
